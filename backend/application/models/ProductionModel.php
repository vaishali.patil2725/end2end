<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ProductionModel extends CI_Model
{
	
	/***********************Trim Inwards Functions*********************************************/
	public function getTrimInwardEntries($data, $company = NULL, $unit = NULL){
		$this->db->select('l.*, erp.erp_title');
        $this->db->from('trim_inward_log l');
        $this->db->join('erp', 'erp.erp_id = l.erp');
		
		if($data['f_erp'] != ''){
			 $this->db->where('l.erp', $data['f_erp']);
		}
		
		if($data['f_date'] != ''){
			 $this->db->where('DATE(l.created_on)', $data['f_date']);
		}
		
		if($unit != NULL){
			$this->db->where('l.unit', $unit);
		}
		
		if($company != NULL){
			$this->db->where('l.company', $company);
		}
        return $this->db->get()->result();
	}
	
	public function getTrimInwardEntriesReport($filters){
		$this->db->select('l.*, erp.erp_title');
        $this->db->from('trim_inward_log l');
        $this->db->join('erp', 'erp.erp_id = l.erp');
		
		if($filters['from_date'] != ''){
			 $this->db->where('DATE(l.created_on) >=', $filters['from_date']);
		}
		
		if($filters['to_date'] != ''){
			 $this->db->where('DATE(l.created_on) <=', $filters['to_date']);
		}
		
		if($filters['unit'] != NULL){
			$this->db->where('l.unit', $filters['unit']);
		}
		
        return $this->db->get()->result();
	}
	
	public function getTrimInwardDetailsById($id){
		$this->db->select('l.*, erp.erp_title, u.full_name');
        $this->db->from('trim_inward_log l');
        $this->db->join('erp', 'erp.erp_id = l.erp');
		 $this->db->join('users u', 'u.user_id = l.created_by');
        return $this->db->get()->row();
	}
	
	public function getTrimOutwardLogDataByInwardId($inward_id){
		$this->db->select('l.*, u.full_name, d.dept_name, ul.line as line_name');
        $this->db->from('trim_outward_log l');
		$this->db->join('departments d', 'd.dept_id = l.department');
		$this->db->join('unit_lines ul', 'ul.line_id = l.line');
		$this->db->join('users u', 'u.user_id = l.created_by');
		$this->db->where('inward_id', $inward_id);
        return $this->db->get()->result();
	}
	
	/***********************Fabric Inwards Functions*********************************************/
	public function getFilteredFabricInwards($data, $company = NULL, $unit = NULL){
		$this->db->select('l.*, erp.erp_title, u.full_name as added_by');
        $this->db->from('fabric_inward_log l');
        $this->db->join('erp', 'erp.erp_id = l.erp');
		$this->db->join('users u', 'u.user_id = l.created_by');
		 
		if($data['f_erp'] != ''){
			 $this->db->where('l.erp', $data['f_erp']);
		}
		
		if($data['f_date'] != ''){
			 $this->db->where('DATE(l.created_on)', $data['f_date']);
		}
		
		if($unit != NULL){
			$this->db->where('l.unit', $unit);
		}
		
		if($company != NULL){
			$this->db->where('l.company', $company);
		}
        return $this->db->get()->result();
	}
	public function getFabricInwardsReport($filters){
		$this->db->select('l.*, erp.erp_title, u.full_name as added_by');
        $this->db->from('fabric_inward_log l');
        $this->db->join('erp', 'erp.erp_id = l.erp');
		$this->db->join('users u', 'u.user_id = l.created_by');
		 
		if($filters['from_date'] != ''){
			 $this->db->where('DATE(l.created_on) >=', $filters['from_date']);
		}
		
		if($filters['to_date'] != ''){
			 $this->db->where('DATE(l.created_on) <=', $filters['to_date']);
		}
		
		if($filters['unit'] != NULL){
			$this->db->where('l.unit', $filters['unit']);
		}
        return $this->db->get()->result();
	}
	
	public function getFabricInwardDetailsById($id){
		$this->db->select('l.*, erp.erp_title, u.full_name');
        $this->db->from('fabric_inward_log l');
        $this->db->join('erp', 'erp.erp_id = l.erp');
		 $this->db->join('users u', 'u.user_id = l.created_by');
        return $this->db->get()->row();
	}
	
	public function getFabricutwardLogDataByInwardId($inward_id){
		$this->db->select('l.*, u.full_name, d.dept_name, ul.line as line_name');
        $this->db->from('fabric_outward_log l');
		$this->db->join('departments d', 'd.dept_id = l.department');
		$this->db->join('unit_lines ul', 'ul.line_id = l.line');
		$this->db->join('users u', 'u.user_id = l.created_by');
		$this->db->where('inward_id', $inward_id);
        return $this->db->get()->result();
	}
	
	/***********************Common Trim / Fabric Functions*********************************************/
	public function addInwardEntry($data){
		$table = $data['table'];
		unset($data['table']);
        return $this->db->insert($table, $data);
    }
	
	public function updateInwardEntry($data){
		$table = $data['table'];
		unset($data['table']);
        $this->db->where('inward_id', $data['inward_id']);
        return $this->db->update($table,$data);
    }
	
	public function addOutwardEntry($data){
		$table = $data['table'];
		unset($data['table']);
        return $this->db->insert($table, $data);
    }
	
	public function updateOutwardEntry($data){
		$table = $data['table'];
		unset($data['table']);
        $this->db->where('outward_id', $data['outward_id']);
        return $this->db->update($table,$data);
    }
	/***********************End Common Trim / Fabric Functions*********************************************/
	
	/****************************************************************************/
	
    public function getItemCodesByRequiremenTypeByErpId($requirement_type, $erp){
        $this->db->select('requirement_id, item_code');
        $this->db->from('trim_fabric_requirement');
        $this->db->where('requirement_type', $requirement_type);
        $this->db->where('erp', $erp);
    
        return $this->db->get()->result();	
    }

    public function updateMaterialReceivedEntry($data){
        $this->db->where('inward_id', $data['inward_id']);
        return $this->db->update('material_received',$data);
    }
    
    public function addMaterialReceivedEntry($data){
        return $this->db->insert('material_received', $data);
    }

    public function getReceivedMaterialData($unit, $inward_type){
        $this->db->select('m.*, erp.erp_title');
        $this->db->from('material_received m');
        $this->db->join('erp', 'erp.erp_id = m.erp');
        $this->db->where('unit_id', $unit);
		$this->db->where('inward_type', $inward_type);
        return $this->db->get()->result();
    }
	
	
    public function updateMaterialOutwardEntry($data){
        $this->db->where('outward_id', $data['outward_id']);
        return $this->db->update('outward_logs',$data);
    }
    
    public function addMaterialOutwardEntry($data){
        return $this->db->insert('outward_logs', $data);
    }
	
	public function getOutwardLogData($unit, $inward_table, $outward_table){
		$this->db->select('l.*, lt.*, erp.erp_title, d.dept_name, ul.line as line_name, u.full_name as added_by, l.quantity as outward_quantity, lt.received_quantity');
        $this->db->from(''.$outward_table.' l');
		$this->db->join(''.$inward_table.' lt', 'lt.inward_id = l.inward_id');
        $this->db->join('erp', 'erp.erp_id = lt.erp');
		$this->db->join('departments d', 'd.dept_id = l.department');
		$this->db->join('unit_lines ul', 'ul.line_id = l.line');
		$this->db->join('users u', 'l.created_by = u.user_id');
        $this->db->where('lt.unit', $unit);
        return $this->db->get()->result();
	}
	
	public function getOutwardLogDataReport($inward_table, $outward_table, $filters){
		$this->db->select('l.*, lt.*, erp.erp_title, d.dept_name, ul.line as line_name, u.full_name as added_by, l.quantity as outward_quantity, lt.received_quantity');
        $this->db->from(''.$outward_table.' l');
		$this->db->join(''.$inward_table.' lt', 'lt.inward_id = l.inward_id');
        $this->db->join('erp', 'erp.erp_id = lt.erp');
		$this->db->join('departments d', 'd.dept_id = l.department');
		$this->db->join('unit_lines ul', 'ul.line_id = l.line');
		$this->db->join('users u', 'l.created_by = u.user_id');
		
		if($filters['from_date'] != ''){
			 $this->db->where('DATE(l.created_on) >=', $filters['from_date']);
		}
		
		if($filters['to_date'] != ''){
			 $this->db->where('DATE(l.created_on) <=', $filters['to_date']);
		}
		
		if($filters['unit'] != NULL){
			$this->db->where('lt.unit', $filters['unit']);
		}
        return $this->db->get()->result();
	}
	
    public function addProductionData($data){
		$table = $data['table'];
		unset($data['table']);
        return $this->db->insert($table, $data);
    }
	
	public function updateProductionData($data){
		$table = $data['table'];
		unset($data['table']);
        $this->db->where('id', $data['id']);
        return $this->db->update($table,$data);
    }
    
	public function getProductionData($data, $unit){

		$this->db->select('cpd.*, erp.erp_title, u.full_name, ul.line as line_name,');
        $this->db->from(''.$data['table'].' cpd');
        $this->db->join('erp', 'erp.erp_id = cpd.erp');
		$this->db->join('unit_lines ul', 'ul.line_id = cpd.line');
		$this->db->join('users u', 'cpd.created_by = u.user_id');
        $this->db->where('cpd.unit', $unit);
		
		if($data['f_erp'] != ''){
			 $this->db->where('cpd.erp', $data['f_erp']);
		}
		
		if(isset($data['f_section']) && $data['f_section'] != ''){
			 $this->db->where('cpd.section', $data['f_section']);
		}
		
		if($data['f_line'] != ''){
			 $this->db->where('cpd.line', $data['f_line']);
		}
		
		if($data['f_date'] != ''){
			 $this->db->where('DATE(cpd.created_on)', $data['f_date']);
		}
		
        return $this->db->get()->result();
	}
	
	public function getFabricInspectionLogs($data, $unit){
		$this->db->select('l.*, erp.erp_title, u.full_name as added_by');
        $this->db->from('fabric_checking_log l');
        $this->db->join('erp', 'erp.erp_id = l.erp');
		$this->db->join('users u', 'l.created_by = u.user_id');
        $this->db->where('unit', $unit);
		
		if($data['f_erp'] != ''){
			 $this->db->where('l.erp', $data['f_erp']);
		}
		
		if($data['f_date'] != ''){
			 $this->db->where('DATE(l.created_on)', $data['f_date']);
		}
		
        return $this->db->get()->result();
	}
	
	public function getFabricInspectionLogsReport($filters){
		$this->db->select('l.*, erp.erp_title, u.full_name as added_by');
        $this->db->from('fabric_checking_log l');
        $this->db->join('erp', 'erp.erp_id = l.erp');
		$this->db->join('users u', 'l.created_by = u.user_id');
		
		if($filters['from_date'] != ''){
			 $this->db->where('DATE(l.created_on) >=', $filters['from_date']);
		}
		
		if($filters['to_date'] != ''){
			 $this->db->where('DATE(l.created_on) <=', $filters['to_date']);
		}
		
		if($filters['unit'] != NULL){
			$this->db->where('l.unit', $filters['unit']);
		}
		
        return $this->db->get()->result();
	}	
	
	public function getReportProductionData($table, $filters){

		$this->db->select('cpd.*, erp.erp_title, u.full_name, ul.line as line_name,');
        $this->db->from(''.$table.' cpd');
        $this->db->join('erp', 'erp.erp_id = cpd.erp');
		$this->db->join('unit_lines ul', 'ul.line_id = cpd.line');
		$this->db->join('users u', 'cpd.created_by = u.user_id');
        $this->db->where('cpd.unit', $filters['unit']);
		
		if($filters['from_date'] != ''){
			 $this->db->where('DATE(cpd.created_on) >=', $filters['from_date']);
		}
		
		if($filters['to_date'] != ''){
			 $this->db->where('DATE(cpd.created_on) <=', $filters['to_date']);
		}
		
        return $this->db->get()->result();
	}

	
    public function updateFabricInspectionEntry($data){
        $this->db->where('inspection_id', $data['inspection_id']);
        return $this->db->update('fabric_checking_log',$data);
    }
    
    public function addFabricInspectionEntry($data){
        return $this->db->insert('fabric_checking_log', $data);
    }
}
