<?php defined('BASEPATH') or exit('No direct script access allowed');

class DepartmentModel extends CI_Model
{

    protected $dept_id;
    protected $dept_name;
    protected $level;
    protected $created_by;
    protected $created_on;

    CONST PRODUCTION = 'Production';
	
	 CONST CUTTING_ID = 5;
	 CONST SEWING_ID = 6;
	 CONST FINISHING_ID = 7;     
	 CONST KAJA_BUTTON_ID = 8;
	 CONST FUSING_ID = 9;
	 CONST DISPATCH_ID = 10;
	 CONST TRIM_ID = 3;
	 CONST FABRIC_ID = 4;
    
    public function getDepartments($level = NULL){
        $this->db->select('d.*, u.full_name as created_by');
        $this->db->from('departments d');
        $this->db->join('users u', 'u.user_id = d.created_by');
        if($level!=NULL){
            $this->db->where('level', $level);
        }
        return $this->db->get()->result();
    }

    public function addNewDepartment($data)
    {
        return $this->db->insert('departments', $data);
    }

    public function updateDepartment($data){
        $this->db->where('dept_id', $data['dept_id']);
        return $this->db->update('departments',$data);
    }

    public function deleteDepartment($dept_id, $user_id){
        $this->db->set('deleted_by', $user_id);
        $this->db->set('deleted_on', date('Y-m-d H:i:s'));
        $this->db->where('dept_id', $dept_id);
        return $this->db->update('departments');
    }
}
