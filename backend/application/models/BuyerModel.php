<?php defined('BASEPATH') OR exit('No direct script access allowed');

class BuyerModel extends CI_Model
{
    protected $table = 'buyers';

    public function getBuyers(){
        $this->db->select('*');
        $this->db->from('buyers');
		return $this->db->get()->result();
    }

    public function updateBuyer($data){
        $this->db->where('buyer_id', $data['buyer_id']);
        return $this->db->update($this->table,$data);
    }
    
    public function addBuyer($data){
        if($this->db->insert($this->table, $data) == true){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }

    public function getBuyerById($buyer_id){
        $this->db->select('b.*, u1.full_name as created_by');
        $this->db->from('buyers b');
        $this->db->join('users u', 'u.buyer_id=b.buyer_id');
        $this->db->join('users u1', 'u1.user_id=b.created_by');
        $this->db->where('b.buyer_id', $buyer_id);
		return $this->db->get()->row();
    }
}
