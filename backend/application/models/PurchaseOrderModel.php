<?php defined('BASEPATH') OR exit('No direct script access allowed');

class PurchaseOrderModel extends CI_Model
{
    protected $table = 'purchase_orders';
	
	public function getPurchaseOrders($company = NULL) {
        $this->db->select('p.order_id, p.order_title');
        $this->db->from('purchase_orders p');
		
		if($company != NULL){
			 $this->db->where('p.company_id', $company);
		}
		return $this->db->get()->result();	
    }
	
	public function getFilteredPurchaseOrders($data){

		$this->db->select('p.*, b.full_name as buyer, b.email as buyer_email, u.full_name as created_by_name, s.style_code as style, c.name as company_name');
        $this->db->from('purchase_orders p');
        $this->db->join('buyers b', 'p.buyer_id = b.buyer_id', 'left');
        $this->db->join('styles s', 'p.style_id = s.style_id', 'left');
		$this->db->join('companies c', 'c.company_id = p.company_id', 'left');
		$this->db->join('users u', 'u.user_id = p.created_by', 'left');
		
		if($data['po_company'] != ''){
			 $this->db->where('p.company_id', $data['po_company']);
		}
		
		if($data['po_title'] != ''){
			 $this->db->where('p.buyer_id', $data['po_title']);
		}
		
		if($data['po_style'] != ''){
			 $this->db->where('p.style_id', $data['po_style']);
		}
		
		if($data['po_date_issued'] != ''){
			 $this->db->where('p.date_issued', $data['po_date_issued']);
		}
		return $this->db->get()->result();	
	}
    
	public function getPurchaseOrdersByBuyerId($buyer_id) {
        $this->db->select('p.*, s.style_name as style');
        $this->db->from('purchase_orders p');
        $this->db->join('styles s', 'p.style_id = s.style_id', 'left');
        $this->db->where('p.buyer_id', $buyer_id);
		return $this->db->get()->result();	
    }
    public function addNewPurchaseOrder($data){
        return $this->db->insert($this->table, $data);
    }

    public function updatePurchaseOrder($data){
        $this->db->where('order_id', $data['order_id']);
        return $this->db->update($this->table,$data);
    }

    public function addDispatchLot($data){
        return $this->db->insert('dispatch_lots', $data);
    }

    public function getPurchaseOrderById($order_id){
        $this->db->select('p.*, b.full_name as buyer, b.email as buyer_email, s.style_name as style');
        $this->db->from('purchase_orders p');
        $this->db->join('buyers b', 'p.buyer_id = b.buyer_id', 'left');
        $this->db->join('styles s', 'p.style_id = s.style_id', 'left');
        $this->db->where('order_id', $order_id);
        return $this->db->get()->row();	

    }

    public function getDispatchLots($company = NULL, $unit = NULL){
        $this->db->select('d.*, e.full_name as created_by, erp.erp_title');
        $this->db->from('dispatch_lots d');
		$this->db->join('erp', 'erp.erp_id = d.erp');
        $this->db->join('employees e', 'e.employee_id = d.created_by');
		
		if($company !== NULL){
			$this->db->where('company', $company);
		}
		
		if($unit !== NULL && $unit != 0){
			$this->db->where('unit', $unit);
		}
        return $this->db->get()->result();
    }
	
	public function getCartonContent($dispatch_id){
		$this->db->select('c.*, e.full_name as created_by');
        $this->db->from('carton_content c');
        $this->db->join('employees e', 'e.employee_id = c.created_by');
		$this->db->where('dispatch_no', $dispatch_id);
		
        return $this->db->get()->result();
	}
	
	public function updateCartonContent($data){
        $this->db->where('carton_id', $data['carton_id']);
        return $this->db->update('carton_content',$data);
    }

    public function addCartonContent($data){
        return $this->db->insert('carton_content', $data);
    }


    public function getLotsToAddDeliverySchedule($order_id, $ids = []){
        $this->db->select('d.dispatch_id, d.dispatch_lot');
        $this->db->from('dispatch_lots d');
        $this->db->where('po_number', $order_id);
        if(count($ids) > 0){
            $this->db->where_not_in('dispatch_id ', $ids);
        }
        return $this->db->get()->result();
    }

    public function getDispatchDetailsByErpId($erp_id){
        $this->db->distinct();
        $this->db->select('lot_number');
        $this->db->from('dispatch_details');
        $this->db->where('erp', $erp_id);
        return $this->db->get()->result();
    }

    public function updateDispatchLot($data){
        $this->db->where('dispatch_id', $data['dispatch_id']);
        return $this->db->update('dispatch_lots',$data);
    }
	
}