<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ErpModel extends CI_Model
{
    protected $table = 'erp';
	
	public function getErpList($company_id = NULL) {
        $this->db->select('erp.*, po.order_title, s.style_name, b.full_name as buyer,b.buyer_id, c.name as copmany, es.erp_status as status');
        $this->db->from('erp');
        $this->db->join('purchase_orders po', 'erp.po_number = po.order_id', 'left');
        $this->db->join('styles s', 'erp.style_number = s.style_id', 'left');
        $this->db->join('buyers b', 'erp.buyer = b.buyer_id', 'left');
        $this->db->join('companies c', 'po.company_id = c.company_id', 'left');
        $this->db->join('erp_statuses es', 'erp.erp_status = es.erp_status_id', 'left');
        if($company_id != NULL){
            $this->db->where('po.company_id', $company_id);
        }
		return $this->db->get()->result();	
    }
	
	public function getFilteredErpListData($data, $company_id = NULL){
		$this->db->select('erp.*, u.full_name as created_by_name, po.order_title, s.style_name, b.full_name as buyer,b.buyer_id, c.name as copmany, es.erp_status as status');
        $this->db->from('erp');
        $this->db->join('purchase_orders po', 'erp.po_number = po.order_id', 'left');
        $this->db->join('styles s', 'erp.style_number = s.style_id', 'left');
        $this->db->join('buyers b', 'erp.buyer = b.buyer_id', 'left');
        $this->db->join('companies c', 'po.company_id = c.company_id', 'left');
        $this->db->join('erp_statuses es', 'erp.erp_status = es.erp_status_id', 'left');
		$this->db->join('users u', 'u.user_id = erp.created_by', 'left');

		if($company_id != NULL){
            $this->db->where('po.company_id', $company_id);
        }
		
		if($data['erp_order'] != ''){
			 $this->db->where('erp.po_number', $data['erp_order']);
		}
		
		if($data['erp_buyer'] != ''){
			 $this->db->where('erp.buyer', $data['erp_buyer']);
		}
		
		if($data['erp_style'] != ''){
			 $this->db->where('erp.style_number', $data['erp_style']);
		}
		
		if($data['erp_status'] != ''){
			 $this->db->where('erp.erp_status', $data['erp_status']);
		}
		$this->db->order_by('created_on');

		return $this->db->get()->result();	
	}

	public function addNewErp($data){
        $this->db->insert($this->table, $data);
        $insert_id = $this->db->insert_id();

        return  $insert_id;
    }

    public function getColorData($erp_id){
        $this->db->select('*');
        $this->db->from('erp_colors');       
        $this->db->where('erp', $erp_id);
        return $this->db->get()->result();	
    }

    public function updateErp($data){
        $this->db->where('erp_id', $data['erp_id']);
        return $this->db->update($this->table, $data);
    }

	public function getLatestErpNumber(){
		$this->db->select('erp.erp_id, erp.erp_title');
        $this->db->from('erp');
		$this->db->where('MONTH(created_on)', date('m'));
		return $this->db->get()->row();		 
	}
	
    public function getErpById($erp_id){
        $this->db->select('erp.*, b.*, s.style_name, po.order_title, es.erp_status as status');
        $this->db->from('erp');
        $this->db->join('purchase_orders po', 'erp.po_number = po.order_id', 'left');
        $this->db->join('styles s', 'erp.style_number = s.style_id', 'left');
        $this->db->join('buyers b', 'erp.buyer = b.buyer_id', 'left');
        $this->db->join('erp_statuses es', 'es.erp_status_id = erp.erp_status', 'left');
        $this->db->where('erp_id', $erp_id);
        return $this->db->get()->row();	
    }

    public function addDeliverySchedule($data){
        return $this->db->insert('dispatch_details', $data);
    }

    public function getDeliverySchedulesByErpId($erp_id){
        $this->db->select('d.*, e1.full_name as created_by, e2.full_name as helper_name, l.dispatch_lot');
        $this->db->from('dispatch_details d');
        $this->db->join('employees e1', 'e1.employee_id = d.created_by', 'left');
        $this->db->join('employees e2', 'e2.employee_id = d.helper', 'left');
        $this->db->join('dispatch_lots l', 'l.dispatch_id = d.lot_number', 'left');
        $this->db->where('erp', $erp_id);
        return $this->db->get()->result();	
    }

    public function updateDeliverySchedule($data){
        $this->db->where('id', $data['id']);
        return $this->db->update('dispatch_details',$data);
    }

    public function addMaterialRequirement($data){
        return $this->db->insert('material_requirement', $data);
    }

    public function updateMaterialRequirement($data){
        $this->db->where('id', $data['id']);
        return $this->db->update('material_requirement',$data);
    }

    public function getMaterialRequirements($erp_id, $req){
        $this->db->select('*');
        $this->db->from('material_requirement');
        $this->db->where('erp_id', $erp_id);
        if($req == 'top'){
            $this->db->where('top_requirement!=', '');
        }

        if($req == 'bottom'){
            $this->db->where('bottom_requirement!=', '');
        }
        return $this->db->get()->result();	
    }

    public function getRequirementsByErpIdByRequiremenType($erp_id, $requirement_type){
        $this->db->select('r.*, e1.full_name');
        $this->db->from('trim_fabric_requirement r');
        $this->db->join('employees e1', 'e1.employee_id = r.created_by', 'left');
        $this->db->where('r.erp', $erp_id);
        $this->db->where('r.requirement_type', $requirement_type);
        return $this->db->get()->result();
    }

    public function addTrimFabricRequirementData($data){
        return $this->db->insert('trim_fabric_requirement', $data);
    }

    public function updateTrimFabricRequirementData($data){
        $this->db->where('requirement_id', $data['requirement_id']);
        return $this->db->update('trim_fabric_requirement', $data);
    }

    public function getErpStatuses() {
        $this->db->select('*');
        $this->db->from('erp_statuses');
		return $this->db->get()->result();	
    }
}