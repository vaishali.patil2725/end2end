<?php defined('BASEPATH') OR exit('No direct script access allowed');

class UnitLinesModel extends CI_Model
{
    protected $table = 'unit_lines';

    public function getUnitLines($department, $company = NULL, $unit = NULL){
        $this->db->select('l.*, u.full_name as created_by, d.dept_name, c.name as company, unit.unit_name');
        $this->db->from('unit_lines l');
        $this->db->join('users u', 'u.user_id = l.created_by', 'left');
        $this->db->join('companies c', 'c.company_id = l.company_id', 'left');
        $this->db->join('units unit', 'unit.unit_id = l.unit_id', 'left');
        $this->db->join('departments d', 'd.dept_id = l.dept_id', 'left');
		$this->db->where('l.dept_id', $department);
		
        if($company != NULL){
            $this->db->where('l.company_id', $company);
        }

        if($unit != NULL && $unit != 0){
            $this->db->where('l.unit_id', $unit);
        }
        
		return $this->db->get()->result();
    }

	public function getAllLines($company = NULL, $unit = NULL){
        $this->db->select('l.*, u.full_name as created_by, d.dept_name, c.name as company, unit.unit_name');
        $this->db->from('unit_lines l');
        $this->db->join('users u', 'u.user_id = l.created_by', 'left');
        $this->db->join('companies c', 'c.company_id = l.company_id', 'left');
        $this->db->join('units unit', 'unit.unit_id = l.unit_id', 'left');
        $this->db->join('departments d', 'd.dept_id = l.dept_id', 'left');
		
        if($company != NULL){
            $this->db->where('l.company_id', $company);
        }

        if($unit != NULL && $unit != 0){
            $this->db->where('l.unit_id', $unit);
        }
        
		return $this->db->get()->result();
    }

    public function updateLine($data){
        $this->db->where('line_id', $data['line_id']);
        return $this->db->update($this->table,$data);
    }
    
    public function addNewLine($data){
        return $this->db->insert($this->table, $data);
    }
}
