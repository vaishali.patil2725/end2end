<?php defined('BASEPATH') OR exit('No direct script access allowed');

class BrandModel extends CI_Model
{
    protected $table = 'brands';

    public function getBrands($company_id = NULL){
        $this->db->select('b.*, u.full_name as created_by, c.name as company');
        $this->db->from('brands b');
        $this->db->join('users u', 'u.user_id = b.created_by', 'left');
        $this->db->join('companies c', 'c.company_id = b.company', 'left');
        if($company_id != NULL){
            $this->db->where('company', $company_id);
        }
		return $this->db->get()->result();
    }

    public function updateBrand($data){
        $this->db->where('brand_id', $data['brand_id']);
        return $this->db->update($this->table,$data);
    }
    
    public function addNewBrand($data){
        return $this->db->insert($this->table, $data);
    }
}
