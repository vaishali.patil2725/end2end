<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MachineModel extends CI_Model
{
    protected $table = 'machines';

    public function getAllMachines($company = NULL, $unit = NULL){
        $this->db->select('m.*, u.full_name as added_by');
        $this->db->from('machines m');
		$this->db->join('users u', 'u.user_id = m.created_by');
		
		if($company != NULL){
			$this->db->where('m.company', $company);
		}
		
		if($unit != NULL){
			$this->db->where('unit', $unit);
		}
		return $this->db->get()->result();
    }

    public function updateMachine($data){
        $this->db->where('machine_id', $data['machine_id']);
        return $this->db->update($this->table, $data);
    }
    
    public function addNewMachine($data){
        return $this->db->insert($this->table, $data);
    }
	
	 public function updateServiceHistory($data){
        $this->db->where('service_id', $data['service_id']);
        return $this->db->update('service_history', $data);
    }
    
    public function addServiceHistory($data){
        return $this->db->insert('service_history', $data); 
    }
	
	public function getServiceHistory($company = NULL, $unit = NULL, $machine = NULL){
        $this->db->select('s.*, m.serial_number, u.full_name as added_by');
        $this->db->from('service_history s'); 
		$this->db->join('machines m', 'm.machine_id = s.machine');
		$this->db->join('users u', 'u.user_id = s.created_by');
		 
		if($machine != NULL){
			$this->db->where('m.machine_id', $machine);
		}
		
		if($company != NULL){
			$this->db->where('m.company', $company);
		}
		
		if($unit != NULL){
			$this->db->where('m.unit', $unit);
		}
		
		return $this->db->get()->result();
    }

    public function getBuyerById($buyer_id){
        $this->db->select('b.*, u1.full_name as created_by');
        $this->db->from('buyers b');
        $this->db->join('users u', 'u.buyer_id=b.buyer_id');
        $this->db->join('users u1', 'u1.user_id=b.created_by');
        $this->db->where('b.buyer_id', $buyer_id);
		return $this->db->get()->row();
    }
	
	public function validateMachinCode($serial_number, $machine_id = NULL){
		$this->db->select('*');
        $this->db->from('machines');
        $this->db->where('serial_number', $serial_number);
		if($machine_id != NULL){
			$this->db->where('machine_id !=', $machine_id);
		}
		$rows = $this->db->get()->num_rows();;
		
		if($rows > 0){
			return false;
		}
		return true;
	}
}
