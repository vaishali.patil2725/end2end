<?php defined('BASEPATH') or exit('No direct script access allowed');

class UserRoleModel extends CI_Model
{

    protected $table = 'user_roles';
    
    protected $id;
    protected $role;
    protected $description;
    protected $created_by;
    protected $created_on;

    CONST ADMIN_ID = 1;
    CONST SUPERUSER_ID = 2;
    CONST USER_ID = 3;
    CONST BUYER_ID = 4;
    
    public function getUserRoles($isRemoveBuyer = false){
        $this->db->select('r.*, u.full_name as created_by');
        $this->db->from('user_roles r');
        $this->db->join('users u', 'r.created_by = u.user_id', 'left');
		
		if($isRemoveBuyer == true){
			$this->db->where('role !=', 'Buyer');
		}
        return $this->db->get()->result();
    }

    public function addUserRole($data)
    {
        return $this->db->insert($this->table, $data);
    }

    public function updateUserRole($data){
        $this->db->where('id', $data['id']);
        return $this->db->update($this->table,$data);
    }

    public function deleteUserRole($id, $user_id){
        $this->db->set('deleted_by', $user_id);
        $this->db->set('deleted_on', date('Y-m-d H:i:s'));
        $this->db->where('id', $id);
        return $this->db->update($this->table);
    }
}
