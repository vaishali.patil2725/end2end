<?php defined('BASEPATH') or exit('No direct script access allowed');

class DesignationModel extends CI_Model
{

    protected $designation_id;
    protected $designation;
    protected $role;
    protected $access_level;
    protected $created_by;
    protected $created_on;

    CONST SYSTEM_HEAD_ID = 1;
    CONST MANAGING_DIRECTOR_ID = 2;
    CONST MERCHANT_HEAD_ID = 3;
	CONST TRIM_HEAD_ID = 5;
	CONST FABRIC_HEAD_ID = 6;
    CONST GARMENT_MANAGER_ID = 7;

    CONST SYSTEM_HEAD = 'System Head';
    CONST MANAGING_DIRECTOR = 'Managing Director';
    CONST GARMENT_MANAGER = 'Garment Manager';
    CONST MERCHANT_HEAD = 'Merchant Head';
	
	
	
    public function getAllDesignations($ids = NULL){
        $this->db->select('d.*, u.full_name as created_by, r.role');
        $this->db->from('designations d');
        $this->db->join('users u', 'u.user_id = d.created_by', 'left');
        $this->db->join('user_roles r', 'r.id = d.user_role', 'left');
        $this->db->where('d.designation_id !=', 1);

        if($ids != NULL){
            $this->db->where_not_in('d.designation_id', $ids);
        }
        return $this->db->get()->result();
    }

    public function addNewDesignation($data)
    {
        return $this->db->insert('designations', $data);
    }

    public function updateDesignation($data){
        $this->db->where('designation_id', $data['designation_id']);
        return $this->db->update('designations',$data);
    }

    public function deleteDesignation($designation_id, $user_id){
        $this->db->set('deleted_by', $user_id);
        $this->db->set('deleted_on', date('Y-m-d H:i:s'));
        $this->db->where('designation_id', $designation_id);
        return $this->db->update('designations');
    }
	
	/************************* Functions to delete ***************************/
	
	/*
	
    public function getDesignationsByDesignationId($designation_id){
        $this->db->select('designation');
        $this->db->from('designations');
        $this->db->where('designation_id', $designation_id);
        $res = $this->db->get()->row();
        return $res->designation;
    }
	
    public function getCurrentUserDesignation($user_id){
        $this->db->select('d.designation');
        $this->db->from('designations d');
        $this->db->join('employees e', 'e.designation_id = d.designation_id');
        $this->db->join('users u', 'u.employee_id = e.employee_id');
        $this->db->where('u.user_id', $user_id);
        return $this->db->get()->row();
    }
    
    public function getDesignationsByUserAccessLevel($designation_id){
        $sql = "SELECT * FROM designations WHERE access_level > (
                    SELECT d.access_level FROM designations d
                    WHERE designation_id = " . $designation_id . ")";
        $query = $this->db->query($sql);
        return $query->result();
    }
	
	*/
}
