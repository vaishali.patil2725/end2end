<?php defined('BASEPATH') or exit('No direct script access allowed');

class EmployeeModel extends CI_Model
{
    protected $employee_id;
    protected $full_name;
    protected $email;
    protected $phone;
    protected $address;
    protected $city;
    protected $country;
    protected $zip;
    protected $designation_id;
    protected $company_id;
    protected $unit_id;
    protected $department_id;
    protected $status;
    protected $created_by;
    protected $created_on;	
	
    public function getEmployees($employee_id = NULL, $company_id = NULL, $unit_id = NULL)
    {
        $this->db->select('e.*, ur.role, d.user_role, us.username, us.user_id, d.designation, c.name as company, u.unit_name as unit');
        $this->db->from('employees e');
        $this->db->join('users us', 'us.employee_id = e.employee_id', 'left');
		$this->db->join('designations d', 'd.designation_id = e.designation_id', 'left');
        $this->db->join('user_roles ur', 'ur.id = d.user_role', 'left');
        $this->db->join('companies c', 'c.company_id = e.company_id', 'left');
        $this->db->join('units u', 'u.unit_id = e.unit_id', 'left');
		
		if($company_id != NULL){
            $this->db->where('e.company_id', $company_id);
        }

        if($unit_id != NULL && $unit_id != 0){
            $this->db->where('e.unit_id', $unit_id);
        }

        if($employee_id != NULL){
            $this->db->where('e.employee_id !=', $employee_id);
        }
        $this->db->where('e.deleted_on', NULL);
       
        return $this->db->get()->result();
		
    }

    public function getEmployeeByUserId($user_id){
        $this->db->select('e.*');
        $this->db->from('employees e');
        $this->db->join('users u', 'u.employee_id = e.employee_id');
        $this->db->where('u.user_id', $user_id);
        return $this->db->get()->row();
    }

    public function getEmployeeByEmployeeId($employee_id){
        $this->db->select('e.*, d.designation, c.name as company, u.unit_name as unit, u2.full_name as added_by');
        $this->db->from('employees e');
        $this->db->join('designations d', 'd.designation_id = e.designation_id', 'left');
        $this->db->join('companies c', 'c.company_id = e.company_id', 'left');
        $this->db->join('units u', 'u.unit_id = e.unit_id', 'left');
		$this->db->join('users u2', 'u2.user_id = e.created_by', 'left');
        $this->db->where('e.employee_id', $employee_id);
        return $this->db->get()->row();
    }

    public function getFactoryManagerEmployees($designation_id){
        $this->db->select('e.employee_id, e.full_name');
        $this->db->from('employees e');
        $this->db->where('e.deleted_on', NULL);
        //$this->db->where('e.designation_id', $designation_id);
        return $this->db->get()->result();
    }

	
    public function getAllCompanyManagers($role){
        $this->db->select('e.full_name, e.employee_id');
        $this->db->from('employees e');		
		$this->db->join('users u', 'u.employee_id = e.employee_id');
		$this->db->join('user_roles role', 'role.id = u.user_role');
        $this->db->where('role.id', $role);
        return $this->db->get()->result();
    }

    public function addNewEmployee($data)
    {
        return $this->db->insert('employees', $data);
    }

    public function updateEmployee($data){
        $this->db->where('employee_id', $data['employee_id']);
        return $this->db->update('employees',$data);
    }

    public function deleteEmployee($employee_id, $user_id){
        $this->db->set('deleted_by', $user_id);
        $this->db->set('deleted_on', date('Y-m-d H:i:s'));
        $this->db->where('employee_id', $employee_id);
        return $this->db->update('employees');
    }
	
	public function UpdateEmployeeData($data,$id)
	{
		$b=$data['username'];
		unset($data['username']);
		
		$this->db->where('employee_id',$id);
		 $this->db->update('employees',$data);
		$this->db->where('employee_id',$id);
		return $this->db->update('users',array('username'=>$b));
	}
	
	public function CheckOldPassword($data,$id)
	{
		$this->db->where('user_id',$id);
		$this->db->where('password',$data['old_password']);
		if(!empty($this->db->get('users')->row()))
		{
			$this->db->where('user_id',$id);
        return $this->db->update('users',array('password'=>$data['password']));
		}else{
			return 0;
		}
		
	}

	/************************ fucntion to delete ******************************************/
	
	
    /*public function getEmployeesByCompanyId($companyId){
        $this->db->select('e.*, d.designation, c.name as company, u.unit_name as unit, dept.dept_name as department');
        $this->db->from('employees e');
        $this->db->join('designations d', 'd.designation_id = e.designation_id', 'left');
        $this->db->join('companies c', 'c.company_id = e.company_id', 'left');
        $this->db->join('units u', 'u.unit_id = e.unit_id', 'left');
        $this->db->join('departments dept', 'dept.dept_id = e.department_id', 'left');
        if($searchFilter != NULL){
            $this->db->where("(e.full_name LIKE '%".$searchFilter."%' OR e.email LIKE '%".$searchFilter."%')", NULL, FALSE);
        }

        if($user->designation_id == DesignationModel::HR){
            $this->db->where('e.company_id', $user->company_id);
            $this->db->where('e.employee_id !=', $user->employee_id);
        }

       /* if ($user->role != 'admin') {
            $this->db->where('e.company_id', $user->company_id);
            $this->db->where('e.employee_id !=', $user->employee_id);
        }
        if ($user->role == 'user') {
            $this->db->where('e.department_id', $user->department_id);
        }

        if ($user->role == 'superuser' && $user->designation == 'Garment Manager') {
            $this->db->where('e.unit_id', $user->unit_id);
        }
		
        $this->db->where('e.deleted_on', NULL);
       
         $this->db->get()->result();
         print_r($this->db->last_query());
         die;
    }
	
	
    public function getHelpers($company_id){
        $this->db->select('e.full_name, e.employee_id');
        $this->db->from('employees e');
        $this->db->join('designations d', 'd.designation_id = e.designation_id');
        $this->db->where('designation', 'Helper');
        return $this->db->get()->result();
    }
	
	public function getEmployeesByDesignation($unit_id = NULL, $designation_id){
        $this->db->select('e.full_name, e.employee_id');
        $this->db->from('employees e');
        $this->db->join('designations d', 'd.designation_id = e.designation_id');
        $this->db->where('e.designation_id', $designation_id);

        if($unit_id != NULL){
            $this->db->where('e.unit_id', $unit_id);
        }
        return $this->db->get()->result();
    }
	
	 public function getDepartmentHead($company_id, $unit_id, $department_id, $designation_id){
        $this->db->select('e.full_name, e.employee_id');
        $this->db->from('employees e');
        $this->db->where('company_id', $company_id);
        $this->db->where('unit_id', $unit_id);
        $this->db->where('department_id', $department_id);
        $this->db->where('designation_id', $designation_id);
        return $this->db->get()->row();
    }
	*/


}
