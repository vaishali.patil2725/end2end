<?php defined('BASEPATH') or exit('No direct script access allowed');

class UnitsModel extends CI_Model
{
    protected $table = 'units';
   
    
    public function getUnits($company_id = NULL){
        $this->db->select('u.*, e.full_name as manager, c.name as company, users.full_name as created_by');
        $this->db->from('units u');
        $this->db->join('employees e', 'u.manager_employee_id = e.employee_id', 'left');
        $this->db->join('companies c', 'u.company_id = c.company_id', 'left');
        $this->db->join('users', 'users.user_id = u.created_by', 'left');
        if($company_id != NULL){
            $this->db->where('u.company_id', $company_id);
        }
        //$this->db->join('employees e1', 'u.unit_id = e1.unit_id and e1.department_id = 2', 'left');
        //$this->db->join('employees e2', 'u.unit_id = e2.unit_id and e2.department_id = 3', 'left');
        return $this->db->get()->result();
    }

    public function getUnitsByCompanyId($company_id, $is_manager = NULL){
        $this->db->select('*');
        $this->db->from('units');
        if($is_manager != NULL){
            $this->db->where('manager_employee_id =', NULL);
        }
        $this->db->where('company_id', $company_id);
        return $this->db->get()->result();
    }

    public function addNewUnit($data){
        return $this->db->insert('units', $data);
    }

    public function updateUnit($data){
        $this->db->where('unit_id', $data['unit_id']);
        return $this->db->update('units', $data);
    }
	
	/*************************** functions to delete **********************************/
	
	/*public function getUnitsByCompanyIdByUnitId($company_id = NULL, $unit_id = NULL){
        $this->db->select('*');
        $this->db->from('units');

        if($company_id != NULL){
            $this->db->where('company_id', $company_id);
        }

        if($unit_id != NULL){
            $this->db->where('unit_id', $unit_id);
        }

        return $this->db->get()->result();
    }*/
}
