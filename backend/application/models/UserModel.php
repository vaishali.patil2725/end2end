<?php defined('BASEPATH') OR exit('No direct script access allowed');

class UserModel extends CI_Model
{
    protected $table = 'users';
	
	public function getUserInfo($id) {
        $this->db->select('u.username, u.user_role, e.*, d.designation, c.name as company, un.unit_name as unit');
        $this->db->from('users u');
        $this->db->join('employees e', 'u.employee_id = e.employee_id', 'left');
        $this->db->join('designations d', 'd.designation_id = e.designation_id', 'left');
        $this->db->join('companies c', 'c.company_id = e.company_id', 'left');
        $this->db->join('units un', 'un.unit_id = e.unit_id', 'left');
		$this->db->where('u.user_id', $id);
        return $this->db->get()->row();	
          $this->db->last_query();
    }

    public function getUserInfoById($user_id){
        $this->db->select('u.*, e.company_id, e.unit_id, e.department_id, e.designation_id');
        $this->db->from('users u');
        $this->db->join('employees e', 'u.employee_id = e.employee_id', 'left');
		$this->db->where('u.user_id', $user_id);
        return $this->db->get()->row();	
    }

    public function getLoggedInEmployeeInfoByUserId($id) {
        $this->db->select('e.*');
        $this->db->from('users u');
        $this->db->join('employees e', 'u.employee_id = e.employee_id', 'left');
		$this->db->where('u.user_id', $id);
		return $this->db->get()->row();	
    }
    
    public function addUser($data){
        if($this->db->insert($this->table, $data) == true){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }
	
    public function updateUser($data){
        $this->db->where('user_id', $data['user_id']);
        return $this->db->update('users',$data);
    }
}
