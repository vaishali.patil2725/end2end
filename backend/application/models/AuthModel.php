<?php defined('BASEPATH') OR exit('No direct script access allowed');

class AuthModel extends CI_Model
{
    protected $user_table = 'users';
	/**
     * User Login
     * ----------------------------------
     * @param: username or email address
     * @param: password
     */
	 
    public function userLogin($username, $password)
    {
        $result = $this->db->get_where('users', ['username' => $username, 'password' => $password, 'status'=> 1]);
		return $result->row();
    }
	
	public function getUserById($id) {
        $this->db->select('*');
		$this->db->from('users');
		$this->db->where('user_id', $id);
		return $this->db->get()->result();	
    }
	
}
