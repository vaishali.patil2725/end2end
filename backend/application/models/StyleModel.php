<?php defined('BASEPATH') OR exit('No direct script access allowed');

class StyleModel extends CI_Model
{
    protected $table = 'styles';

    public function getStyles($company = NULL){
        $this->db->select('s.*, u.full_name as created_by, b.brand_name, c.name as company');
        $this->db->from('styles s');
        $this->db->join('brands b', 's.brand=b.brand_id', 'left');
        $this->db->join('companies c', 'c.company_id=s.company', 'left');
        $this->db->join('users u', 'u.user_id=s.created_by');

		if($company != NULL){
			$this->db->where('s.company', $company);
		}
		return $this->db->get()->result();
    }
	
    public function updateStyle($data){
        $this->db->where('style_id', $data['style_id']);
        return $this->db->update($this->table,$data);
    }
    
    public function addNewStyle($data){
        return $this->db->insert($this->table, $data);
    }

    public function getStyleById($style_id){
        $this->db->select('s.*, u.full_name as created_by, b.brand_name');
        $this->db->from('styles s');
        $this->db->join('brands b', 's.brand=b.brand_id');
        $this->db->join('users u', 'u.user_id=s.created_by');
        $this->db->where('s.style_id', $style_id);
		return $this->db->get()->row();
    }
}
