<?php defined('BASEPATH') OR exit('No direct script access allowed');

class CompanyModel extends CI_Model
{
    protected $table = 'companies';
	
	public function getCompaniesList() {
        $this->db->select('c.*, e.full_name');
        $this->db->from('companies c');
        $this->db->join('employees e', 'c.manager_employee_id = e.employee_id', 'left');
		return $this->db->get()->result();	
    }

    public function addNewCompany($data){
         $this->db->insert($this->table, $data);
         return $this->db->insert_id();
    }
    
    public function updateCompany($data){
        $this->db->where('company_id', $data['company_id']);
        return $this->db->update('companies',$data);
    }

    public function getCompanyDetailsByCompanyId($company_id){
        $this->db->select('e.full_name');
        $this->db->from('companies c');
        $this->db->join('employees e', 'c.manager_employee_id = e.employee_id');
        $this->db->where('c.company_id', $company_id);
        $this->db->where('e.deleted_on !=', NULL);
        return $this->db->get()->row();	
    }
}
