<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400'); 
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
		exit(0);
}
class Company extends CI_Controller
{
	
    public function __construct() {
        parent::__construct();
		$this->load->library('session');
		$this->load->model('CompanyModel');
		$this->load->model('EmployeeModel');
		$this->getLoggedInUser();
    }
	
	public function getCompaniesList(){
		$companies = $this->CompanyModel->getCompaniesList();
		echo json_encode($companies);
	}

	public function addorEditCompany(){
		$company = (array) json_decode(file_get_contents("php://input"), true);
		
		if($company['company_id']==''){
			$res = $this->CompanyModel->addNewCompany($company);
			if( $res == false ) {
				echo json_encode(0);
				return;
			}
			$this->EmployeeModel->updateEmployee(['employee_id'=>$company['manager_employee_id'], 'company_id'=>$res]);
		}else{
			if( $this->CompanyModel->updateCompany($company) == false ) {
				echo json_encode(0);
				return;
			}
			$this->EmployeeModel->updateEmployee(['employee_id'=>$company['manager_employee_id'], 'company_id'=>$company['company_id']]);
		}
		
		echo json_encode(1);
	}

	/******************************Other Functions******************************************/
	public function getLoggedInUser(){
		$user_token = $this->authorization_token->validateToken();
		if($user_token['status'] == false){
			echo json_encode(0);
			return;
		}

		$this->user_id = $user_token['data']->id;
		$this->role = $user_token['data']->role;

		$this->objCurrentEmployee = $this->EmployeeModel->getEmployeeByUserId($this->user_id);

	}
}