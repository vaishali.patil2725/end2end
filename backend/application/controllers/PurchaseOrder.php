<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400'); 
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
		exit(0);
}
class PurchaseOrder extends CI_Controller
{
	protected $user_id;
	protected $role;
	protected $designation;
	protected $objCurrentEmployee;

    public function __construct() {
        parent::__construct();
		$this->load->library('session');
		$this->load->model('PurchaseOrderModel');
		$this->load->model('EmployeeModel');
		$this->load->model('BuyerModel');
		$this->load->model('StyleModel');
		$this->load->model('CompanyModel');
		$this->getLoggedInUser();
    }

	public function getRequiredInitialData(){
		$data['companies'] = $this->CompanyModel->getCompaniesList();
		$data['buyers'] = $this->BuyerModel->getBuyers();
		$data['styles'] = $this->StyleModel->getStyles($this->objCurrentEmployee->company_id);
		echo json_encode($data);
	}
	
	public function getFilteredPurchaseOrders(){
		$data = (array) json_decode(file_get_contents("php://input"), true);
		$orders = $this->PurchaseOrderModel->getFilteredPurchaseOrders($data);
		echo json_encode($orders);
	}
	
    public function getPurchaseOrders(){
		$purchase_orders = $this->PurchaseOrderModel->getPurchaseOrders();
		echo json_encode($purchase_orders);
	}

	public function addorEditPurchaseOrder(){
		$po = (array) json_decode(file_get_contents("php://input"), true);
		if($po['order_id'] == ''){
			$po['company_id'] = $this->objCurrentEmployee->company_id;
			$po['created_by'] = $this->user_id;
			if( $this->PurchaseOrderModel->addNewPurchaseOrder($po) == false ) {
				echo json_encode(0);
				return;
			}
		}else{
			$data = (array) json_decode(file_get_contents("php://input"), true);
			if( $this->PurchaseOrderModel->updatePurchaseOrder($data) == false ) {
				echo json_encode(0);
				return;
			}
		}
		
		echo json_encode(1);
	}
		
	public function getPurchaseOrderDetails(){
		$data = (array) json_decode(file_get_contents("php://input"), true);
		$purchase_order = $this->PurchaseOrderModel->getPurchaseOrderById($data['order_id']);
		echo json_encode($purchase_order);
	}

	/****************************** Dispatch Lots ********************************/
	public function getDispatchLots(){
		$dispatch_lots = $this->PurchaseOrderModel->getDispatchLots($this->objCurrentEmployee->company_id, $this->objCurrentEmployee->unit_id);
		echo json_encode($dispatch_lots);
	}
	
	public function addorEditDispatchLot(){
		$lot = (array) json_decode(file_get_contents("php://input"), true);
		if($lot['dispatch_id'] == ''){
			$lot['created_by'] = $this->user_id ;
			$lot['company'] = $this->objCurrentEmployee->company_id;
			$lot['unit'] = $this->objCurrentEmployee->unit_id;
			if( $this->PurchaseOrderModel->addDispatchLot($lot) == false ) {
				echo json_encode(0);
				return;
			}
		}else{
			$data = (array) json_decode(file_get_contents("php://input"), true);
			if( $this->PurchaseOrderModel->updateDispatchLot($data) == false ) {
				echo json_encode(0);
				return;
			}
	
		}
		echo json_encode(1);
	}

	public function getLotsToAddDeliverySchedule(){
		$data = (array) json_decode(file_get_contents("php://input"), true);
		$lots = $this->PurchaseOrderModel->getDispatchDetailsByErpId($data['erp_id']);
		$ids = array_column($lots, 'lot_number');
		$dispatch_lots = $this->PurchaseOrderModel->getLotsToAddDeliverySchedule($data['order_id'], $ids);
		echo json_encode($dispatch_lots);
	}
	
	public function getCartonContent(){
		$data = (array) json_decode(file_get_contents("php://input"), true);
		$cartons = $this->PurchaseOrderModel->getCartonContent($data['dispatch_id']);
		echo json_encode($cartons);
	}

	public function addorEditCartonContent(){
		$carton = (array) json_decode(file_get_contents("php://input"), true);
		if($carton['carton_id'] == ''){
			$carton['created_by'] = $this->user_id ;
			if( $this->PurchaseOrderModel->addCartonContent($carton) == false ) {
				echo json_encode(0);
				return;
			}
		}else{
			$data = (array) json_decode(file_get_contents("php://input"), true);
			if( $this->PurchaseOrderModel->updateCartonContent($data) == false ) {
				echo json_encode(0);
				return;
			}
	
		}
		echo json_encode(1);
	}
	
	/******************************Other Functions******************************************/
	public function getLoggedInUser(){
		$user_token = $this->authorization_token->validateToken();
		if($user_token['status'] == false){
			echo json_encode(0);
			return;
		}

		$this->user_id = $user_token['data']->id;
		$this->role = $user_token['data']->role;

		$this->objCurrentEmployee = $this->EmployeeModel->getEmployeeByUserId($this->user_id);
	}

}