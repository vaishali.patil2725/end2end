<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400'); 
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
		exit(0);
}
class Employee extends CI_Controller
{
	protected $user_id;
	protected $role;
	protected $designation;
	protected $objCurrentEmployee;

    public function __construct() {
        parent::__construct();
		$this->load->library('session');
		$this->load->model('EmployeeModel');
		$this->load->model('UserModel');
		$this->load->model('DesignationModel');
		$this->load->model('DepartmentModel');
		$this->load->model('CompanyModel');
		$this->load->model('UserRoleModel');
		$this->load->model('UnitsModel');
		$this->load->model('BuyerModel');
		$this->getLoggedInUser();
    }
	
	public function getEmployees(){
		if($this->role == UserRoleModel::ADMIN_ID){
			$employees = $this->EmployeeModel->getEmployees($this->objCurrentEmployee->employee_id);
		}else{
			$employees = $this->EmployeeModel->getEmployees($this->objCurrentEmployee->employee_id, $this->objCurrentEmployee->company_id, $this->objCurrentEmployee->unit_id);
		}

		echo json_encode($employees);
	}
	
	public function getFilteredEmployees(){
		$post_data = (array) json_decode(file_get_contents("php://input"), true);
		if($post_data['company'] != ''){
			$employees = $this->EmployeeModel->getEmployees($this->objCurrentEmployee->employee_id, $post_data['company']);
		}else{
			$employees = $this->EmployeeModel->getEmployees($this->objCurrentEmployee->employee_id, $this->objCurrentEmployee->company_id, $this->objCurrentEmployee->unit_id);
		}

		echo json_encode($employees);
	}

	public function getFactoryManagerEmployees(){
		$employees = $this->EmployeeModel->getFactoryManagerEmployees(DesignationModel::GARMENT_MANAGER_ID);
		echo json_encode($employees);
	}
	
	public function getAllCompanyManagers(){
		$managers = $this->EmployeeModel->getAllCompanyManagers(UserRoleModel::SUPERUSER_ID);
		echo json_encode($managers);
	}

	public function getRequiredFormData(){
		$data['user_roles'] = $this->UserRoleModel->getUserRoles(true);
		if($this->role == UserRoleModel::ADMIN_ID){
			$data['companies'] = $this->CompanyModel->getCompaniesList();
			$data['units'] = $this->UnitsModel->getUnits();
			$data['designations'] = $this->DesignationModel->getAllDesignations();
		}else{
			if($this->objCurrentEmployee->designation_id == DesignationModel::MERCHANT_HEAD_ID){
				$designations = [DesignationModel::MANAGING_DIRECTOR_ID, DesignationModel::MERCHANT_HEAD_ID];
			}else if($this->objCurrentEmployee->designation_id == DesignationModel::GARMENT_MANAGER_ID){
				$designations = [DesignationModel::MANAGING_DIRECTOR_ID, DesignationModel::MERCHANT_HEAD_ID, DesignationModel::GARMENT_MANAGER_ID];
			}else{
				$designations = [DesignationModel::MANAGING_DIRECTOR_ID];
			}
			$data['designations'] = $this->DesignationModel->getAllDesignations($designations);
			$data['units'] = $this->UnitsModel->getUnitsByCompanyId($this->objCurrentEmployee->company_id);
		}
		
		$data['departments'] = $this->DepartmentModel->getDepartments();
		echo json_encode($data);
	}

	public function addorEditEmployee(){
		$employee = (array) json_decode(file_get_contents("php://input"), true);

		if($employee['employee_id']==''){
			$employee_info = $this->UserModel->getLoggedInEmployeeInfoByUserId($this->user_id);
			$employee['created_by'] = $this->user_id;
			if($this->role != 'Admin'){
				$employee['company_id'] = $employee_info->company_id;
			}
			if( $this->EmployeeModel->addNewEmployee($employee) == false ) {
				echo json_encode(0);
				return;
			}
		}else{
			if( $this->EmployeeModel->updateEmployee($employee) == false ) {
				echo json_encode(0);
				return;
			}
		}
		
		echo json_encode(1);
	}
	
	public function getEmployeeInformaton(){
		$employee = (array) json_decode(file_get_contents("php://input"), true);
		$employee_info = $this->EmployeeModel->getEmployeeByEmployeeId($employee['employee_id']);
		echo json_encode($employee_info);
	}

	public function editEmployee(){
		$employee = (array) json_decode(file_get_contents("php://input"), true);
		if( $this->EmployeeModel->editEmployee($employee) == false ) {
			echo json_encode(0);
			return;
		}
		return 1;
	}

	public function deleteEmployee(){
		$employee = (array) json_decode(file_get_contents("php://input"), true);
		$user_token = $this->authorization_token->validateToken();
		if( $this->EmployeeModel->deleteEmployee($employee['employee_id'], $user_token['data']->id) == false ) {
			echo json_encode(0);
			return;
		}
		return 1;
	}

	public function getBuyers(){
		$buyers = $this->BuyerModel->getBuyers();
		echo json_encode($buyers);
	}

	public function createorEditUser(){
		$user = (array) json_decode(file_get_contents("php://input"), true);
		
		if($user['user_id'] == ''){
			$user['created_by'] = $this->user_id;
			$user['user_type'] = 'employee';
			if( $this->UserModel->addUser($user) == false ) {
				echo json_encode(0);
				return;
			}
		}else{
			if( $this->UserModel->updateUser($user) == false ) {
				echo json_encode(0);
				return;
			}
		}
		
	}
	
	public function getEmployeeDetails()
	{
	  echo json_encode($this->EmployeeModel->getEmployeeByUserId($this->user_id));
	}
	
	public function UpdateEmployeeData()
	{
		$data = (array) json_decode(file_get_contents("php://input"), true);
		echo json_encode($this->EmployeeModel->UpdateEmployeeData($data,$this->employee_id));

	}
	
	public function ChangePassword()
	{
		$data = (array) json_decode(file_get_contents("php://input"), true);
		if($this->EmployeeModel->CheckOldPassword($data,$this->user_id))
		{
			echo 1;
		}else{
			echo 0;
		}
	}

	/******************************Other Functions******************************************/
	public function getLoggedInUser(){
		$user_token = $this->authorization_token->validateToken();
		if($user_token['status'] == false){
			echo json_encode(0);
			return;
		}

		$this->user_id = $user_token['data']->id;
		$this->role = $user_token['data']->role;

		$this->objCurrentEmployee = $this->EmployeeModel->getEmployeeByUserId($this->user_id);
	}
}