<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400'); 
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
		exit(0);
}
class UnitLines extends CI_Controller
{
	protected $user_id;
	protected $role;
	protected $designation;
	protected $objCurrentEmployee;

    public function __construct() {
        parent::__construct();
		$this->load->library('session');
		$this->load->model('EmployeeModel');
		$this->load->model('UnitLinesModel');
		$this->getLoggedInUser();
    }
	
	public function getUnitLines(){
		$data = (array) json_decode(file_get_contents("php://input"), true);
        $lines = $this->UnitLinesModel->getUnitLines($data['department'], $this->objCurrentEmployee->company_id, $this->objCurrentEmployee->unit_id);
		echo json_encode($lines);
	}
	
	public function getAllLines(){
        $lines = $this->UnitLinesModel->getAllLines($this->objCurrentEmployee->company_id, $this->objCurrentEmployee->unit_id);
		echo json_encode($lines);
	}

	public function addorEditUnitLine(){
		$line = (array) json_decode(file_get_contents("php://input"), true);
		if($line['line_id']==''){
            $line['company_id'] = $this->objCurrentEmployee->company_id;
           // $line['unit_id'] = $this->objCurrentEmployee->unit_id;
           $line['unit_id'] = 1;
			$line['created_by'] = $this->user_id;
			if( $this->UnitLinesModel->addNewLine($line) == false ) {
				echo json_encode(0);
				return;
			}
		}else{
			if( $this->UnitLinesModel->updateLine($line) == false ) {
				echo json_encode(0);
				return;
			}
		}

		echo json_encode(1);
	}

	public function getUnitsByCompanyId(){
		$data = (array) json_decode(file_get_contents("php://input"), true);
		$units = $this->UnitsModel->getUnitsByCompanyId($data['company_id']);
		echo json_encode($units);
	}

	/******************************Other Functions******************************************/
	public function getLoggedInUser(){
		$user_token = $this->authorization_token->validateToken();
		if($user_token['status'] == false){
			echo json_encode(0);
			return;
		}

		$this->user_id = $user_token['data']->id;
		$this->role = $user_token['data']->role;

		$this->objCurrentEmployee = $this->EmployeeModel->getEmployeeByUserId($this->user_id);
	}
}