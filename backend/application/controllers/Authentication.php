<?php defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400'); 
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
        exit(0);
}
require APPPATH . '/libraries/REST_Controller.php';
 
class Authentication extends \Restserver\Libraries\REST_Controller
{
	
    public function __construct() {
        parent::__construct();
		$this->load->library('session');
		$this->load->model('UserModel');
		$this->load->model('AuthModel');
    }

    /**
     * User Login API
     * --------------------
     * @param: username 
     * @param: password
     * --------------------------
     * @method : POST
     * @link: api/user/login
     */
    public function userLogin_post()
    {
		$_POST= (array) json_decode(file_get_contents("php://input"), true);
        $_POST = $this->security->xss_clean($_POST);
        # Form Validation
		$this->form_validation->set_rules('username', 'username', 'trim|required');
        $this->form_validation->set_rules('password', 'password', 'trim|required');
        if ($this->form_validation->run() == FALSE)
        {
            // Form Validation Errors
            $message = array(
                'status' => false,
                'error' => $this->form_validation->error_array(),
                'message' => validation_errors()
            );

            $this->response($message, REST_Controller::HTTP_NOT_FOUND);
        }
        else
        {		
            $user = $this->AuthModel->userLogin($_POST['username'], $_POST['password']);
            if( $user != '' ) {
                // Load Authorization Token Library
                $this->load->library('Authorization_Token');
                
                $user_info = $this->UserModel->getUserInfoById($user->user_id);
				$token_data['id'] = $user_info->user_id;
                $token_data['username'] = $user_info->username;
                $token_data['role'] = $user_info->user_role;
                $token_data['time'] = time();
				$user_token = $this->authorization_token->generateToken($token_data);

                $user_data = array(
						'token'=>$user_token, 
						'role'=>$user_info->user_role, 
						'company' => $user_info->company_id,
						'unit' => $user_info->unit_id,
						'department' => $user_info->department_id,
						'designation' => $user_info->designation_id,
						);
                echo json_encode($user_data);
			} else {
				echo json_encode(0);
			}
        }
    }
	
	public function getLoggedInUserData_get(){
       
        $user_token = $this->authorization_token->validateToken();
		if( $user_token['status'] == false ) {
			echo json_encode(0);
			return;
        }
		$user_info = $this->UserModel->getUserInfo($user_token['data']->id);
		echo json_encode($user_info);
	}
}