<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400'); 
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
		exit(0);
}
class Machines extends CI_Controller
{
    protected $user_id;
	protected $role;
	protected $designation;
	protected $objCurrentEmployee;
	
    public function __construct() {
        parent::__construct();
		$this->load->library('session');
        $this->load->model('MachineModel');
        $this->load->model('UserModel');
        $this->load->model('EmployeeModel');
        $this->load->model('UserRoleModel');
        $this->getLoggedInUser();
    }
	
	public function getAllMachines(){
		$machines = $this->MachineModel->getAllMachines($this->objCurrentEmployee->company_id, $this->objCurrentEmployee->unit_id);
		echo json_encode($machines);
    }
    
    public function addorEditMachine(){
        $machine = (array) json_decode(file_get_contents("php://input"), true);

        if($machine['machine_id']==''){
            $machine['company'] = $this->objCurrentEmployee->company_id;
			$machine['unit'] = $this->objCurrentEmployee->unit_id;
            $machine['created_by'] = $this->user_id;
			
            if( $this->MachineModel->addNewMachine($machine) == false ) {
                echo json_encode(0);
                return;
            }
        }else{
            if($this->MachineModel->updateMachine($machine) == false){
                echo json_encode(0);
                return;
            } 
        }

		echo json_encode(1);
    }
	
	public function getServiceHistory(){
		$filter = (array) json_decode(file_get_contents("php://input"), true);
		$machine = NULL;
		if(isset($filter['machine'])){
			$machine = $filter['machine'];
		}
		$history = $this->MachineModel->getServiceHistory($this->objCurrentEmployee->company_id, $this->objCurrentEmployee->unit_id, $machine);
		echo json_encode($history);
    }
	
	public function addorEditServiceHostory(){
        $history = (array) json_decode(file_get_contents("php://input"), true);

        if($history['service_id']==''){
            $history['created_by'] = $this->user_id;
			
            if( $this->MachineModel->addServiceHistory($history) == false ) {
                echo json_encode(0);
                return;
            }
        }else{
            if($this->MachineModel->updateServiceHistory($history) == false){
                echo json_encode(0);
                return;
            } 
        }

		echo json_encode(1);
    }
	
	public function validateMachinCode(){
		  $m = (array) json_decode(file_get_contents("php://input"), true);
		  $machine_id = NULL;
		  if(isset($m['serial_number']) && $m['serial_number'] != ''){
			    if($m['machine_id'] != ''){
					$machine_id = $m['machine_id'];
				}

			    if( $this->MachineModel->validateMachinCode($m['serial_number'], $machine_id) == false ) {
					echo json_encode(0);
					return;
				}
		  }
		  echo json_encode(1);
		  return;

	}

    /******************************Other Functions******************************************/
	public function getLoggedInUser(){
		$user_token = $this->authorization_token->validateToken();
		if($user_token['status'] == false){
			echo json_encode(0);
			return;
		}

		$this->user_id = $user_token['data']->id;
		$this->role = $user_token['data']->role;

		$this->objCurrentEmployee = $this->EmployeeModel->getEmployeeByUserId($this->user_id);

	}
}