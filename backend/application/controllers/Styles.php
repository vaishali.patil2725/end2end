<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400'); 
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
		exit(0);
}
class Styles extends CI_Controller
{
    protected $user_id;
	protected $role;
	protected $designation;
	protected $objCurrentEmployee;
	
    public function __construct() {
        parent::__construct();
		$this->load->library('session');
        $this->load->model('StyleModel');
        $this->load->model('EmployeeModel');
        $this->load->model('UserRoleModel');
        $this->getLoggedInUser();
    }
	
	public function getStyles(){
        $styles = $this->StyleModel->getStyles($this->objCurrentEmployee->company_id);
		echo json_encode($styles);
    }
	
	public function getFilteredStyles(){
		$post_data = (array) json_decode(file_get_contents("php://input"), true);
		if($post_data['company'] != ''){
			$styles = $this->StyleModel->getStyles($post_data['company']);
		}else{
			$styles = $this->StyleModel->getStyles();
		}
		echo json_encode($styles);
    }
    
    public function addorEditNewStyle(){
        $style = (array) json_decode(file_get_contents("php://input"), true);

        if($style['style_id']==''){
            $style['company'] = $this->objCurrentEmployee->company_id;
            $style['created_by'] = $this->user_id;
            if( $this->StyleModel->addNewStyle($style) == false ) {
                echo json_encode(0);
                return;
            }
        }else{
            if($this->StyleModel->updateStyle($style) == false){
                echo json_encode(0);
                return;
            } 
        }

		echo json_encode(1);
    }

    public function getStyleDetails(){
        $style = (array) json_decode(file_get_contents("php://input"), true);
        $data = $this->StyleModel->getStyleById($style['style_id']);
        echo json_encode($data);
    }

    /******************************Other Functions******************************************/
	public function getLoggedInUser(){
		$user_token = $this->authorization_token->validateToken();
		if($user_token['status'] == false){
			echo json_encode(0);
			return;
		}

		$this->user_id = $user_token['data']->id;
		$this->role = $user_token['data']->role;

		$this->objCurrentEmployee = $this->EmployeeModel->getEmployeeByUserId($this->user_id);

	}
}