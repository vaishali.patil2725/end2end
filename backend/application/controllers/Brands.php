<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400'); 
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
		exit(0);
}
class Brands extends CI_Controller
{
    protected $user_id;
	protected $role;
	protected $designation;
	protected $objCurrentEmployee;
	
    public function __construct() {
        parent::__construct();
		$this->load->library('session');
        $this->load->model('BrandModel');
        $this->load->model('EmployeeModel');
        $this->load->model('UserRoleModel');
        $this->getLoggedInUser();
    }
	
	public function getBrands(){
        if($this->role == UserRoleModel::ADMIN_ID){
			$brands = $this->BrandModel->getBrands();
        }else{
            $brands = $this->BrandModel->getBrands($this->objCurrentEmployee->company_id);
        }
		echo json_encode($brands);
    }
    
	public function getFilteredBrands(){
		$post_data = (array) json_decode(file_get_contents("php://input"), true);
		if($post_data['company'] != ''){
			$brands = $this->BrandModel->getBrands($post_data['company']);
		}else{
			$brands = $this->BrandModel->getBrands();
		}
		echo json_encode($brands);
	}
	
    public function addorEditBrand(){
        $brand = (array) json_decode(file_get_contents("php://input"), true);
        if($brand['brand_id']==''){
            $brand['company'] = $this->objCurrentEmployee->company_id;
            $brand['created_by'] = $this->user_id;
            if( $this->BrandModel->addNewBrand($brand) == false ) {
                echo json_encode(0);
                return;
            }
        }else{
            if( $this->BrandModel->updateBrand($brand) == false ) {
                echo json_encode(0);
                return;
            }
        }

		echo json_encode(1);
	}

    /******************************Other Functions******************************************/
	public function getLoggedInUser(){
		$user_token = $this->authorization_token->validateToken();
		if($user_token['status'] == false){
			echo json_encode(0);
			return;
		}

		$this->user_id = $user_token['data']->id;
		$this->role = $user_token['data']->role;

		$this->objCurrentEmployee = $this->EmployeeModel->getEmployeeByUserId($this->user_id);

	}
}