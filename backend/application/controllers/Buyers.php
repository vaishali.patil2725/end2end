<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400'); 
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
		exit(0);
}
class Buyers extends CI_Controller
{
    protected $user_id;
	protected $role;
	protected $designation;
	protected $objCurrentEmployee;
	
    public function __construct() {
        parent::__construct();
		$this->load->library('session');
        $this->load->model('BuyerModel');
        $this->load->model('UserModel');
        $this->load->model('EmployeeModel');
        $this->load->model('PurchaseOrderModel');
        $this->load->model('UserRoleModel');
        $this->getLoggedInUser();
    }
	
	public function getBuyers(){
		$buyers = $this->BuyerModel->getBuyers();
		echo json_encode($buyers);
    }
    
    public function addorEditNewBuyer(){
        $buyer = (array) json_decode(file_get_contents("php://input"), true);

        if($buyer['buyer_id']==''){

            $buyer_info = [];
            $buyer_info['full_name'] = $buyer['full_name'];
            $buyer_info['email'] = $buyer['email'];
            $buyer_info['phone'] = $buyer['phone'];
            $buyer_info['address'] = $buyer['address'];
            $buyer_info['city'] = $buyer['city'];
            $buyer_info['country'] = $buyer['country'];
            $buyer_info['zip'] = $buyer['zip'];
            $buyer_info['created_by'] = $this->user_id;

            $buyer_id = $this->BuyerModel->addBuyer($buyer_info);
            if($buyer_id == false){
                echo json_encode(0);
                return;
            }

            $user_info = [];
            $user_info['username'] = $buyer['username'];
            $user_info['password'] = $buyer['username'];
            $user_info['buyer_id'] = $buyer_id;
            $user_info['user_role'] = UserRoleModel::BUYER_ID;
			$user_info['full_name'] = $buyer['full_name'];
			$user_info['created_by'] = $this->user_id;
            $user_info['status'] = 1;

            if( $this->UserModel->addUser($user_info) == false){
                echo json_encode(0);
			    return;
            }
          
        }else{
            unset($buyer['username']);
            unset($buyer['password']);
            if($this->BuyerModel->updateBuyer($buyer) == false){
                echo json_encode(0);
                return;
            } 
        }

		echo json_encode(1);
    }

    public function getBuyerDetails(){
        $buyer = (array) json_decode(file_get_contents("php://input"), true);
        $data['buyer_data'] = $this->BuyerModel->getBuyerById($buyer['buyer_id']);
        $data['purchase_orders'] = $this->PurchaseOrderModel->getPurchaseOrdersByBuyerId($buyer['buyer_id']);
        echo json_encode($data);
    }

    /******************************Other Functions******************************************/
	public function getLoggedInUser(){
		$user_token = $this->authorization_token->validateToken();
		if($user_token['status'] == false){
			echo json_encode(0);
			return;
		}

		$this->user_id = $user_token['data']->id;
		$this->role = $user_token['data']->role;

		$this->objCurrentEmployee = $this->EmployeeModel->getEmployeeByUserId($this->user_id);

	}
}