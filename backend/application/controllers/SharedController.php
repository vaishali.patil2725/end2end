<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400'); 
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
		exit(0);
}
class SharedController extends CI_Controller
{
	protected $user_id;
	protected $role;
	protected $designation;
	protected $objCurrentEmployee;

    public function __construct() {
        parent::__construct();
		$this->load->library('session');
		$this->load->model('EmployeeModel');
		$this->load->model('DesignationModel');
		$this->load->model('DepartmentModel');
		$this->load->model('UserRoleModel');
		$this->getLoggedInUser();
	}
	

	/*********************************Designations****************************/
	public function getDesignations(){
		$designations = $this->DesignationModel->getAllDesignations();
		echo json_encode($designations);
	}

	public function addorEditDesignation(){
		$designation = (array) json_decode(file_get_contents("php://input"), true);

		if($designation['designation_id']==''){
			$designation['created_by'] = $this->user_id;
			if( $this->DesignationModel->addNewDesignation($designation) == false ) {
				echo json_encode(0);
				return;
			}
		}else{
			if( $this->DesignationModel->updateDesignation($designation) == false ) {
				echo json_encode(0);
				return;
			}
		}
		
		echo json_encode(1);
	}

	
	public function deleteDesignation(){
		$designation = (array) json_decode(file_get_contents("php://input"), true);
		if( $this->DesignationModel->deleteDesignation($designation['designation_id'], $this->user_id) == false ) {
			echo json_encode(0);
			return;
		}
		return 1;
	}
	
	/*********************************Departments****************************/
	public function getDepartments(){
		$departments = $this->DepartmentModel->getDepartments();
		echo json_encode($departments);
	}

	public function addorEditDepartment(){
		$department = (array) json_decode(file_get_contents("php://input"), true);

		if($department['dept_id']==''){
			$department['created_by'] = $this->user_id;
			if( $this->DepartmentModel->addNewDepartment($department) == false ) {
				echo json_encode(0);
				return;
			}
		}else{
			if( $this->DepartmentModel->updateDepartment($department) == false ) {
				echo json_encode(0);
				return;
			}
		}
		
		echo json_encode(1);
	}

	
	public function deleteDepartment(){
		$department = (array) json_decode(file_get_contents("php://input"), true);
		if( $this->DepartmentModel->deleteDepartment($department['dept_id'], $this->user_id) == false ) {
			echo json_encode(0);
			return;
		}
		return 1;
	}
	
	/*********************************User Roles****************************/
	public function getUserRoles(){
		$roles = $this->UserRoleModel->getUserRoles(true);
		echo json_encode($roles);
	}

	public function addorEditUserRole(){
		$role = (array) json_decode(file_get_contents("php://input"), true);

		if($role['id']==''){
			$role['created_by'] = $this->user_id;
			if( $this->UserRoleModel->addUserRole($role) == false ) {
				echo json_encode(0);
				return;
			}
		}else{
			if( $this->UserRoleModel->updateUserRole($role) == false ) {
				echo json_encode(0);
				return;
			}
		}
		
		echo json_encode(1);
	}

	
	public function deleteUserRole(){
		$role = (array) json_decode(file_get_contents("php://input"), true);
		if( $this->UserRoleModel->deleteUserRole($role['id'], $this->user_id) == false ) {
			echo json_encode(0);
			return;
		}
		return 1;
	}
	
	/******************************Other Functions******************************************/
	public function getLoggedInUser(){
		$user_token = $this->authorization_token->validateToken();
		if($user_token['status'] == false){
			echo json_encode(0);
			return;
		}

		$this->user_id = $user_token['data']->id;
		$this->role = $user_token['data']->role;

		$this->objCurrentEmployee = $this->EmployeeModel->getEmployeeByUserId($this->user_id);

	}
}