<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400'); 
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
		exit(0);
}
class Production extends CI_Controller
{
	
    public function __construct() {
        parent::__construct();
		$this->load->library('session');
		$this->load->model('ProductionModel');
		$this->load->model('DesignationModel');
        $this->load->model('EmployeeModel');
		$this->getLoggedInUser();
    }
	
	/***********************Trim Inwards*********************************************/
	
	public function getTrimInwardEntries(){
		$data = (array) json_decode(file_get_contents("php://input"), true);
		$res =  $this->ProductionModel->getTrimInwardEntries($data, $this->objCurrentEmployee->company_id, $this->objCurrentEmployee->unit_id);
		echo json_encode($res);
	}
	
	public function getTrimInwardDetails(){
		 $data = (array) json_decode(file_get_contents("php://input"), true);
		 $details = $this->ProductionModel->getTrimInwardDetailsById($data['inward_id']);
		 echo json_encode($details);
	}
	
	public function getTrimOutwardLogData(){
		$data = (array) json_decode(file_get_contents("php://input"), true);
		 $outwards = $this->ProductionModel->getTrimOutwardLogDataByInwardId($data['inward_id']);
		 echo json_encode($outwards);
	}
	
	/***********************Fabric Inwards*********************************************/
	
	public function getFilteredFabricInwards(){
		$data = (array) json_decode(file_get_contents("php://input"), true);
		$res =  $this->ProductionModel->getFilteredFabricInwards($data, $this->objCurrentEmployee->company_id, $this->objCurrentEmployee->unit_id);
		echo json_encode($res);
	}
	
	public function getFabricInwardDetails(){
		 $data = (array) json_decode(file_get_contents("php://input"), true);
		 $details = $this->ProductionModel->getFabricInwardDetailsById($data['inward_id']);
		 echo json_encode($details);
	}
	
	public function getFabricOutwardLogData(){
		$data = (array) json_decode(file_get_contents("php://input"), true);
		 $outwards = $this->ProductionModel->getFabricutwardLogDataByInwardId($data['inward_id']);
		 echo json_encode($outwards);
	}
	
	/***********************Common Trim / Fabric Inwards*********************************************/
	public function addorUpdateInwardEntry(){
		$data = (array) json_decode(file_get_contents("php://input"), true);
		if($data['inward_id'] == ''){
			$data['company'] = $this->objCurrentEmployee->company_id;
			$data['unit'] = $this->objCurrentEmployee->unit_id;
			$data['created_by'] = $this->user_id;
			if( $this->ProductionModel->addInwardEntry($data) == false ) {
				echo json_encode(0);
				return;
			}	
		}else{
			if( $this->ProductionModel->updateInwardEntry($data) == false ) {
				echo json_encode(0);
				return;
			}
		}
		
		echo json_encode(1);
	}
	
	public function addorEditOutwardEntry(){
		$data = (array) json_decode(file_get_contents("php://input"), true);
		if($data['outward_id'] == ''){
			$data['created_by'] = $this->user_id;
			if( $this->ProductionModel->addOutwardEntry($data) == false ) {
				echo json_encode(0);
				return;
			}	
		}else{
			if( $this->ProductionModel->updateOutwardEntry($data) == false ) {
				echo json_encode(0);
				return;
			}
		}
		
		echo json_encode(1);
	}
	
	
	public function getOutwardLogData(){
		if($this->objCurrentEmployee->designation_id == DesignationModel::TRIM_HEAD_ID){
			$inward_table = 'trim_inward_log';
			$outward_table = 'trim_outward_log';
		}else{
			$inward_table = 'fabric_inward_log';
			$outward_table = 'fabric_outward_log';
		}
		$data = $this->ProductionModel->getOutwardLogData($this->objCurrentEmployee->unit_id, $inward_table, $outward_table);
		echo json_encode($data);
	}
	
	/***********************************Fabric Inspection ************************************/
	
	public function addorEditFabricInspectionEntry(){
		$data = (array) json_decode(file_get_contents("php://input"), true);
		if($data['inspection_id'] == ''){
			$data['company'] = $this->objCurrentEmployee->company_id;
			$data['unit'] = $this->objCurrentEmployee->unit_id;
			$data['created_by'] = $this->user_id;
			if( $this->ProductionModel->addFabricInspectionEntry($data) == false ) {
				echo json_encode(0);
				return;
			}	
		}else{
			if( $this->ProductionModel->updateFabricInspectionEntry($data) == false ) {
				echo json_encode(0);
				return;
			}
		}
		
		echo json_encode(1);
	}
	
	public function getFabricInspectionLogs(){
		$data = (array) json_decode(file_get_contents("php://input"), true);
		$res = $this->ProductionModel->getFabricInspectionLogs($data, $this->objCurrentEmployee->unit_id);
		echo json_encode($res);
	}
	
	
	/************************************Production Data Entries*****************************/
	public function addorEditProductionData(){
		$data = (array) json_decode(file_get_contents("php://input"), true);
		if($data['id'] == ''){
			$data['datetime'] = date('Y-m-d H:i:s');
			$data['company'] = $this->objCurrentEmployee->company_id;
			$data['unit'] = $this->objCurrentEmployee->unit_id;
			$data['created_by'] = $this->user_id;
			if( $this->ProductionModel->addProductionData($data) == false ) {
				echo json_encode(0);
				return;
			}	
		}else{
			if( $this->ProductionModel->updateProductionData($data) == false ) {
				echo json_encode(0);
				return;
			}
		}
		
		echo json_encode(1);
	}
	
	public function getProductionData(){
		$data = (array) json_decode(file_get_contents("php://input"), true);;
		$data = $this->ProductionModel->getProductionData($data, $this->objCurrentEmployee->unit_id);
		echo json_encode($data);
	}
	
	/****************************************other required functions****************************/
	public function getItemCodes(){
		$data = (array) json_decode(file_get_contents("php://input"), true);
		$items = $this->ProductionModel->getItemCodesByRequiremenTypeByErpId($data['requirement_type'], $data['erp_id']);
		echo json_encode($items);
	}
	
	/******************************Other Functions******************************************/
	public function getLoggedInUser(){
		$user_token = $this->authorization_token->validateToken();

		if($user_token['status'] == false){
			echo json_encode(0);
			return;
		}

		$this->user_id = $user_token['data']->id;
		$this->role = $user_token['data']->role;

		$this->objCurrentEmployee = $this->EmployeeModel->getEmployeeByUserId($this->user_id);

	}
}