<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400'); 
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
		exit(0);
}
class ReportsController extends CI_Controller
{
    protected $user_id;
	protected $role;
	protected $designation;
	protected $objCurrentEmployee;
	
    public function __construct() {
        parent::__construct();
		$this->load->library('session');
        $this->load->model('UserModel');
        $this->load->model('ProductionModel');
		$this->load->model('EmployeeModel');
		$this->load->model('DepartmentModel');
        $this->getLoggedInUser();
    }
	
	public function getReportsData(){
		$filters = (array) json_decode(file_get_contents("php://input"), true);
		if($filters['report_type'] == 'cutting_production_report'){
		  $data = $this->ProductionModel->getReportProductionData('cutting_production_data', $filters);
		}elseif($filters['report_type'] == 'sewing_production_report'){
		  $data = $this->ProductionModel->getReportProductionData('sewing_production_data', $filters);
		}elseif($filters['report_type'] == 'finishing_production_report'){
		  $data = $this->ProductionModel->getReportProductionData('finishing_production_data', $filters);
		}elseif($filters['report_type'] == 'kajabutton_production_report'){
		  $data = $this->ProductionModel->getReportProductionData('kaja_button_production_data', $filters);
		}elseif($filters['report_type'] == 'fusing_production_report'){
		  $data = $this->ProductionModel->getReportProductionData('fusing_production_data', $filters);
		}elseif($filters['report_type'] == 'trim_inward_report'){
		 $data =  $this->ProductionModel->getTrimInwardEntriesReport($filters);
		}elseif($filters['report_type'] == 'trim_outward_report'){
			$inward_table = 'trim_inward_log';
			$outward_table = 'trim_outward_log';
			$data = $this->ProductionModel->getOutwardLogDataReport($inward_table, $outward_table, $filters);
		}elseif($filters['report_type'] == 'fabric_inward_report'){
		    $data = $this->ProductionModel->getFabricInwardsReport($filters);
		}elseif($filters['report_type'] == 'fabric_outward_report'){
		    $inward_table = 'fabric_inward_log';
			$outward_table = 'fabric_outward_log';
			$data = $this->ProductionModel->getOutwardLogDataReport($inward_table, $outward_table, $filters);
		}elseif($filters['report_type'] == 'fabric_inspection_report'){
		  $data = $this->ProductionModel->getFabricInspectionLogsReport($filters);
		}
		echo json_encode($data);
    }
    
    /******************************Other Functions******************************************/
	public function getLoggedInUser(){
		$user_token = $this->authorization_token->validateToken();
		if($user_token['status'] == false){
			echo json_encode(0);
			return;
		}

		$this->user_id = $user_token['data']->id;
		$this->role = $user_token['data']->role;

		$this->objCurrentEmployee = $this->EmployeeModel->getEmployeeByUserId($this->user_id);

	}
}