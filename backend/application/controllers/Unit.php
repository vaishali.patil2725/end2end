<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400'); 
}
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
		exit(0);
}
class Unit extends CI_Controller
{
	protected $user_id;
	protected $role;
	protected $designation;
	protected $objCurrentEmployee;

    public function __construct() {
        parent::__construct();
		$this->load->library('session');
		$this->load->model('EmployeeModel');
		$this->load->model('UnitsModel');
		$this->load->model('UserRoleModel');
		$this->getLoggedInUser();
    }
	
	public function getUnits(){
		if($this->role == UserRoleModel::ADMIN_ID){
			$units = $this->UnitsModel->getUnits();
        }else{
            $units = $this->UnitsModel->getUnits($this->objCurrentEmployee->company_id);
        }
		echo json_encode($units);
	}
	
	public function getFilteredUnits(){
		$post_data = (array) json_decode(file_get_contents("php://input"), true);
		if($post_data['company'] != ''){
			$units = $this->UnitsModel->getUnits($post_data['company']);
        }else{
            $units = $this->UnitsModel->getUnits($this->objCurrentEmployee->company_id);
        }
		echo json_encode($units);
	}

	public function addorEditNewUnit(){
		$unit = (array) json_decode(file_get_contents("php://input"), true);
		if($unit['unit_id']==''){
			$unit['company_id'] = $this->objCurrentEmployee->company_id;
			$unit['created_by'] = $this->user_id;
			if( $this->UnitsModel->addNewUnit($unit) == false ) {
				echo json_encode(0);
				return;
			}
		}else{
			if( $this->UnitsModel->updateUnit($unit) == false ) {
				echo json_encode(0);
				return;
			}
		}

		echo json_encode(1);
	}

	public function getUnitsByCompanyId(){
		$data = (array) json_decode(file_get_contents("php://input"), true);
		$units = $this->UnitsModel->getUnitsByCompanyId($data['company_id']);
		echo json_encode($units);
	}

	/******************************Other Functions******************************************/
	public function getLoggedInUser(){
		$user_token = $this->authorization_token->validateToken();
		if($user_token['status'] == false){
			echo json_encode(0);
			return;
		}

		$this->user_id = $user_token['data']->id;
		$this->role = $user_token['data']->role;

		$this->objCurrentEmployee = $this->EmployeeModel->getEmployeeByUserId($this->user_id);
	}
}