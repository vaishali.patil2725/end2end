-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 14, 2020 at 06:42 AM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `end2end`
--

-- --------------------------------------------------------

--
-- Table structure for table `bottom_sizes`
--

CREATE TABLE IF NOT EXISTS `bottom_sizes` (
  `id` int(11) NOT NULL,
  `size` int(100) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bottom_sizes`
--

INSERT INTO `bottom_sizes` (`id`, `size`, `created_on`) VALUES
(1, 1, '2019-12-28 18:27:54'),
(2, 2, '2019-12-28 18:27:54'),
(3, 3, '2019-12-28 18:28:19'),
(4, 4, '2019-12-28 18:28:19'),
(5, 5, '2019-12-28 18:28:26'),
(6, 6, '2019-12-28 18:28:26'),
(7, 7, '2019-12-28 18:28:34'),
(8, 8, '2019-12-28 18:28:34'),
(9, 9, '2019-12-28 18:28:42'),
(10, 10, '2019-12-28 18:28:42'),
(11, 11, '2019-12-28 18:28:49'),
(12, 12, '2019-12-28 18:28:49'),
(13, 13, '2019-12-28 18:28:58'),
(14, 14, '2019-12-28 18:28:58');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE IF NOT EXISTS `brands` (
  `brand_id` int(11) NOT NULL,
  `brand_name` varchar(100) NOT NULL,
  `company` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`brand_id`, `brand_name`, `company`, `created_by`, `created_on`) VALUES
(2, 'PQR ', 1, 2, '2019-12-29 05:55:16'),
(3, 'ABC2', 1, 2, '2019-12-29 05:57:47'),
(4, 'BRAND1', 1, 2, '2019-12-30 12:44:41'),
(5, 'SL Software Solutions', 1, 2, '2020-01-18 20:42:32'),
(6, 'Test', 5, 2, '2020-01-25 06:15:56');

-- --------------------------------------------------------

--
-- Table structure for table `buyers`
--

CREATE TABLE IF NOT EXISTS `buyers` (
  `buyer_id` int(11) NOT NULL,
  `full_name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` int(15) DEFAULT NULL,
  `address` varchar(1000) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `zip` int(6) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `buyers`
--

INSERT INTO `buyers` (`buyer_id`, `full_name`, `email`, `phone`, `address`, `city`, `country`, `zip`, `created_by`, `created_on`) VALUES
(1, 'Buyer 1', 'b1@gmail.com', 2147483647, 'Kharadi', 'Pune', 'India', 411023, 2, '2020-02-20 09:39:11'),
(2, 'Buyer 2', 'b2@gmail.com', 795457656, 'Kharadi', 'Pune', 'India', 411045, 2, '2020-02-27 08:36:58');

-- --------------------------------------------------------

--
-- Table structure for table `carton_content`
--

CREATE TABLE IF NOT EXISTS `carton_content` (
  `carton_id` int(11) NOT NULL,
  `carton_number` int(11) NOT NULL,
  `dispatch_no` int(11) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `carton_content`
--

INSERT INTO `carton_content` (`carton_id`, `carton_number`, `dispatch_no`, `size`, `quantity`, `created_by`, `created_on`) VALUES
(1, 567, 2, 100, 400, 14, '2020-03-01 08:11:07'),
(2, 58, 0, 100, 300, 14, '2020-03-01 08:14:50'),
(3, 456, 2, 400, 500, 14, '2020-03-01 08:22:11'),
(4, 4566, 2, 40066, 5006, 14, '2020-03-01 08:22:21');

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE IF NOT EXISTS `companies` (
  `company_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `number` varchar(100) DEFAULT NULL,
  `manager_employee_id` int(11) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `zip` int(6) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`company_id`, `name`, `number`, `manager_employee_id`, `address`, `city`, `country`, `zip`) VALUES
(1, 'Punit Creation', 'ASD234', 13, 'Thane1', 'Mumbai', 'India', 411024),
(5, 'SL Software solutions', '345', 3, 'Kharadi', 'Pune', 'India', 411014),
(6, 'End 2 End', '345', 1, 'Kharadi', 'Pune', 'India', 411014);

-- --------------------------------------------------------

--
-- Table structure for table `cutting_production_data`
--

CREATE TABLE IF NOT EXISTS `cutting_production_data` (
  `id` int(11) NOT NULL,
  `erp` int(11) NOT NULL,
  `company` int(11) NOT NULL,
  `unit` int(11) NOT NULL,
  `section` varchar(100) DEFAULT NULL,
  `line` int(11) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `production_hour` varchar(100) DEFAULT NULL,
  `output` varchar(100) DEFAULT NULL,
  `machineries` int(11) DEFAULT NULL,
  `helpers` int(11) DEFAULT NULL,
  `cutters` int(11) DEFAULT NULL,
  `layerers` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cutting_production_data`
--

INSERT INTO `cutting_production_data` (`id`, `erp`, `company`, `unit`, `section`, `line`, `datetime`, `production_hour`, `output`, `machineries`, `helpers`, `cutters`, `layerers`, `created_by`, `created_on`) VALUES
(1, 1, 1, 1, 'cutting', 2, '2020-02-20 15:53:30', '12:00 - 1:00 PM', 'Test Output', 10, 10, 10, 0, 7, '2020-02-20 15:10:14'),
(2, 1, 1, 1, 'layering', 2, '2020-02-20 15:57:46', '12:00 - 1:00 PM', 'Test Output', 20, 20, 0, 20, 7, '2020-02-20 14:57:46'),
(3, 1, 1, 1, 'layering', 3, '2020-02-20 16:32:53', '12:00 - 1:00 PM', 'Test Output', 20, 20, 0, 20, 7, '2020-02-20 15:32:53'),
(4, 1, 1, 1, 'layering', 4, '2020-02-20 16:32:58', '12:00 - 1:00 PM', 'Test Output', 20, 20, 0, 20, 7, '2020-02-20 15:32:58');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE IF NOT EXISTS `departments` (
  `dept_id` int(11) NOT NULL,
  `dept_name` varchar(100) NOT NULL,
  `description` text,
  `created_by` int(11) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`dept_id`, `dept_name`, `description`, `created_by`, `created_on`) VALUES
(3, 'Trim', 'Trim', 1, '2020-02-17 06:47:10'),
(4, 'Fabric', 'Fabric', 1, '2020-02-17 06:47:16'),
(5, 'Cutting', 'Cutting', 1, '2020-02-17 06:47:33'),
(6, 'Sewing', 'Sewing', 1, '2020-02-17 06:47:39'),
(7, 'Finishing', 'Finishing', 1, '2020-02-17 06:47:50'),
(8, 'Kaja-Buttoning	', 'Kaja-Buttoning	', 1, '2020-02-17 06:47:56'),
(9, 'Fusing', 'Fusing', 1, '2020-02-17 06:48:02'),
(10, 'Dispatch', 'Dispatch', 1, '2020-02-17 06:48:08'),
(11, 'Mechanical', 'Mechanical', 1, '2020-03-07 06:14:19');

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE IF NOT EXISTS `designations` (
  `designation_id` int(11) NOT NULL,
  `designation` varchar(100) NOT NULL,
  `user_role` int(11) DEFAULT NULL,
  `department` int(11) DEFAULT NULL,
  `description` text,
  `created_by` int(11) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`designation_id`, `designation`, `user_role`, `department`, `description`, `created_by`, `created_on`) VALUES
(1, 'System Head', 1, 1, 'Admin of System', 2, '2020-02-05 12:33:56'),
(2, 'Managing Director', 2, 1, 'Company managing Director ( MD )', 2, '2020-02-05 12:33:59'),
(4, 'Merchandiser', 2, 1, 'merchandiser', 2, '2020-02-05 12:34:02'),
(5, 'Trim Head', 3, 1, 'Trim Head', 2, '2020-02-17 13:05:55'),
(6, 'Fabric Head', 3, 1, 'Fabric Head', 2, '2020-02-17 13:06:01'),
(7, 'Garment Manager', 2, 2, 'unit head', 2, '2020-02-05 12:34:16'),
(8, 'Sewing Head', 3, 2, 'Sewing Head', 2, '2020-02-05 12:34:19'),
(9, 'Dispatch Head', 3, 2, 'Dispatch Head', 2, '2020-02-05 12:34:21'),
(10, 'Finishing Head', 3, 2, 'Finishing Head', 2, '2020-02-05 12:34:23'),
(11, 'Cutting Head', 3, 2, 'Cutting Head', 2, '2020-02-05 12:34:25'),
(12, 'Kaja Button Head', 3, 2, 'Kaja Button Head', 2, '2020-02-05 12:34:27'),
(13, 'Fusing Head', 3, 2, 'Fusing Head', 2, '2020-02-05 12:34:31'),
(14, 'New Designation', 2, NULL, 'Ass', 1, '2020-03-06 05:37:00'),
(15, 'Mechanic', 3, NULL, 'Mechanic', 2, '2020-03-07 05:55:28'),
(16, 'New', 2, NULL, 'sdsfe', 1, '2020-05-13 11:40:08');

-- --------------------------------------------------------

--
-- Table structure for table `dispatch_lots`
--

CREATE TABLE IF NOT EXISTS `dispatch_lots` (
  `dispatch_id` int(11) NOT NULL,
  `dispatch_lot` varchar(100) NOT NULL,
  `company` int(11) NOT NULL,
  `unit` int(11) NOT NULL,
  `erp` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `total_cartons` int(100) NOT NULL,
  `address` text NOT NULL,
  `country` varchar(100) NOT NULL,
  `zip` int(11) NOT NULL,
  `helpers` int(100) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dispatch_lots`
--

INSERT INTO `dispatch_lots` (`dispatch_id`, `dispatch_lot`, `company`, `unit`, `erp`, `quantity`, `total_cartons`, `address`, `country`, `zip`, `helpers`, `created_by`, `created_on`) VALUES
(2, 'Lot 1', 1, 1, 1, 3000, 30, 'kharadi', 'India', 935454, 20, 14, '2020-02-29 12:54:00');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE IF NOT EXISTS `employees` (
  `employee_id` int(11) NOT NULL,
  `full_name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` int(13) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `zip` varchar(6) DEFAULT NULL,
  `designation_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `unit_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_by` int(11) DEFAULT NULL,
  `deleted_on` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`employee_id`, `full_name`, `email`, `phone`, `address`, `city`, `country`, `zip`, `designation_id`, `company_id`, `unit_id`, `department_id`, `status`, `created_by`, `created_on`, `deleted_by`, `deleted_on`) VALUES
(1, 'vaishali patil', 'vaishali@gmail.com', 987565433, 'Kharadi', 'Pune', 'India', '411014', 1, NULL, NULL, NULL, 1, 1, '2020-02-05 13:44:13', NULL, NULL),
(2, 'Sachin Labhade', 'sachin@gmail.com', 987876676, 'Hadapsar', 'Pune', 'India', '411014', 4, 1, NULL, NULL, 1, 1, '2020-02-05 13:44:57', NULL, NULL),
(3, 'Sagar Shinde', 's@gmail.com', 898676565, 'Kharadi', 'Pune', 'India', '411022', 7, 1, 1, NULL, 1, 2, '2020-02-20 09:03:07', NULL, NULL),
(4, 'Veena Patil', 'v@gmail.com', 2147483647, 'Kharadi', 'Pune', 'India', '411034', 6, 1, 1, 4, 1, 2, '2020-02-20 09:14:06', NULL, NULL),
(5, 'Sadhana Shirke', 's3@gmail.com', 2147483647, 'Kharadi', 'Pune', 'India', '411023', 5, 1, 1, 3, 1, 2, '2020-02-20 09:40:23', NULL, NULL),
(6, 's', 'ds@fg.com', 2147483647, 'dfd', 'dv', 'fdv', '1312', 10, 1, 1, 7, 1, 2, '2020-02-20 09:50:04', 2, '2020-02-20 10:50:07'),
(7, 'asd', 's@gmail.com', 2147483647, 'sd', 'sf', 'dfv', '2321', 1, 1, 0, NULL, 1, 2, '2020-02-20 09:51:12', 2, '2020-02-20 10:51:15'),
(8, 'Sandesha', 'sandy@gmail.com', 2147483647, 'Kharadi', 'Pune', 'India', '411023', 11, 1, 1, 5, 1, 2, '2020-02-20 14:12:43', NULL, NULL),
(9, 'Sameer ', 'sameer@gmail.com', 2147483647, 'Kharadi', 'Pune', 'India', '411023', 8, 1, 1, 6, 1, 2, '2020-02-20 14:13:14', NULL, NULL),
(10, 'Spruha', 'spruha@gmail.com', 987324323, 'Kharadi', 'Pune', 'India', '411023', 12, 1, 1, 8, 1, 2, '2020-02-20 14:13:47', NULL, NULL),
(11, 'Sameera', 'sameera@gmail.com', 2147483647, 'Kharadi', 'Pune', 'India', '411023', 10, 1, 1, 7, 1, 2, '2020-02-20 14:14:14', NULL, NULL),
(12, 'Sampada', 'sam@gmail.com', 2147483647, 'Kharadi', 'Pune', 'India', '411023', 13, 1, 1, 9, 1, 2, '2020-02-20 14:14:43', NULL, NULL),
(13, 'Sanjay Singh', 's3@gmail.com', 97985464, 'Kharadi', 'Pune', 'India', '765766', 2, 1, 0, NULL, 1, 1, '2020-02-27 09:05:54', NULL, NULL),
(14, 'Radhika', 'r@gmail.com', 45654767, 'Kharadi', 'Pune', 'India', '686865', 2, 5, 0, NULL, 1, 1, '2020-02-27 09:10:40', NULL, NULL),
(19, 'Samir', 's2@gmail.com', 974328974, 'Kharadi', 'Pune', 'India', '678678', 9, 1, 1, 10, 1, 2, '2020-02-28 07:45:06', NULL, NULL),
(20, 'Sarita', 's3@gmail.com', 98943545, 'kharadi', 'pune', 'India', '411015', 14, 1, 1, 5, 1, 1, '2020-03-06 05:54:38', NULL, NULL),
(21, 'Sanjay', 's@gmail.com', 87967676, 'Thane1', 'pune', 'India', '411024', 11, 1, 2, 5, 1, 2, '2020-03-06 10:29:14', NULL, NULL),
(22, 'Sagar', 's@gmail.com', 7896767, 'kharadi', 'pune', 'India', '726722', 15, 1, 1, 11, 1, 2, '2020-03-07 06:15:14', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `erp`
--

CREATE TABLE IF NOT EXISTS `erp` (
  `erp_id` int(11) NOT NULL,
  `erp_title` varchar(100) DEFAULT NULL,
  `po_number` int(11) DEFAULT NULL,
  `style_number` int(11) DEFAULT NULL,
  `buyer` int(11) DEFAULT NULL,
  `quantity` int(100) DEFAULT NULL,
  `rate` float DEFAULT NULL,
  `currency` varchar(100) DEFAULT NULL,
  `sizing_type` tinyint(1) NOT NULL,
  `delivery_terms` varchar(100) DEFAULT NULL,
  `payment_terms` varchar(100) DEFAULT NULL,
  `season` varchar(100) DEFAULT NULL,
  `wash_type` varchar(100) DEFAULT NULL,
  `erp_status` int(100) NOT NULL DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `erp`
--

INSERT INTO `erp` (`erp_id`, `erp_title`, `po_number`, `style_number`, `buyer`, `quantity`, `rate`, `currency`, `sizing_type`, `delivery_terms`, `payment_terms`, `season`, `wash_type`, `erp_status`, `created_by`, `created_on`) VALUES
(1, 'ERP 1', 1, 1, 1, 20000, 100, 'INR', 0, 'Test Terms', 'Test Terms', 'Winter', 'Chemical', 1, 2, '2020-02-20 11:21:43'),
(2, 'ERP 2', 1, 1, 1, 5000, 100, 'INR', 0, 'Test Terms', 'Test Terms', 'Winter', 'Chemical', 1, 2, '2020-02-27 06:05:12'),
(3, 'MAY20-000001', 1, 1, 1, 5000, 30, 'INR', 0, 'dsfs', 'swrwerfwe', 'winter', 'bh', 1, 1, '2020-05-13 08:43:01');

-- --------------------------------------------------------

--
-- Table structure for table `erp_colors`
--

CREATE TABLE IF NOT EXISTS `erp_colors` (
  `id` int(11) NOT NULL,
  `erp` int(11) NOT NULL,
  `color_code` varchar(100) NOT NULL,
  `color` varchar(100) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `erp_statuses`
--

CREATE TABLE IF NOT EXISTS `erp_statuses` (
  `erp_status_id` int(11) NOT NULL,
  `erp_status` varchar(100) NOT NULL,
  `erp_status_level` int(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `erp_statuses`
--

INSERT INTO `erp_statuses` (`erp_status_id`, `erp_status`, `erp_status_level`) VALUES
(1, 'New', 1),
(2, 'Trim/Fabric Pending', 2),
(4, 'Trim/Fabric Approved', 3),
(5, 'Released', 4);

-- --------------------------------------------------------

--
-- Table structure for table `fabric_checking_log`
--

CREATE TABLE IF NOT EXISTS `fabric_checking_log` (
  `inspection_id` int(11) NOT NULL,
  `erp` int(11) NOT NULL,
  `company` int(11) DEFAULT NULL,
  `unit` int(11) NOT NULL,
  `color` varchar(100) NOT NULL,
  `roll_no` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `rejections` int(11) DEFAULT NULL,
  `datetime` timestamp NULL DEFAULT NULL,
  `barcode` varchar(100) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fabric_checking_log`
--

INSERT INTO `fabric_checking_log` (`inspection_id`, `erp`, `company`, `unit`, `color`, `roll_no`, `quantity`, `rejections`, `datetime`, `barcode`, `created_by`, `created_on`) VALUES
(1, 1, 1, 1, 'pink', 567, 40000, 1000, NULL, '434543545', 5, '2020-02-27 07:45:47');

-- --------------------------------------------------------

--
-- Table structure for table `fabric_inward_log`
--

CREATE TABLE IF NOT EXISTS `fabric_inward_log` (
  `inward_id` int(11) NOT NULL,
  `company` int(11) NOT NULL,
  `unit` int(11) NOT NULL,
  `erp` int(11) NOT NULL,
  `fabric_code` varchar(100) NOT NULL,
  `rolls` int(11) NOT NULL,
  `received_quantity` int(11) DEFAULT NULL,
  `grn_no` varchar(100) DEFAULT NULL,
  `barcode` varchar(100) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fabric_inward_log`
--

INSERT INTO `fabric_inward_log` (`inward_id`, `company`, `unit`, `erp`, `fabric_code`, `rolls`, `received_quantity`, `grn_no`, `barcode`, `created_by`, `created_on`) VALUES
(1, 1, 1, 1, 'F34', 200, 20000, '43243253543', '111100011', 5, '2020-02-20 14:03:43');

-- --------------------------------------------------------

--
-- Table structure for table `fabric_outward_log`
--

CREATE TABLE IF NOT EXISTS `fabric_outward_log` (
  `outward_id` int(11) NOT NULL,
  `inward_id` int(11) NOT NULL,
  `quantity` int(100) NOT NULL,
  `department` int(11) NOT NULL,
  `line` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fabric_outward_log`
--

INSERT INTO `fabric_outward_log` (`outward_id`, `inward_id`, `quantity`, `department`, `line`, `created_by`, `created_on`) VALUES
(1, 1, 2000, 5, 3, 5, '2020-02-20 14:07:07');

-- --------------------------------------------------------

--
-- Table structure for table `finishing_production_data`
--

CREATE TABLE IF NOT EXISTS `finishing_production_data` (
  `id` int(11) NOT NULL,
  `erp` int(11) NOT NULL,
  `company` int(11) NOT NULL,
  `unit` int(11) NOT NULL,
  `section` varchar(100) DEFAULT NULL,
  `line` int(11) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `production_hour` varchar(100) DEFAULT NULL,
  `output` varchar(100) DEFAULT NULL,
  `compound_movement` int(100) DEFAULT NULL,
  `helpers` int(11) DEFAULT NULL,
  `ironers` int(11) DEFAULT NULL,
  `checkers` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `finishing_production_data`
--

INSERT INTO `finishing_production_data` (`id`, `erp`, `company`, `unit`, `section`, `line`, `datetime`, `production_hour`, `output`, `compound_movement`, `helpers`, `ironers`, `checkers`, `created_by`, `created_on`) VALUES
(1, 1, 1, 1, 'D', 8, '2020-03-02 07:47:40', '12:00 - 1:00 PM', 'sdc', 2, 22, 22, 22, 10, '2020-03-02 06:47:40');

-- --------------------------------------------------------

--
-- Table structure for table `fusing_production_data`
--

CREATE TABLE IF NOT EXISTS `fusing_production_data` (
  `id` int(11) NOT NULL,
  `erp` int(11) NOT NULL,
  `company` int(11) NOT NULL,
  `unit` int(11) NOT NULL,
  `section` varchar(100) DEFAULT NULL,
  `line` int(11) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `production_hour` varchar(100) DEFAULT NULL,
  `output` varchar(100) DEFAULT NULL,
  `length` int(100) DEFAULT NULL,
  `width` int(100) DEFAULT NULL,
  `converyer_width` int(100) DEFAULT NULL,
  `conveyer_speed` int(11) DEFAULT NULL,
  `helpers` int(11) DEFAULT NULL,
  `machine_no` varchar(100) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fusing_production_data`
--

INSERT INTO `fusing_production_data` (`id`, `erp`, `company`, `unit`, `section`, `line`, `datetime`, `production_hour`, `output`, `length`, `width`, `converyer_width`, `conveyer_speed`, `helpers`, `machine_no`, `created_by`, `created_on`) VALUES
(1, 2, 1, 1, 'C', 5, '2020-03-02 07:53:55', '12:00 - 1:00 AM', '333', 40, 343, 333, 33, 333, '3332fe', 11, '2020-03-02 06:53:55');

-- --------------------------------------------------------

--
-- Table structure for table `kaja_button_production_data`
--

CREATE TABLE IF NOT EXISTS `kaja_button_production_data` (
  `id` int(11) NOT NULL,
  `erp` int(11) NOT NULL,
  `company` int(11) NOT NULL,
  `unit` int(11) NOT NULL,
  `section` varchar(100) DEFAULT NULL,
  `line` int(11) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `production_hour` varchar(100) DEFAULT NULL,
  `output` varchar(100) DEFAULT NULL,
  `spi` int(100) DEFAULT NULL,
  `inches_covered` float DEFAULT NULL,
  `compound_movement` int(100) DEFAULT NULL,
  `machineries` int(11) DEFAULT NULL,
  `tailers` int(11) DEFAULT NULL,
  `helpers` int(11) DEFAULT NULL,
  `ironers` int(11) DEFAULT NULL,
  `checkers` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kaja_button_production_data`
--

INSERT INTO `kaja_button_production_data` (`id`, `erp`, `company`, `unit`, `section`, `line`, `datetime`, `production_hour`, `output`, `spi`, `inches_covered`, `compound_movement`, `machineries`, `tailers`, `helpers`, `ironers`, `checkers`, `created_by`, `created_on`) VALUES
(1, 1, 1, 1, 'G', 12, '2020-03-02 07:59:59', '12:00 - 1:00 AM', '23', 34, 44, 34, 2342, 34, 34, 33, 343, 9, '2020-03-02 06:59:59');

-- --------------------------------------------------------

--
-- Table structure for table `machines`
--

CREATE TABLE IF NOT EXISTS `machines` (
  `machine_id` int(11) NOT NULL,
  `company` int(11) NOT NULL,
  `unit` int(11) NOT NULL,
  `model` varchar(100) NOT NULL,
  `serial_number` int(100) NOT NULL,
  `machine_type` varchar(100) NOT NULL,
  `make` varchar(100) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `machines`
--

INSERT INTO `machines` (`machine_id`, `company`, `unit`, `model`, `serial_number`, `machine_type`, `make`, `created_by`, `created_on`) VALUES
(1, 1, 1, 'ssd', 3243, 'Double Needle', 'fdgvfd', 17, '2020-03-07 07:38:30'),
(2, 1, 1, 'fdd', 22, 'Feed of Arm ', 'gfgfuuuuuuuuuuuuuuuuuu', 17, '2020-03-07 10:00:04'),
(3, 1, 1, 'sad33', 24, 'Double Needle', 'sds', 17, '2020-05-13 13:58:44');

-- --------------------------------------------------------

--
-- Table structure for table `material_received`
--

CREATE TABLE IF NOT EXISTS `material_received` (
  `inward_id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `unit_id` int(11) DEFAULT NULL,
  `inward_type` varchar(100) DEFAULT NULL,
  `erp` int(11) DEFAULT NULL,
  `item_code` varchar(100) DEFAULT NULL,
  `quantity` int(100) DEFAULT NULL,
  `unit` varchar(100) DEFAULT NULL,
  `barcode` varchar(100) DEFAULT NULL,
  `grn_no` varchar(100) DEFAULT NULL,
  `datetime` timestamp NULL DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `material_requirement`
--

CREATE TABLE IF NOT EXISTS `material_requirement` (
  `id` int(11) NOT NULL,
  `erp_id` int(11) NOT NULL,
  `color` varchar(100) DEFAULT NULL,
  `color_code` varchar(100) DEFAULT NULL,
  `bottom_requirement` text,
  `top_requirement` text,
  `created_by` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `material_requirement`
--

INSERT INTO `material_requirement` (`id`, `erp_id`, `color`, `color_code`, `bottom_requirement`, `top_requirement`, `created_by`, `created_on`) VALUES
(1, 1, 'Blue', '#g545', NULL, '{"xs":"100","s":"200","m":"200","l":"100","xl":"100","xxl":"100","xxxl":"100"}', 2, '2020-02-20 11:35:53'),
(2, 1, 'Pink', '#d33d', NULL, '{"xs":"100","s":"100","m":"100","l":"100","xl":"100","xxl":"200","xxxl":"200"}', 2, '2020-02-20 11:37:01'),
(3, 1, 'Grey', '#00fgfv', '{"b1":"300","b2":"600","b3":"300","b4":"900","b5":"900","b6":"300","b7":"900","b8":"900","b9":"600","b10":"600","b11":"300","b12":"300","b13":"600","b14":"300"}', NULL, 2, '2020-02-20 11:38:11'),
(4, 2, 'Green', '#ggdd', NULL, '{"xs":"200","s":"200","m":"200","l":"200","xl":"200","xxl":"200","xxxl":"200"}', 2, '2020-02-27 06:06:10'),
(5, 2, 'Blue', '#5fdf', '{"b1":"400","b2":"400","b3":"400","b4":"400","b5":"400","b6":"400","b7":"400","b8":"400","b9":"400","b10":"400","b11":"400","b12":"400","b13":"400","b14":"400"}', NULL, 2, '2020-02-27 06:06:33');

-- --------------------------------------------------------

--
-- Table structure for table `outward_logs`
--

CREATE TABLE IF NOT EXISTS `outward_logs` (
  `outward_id` int(11) NOT NULL,
  `erp` int(11) NOT NULL,
  `outward_type` varchar(100) DEFAULT NULL,
  `company` int(11) DEFAULT NULL,
  `unit` int(11) DEFAULT NULL,
  `department` int(11) NOT NULL,
  `line` int(11) NOT NULL,
  `batch` int(11) NOT NULL,
  `barcode` varchar(100) NOT NULL,
  `quantity` int(100) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `production_statuses`
--

CREATE TABLE IF NOT EXISTS `production_statuses` (
  `id` int(11) NOT NULL,
  `status` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `production_statuses`
--

INSERT INTO `production_statuses` (`id`, `status`) VALUES
(1, 'Received'),
(2, 'Inspection in Process'),
(3, 'Approved'),
(4, 'Outward'),
(5, 'Cutting in Process'),
(6, 'Cutting Completed'),
(7, 'Sewing in Process'),
(8, 'Sewing Completed'),
(9, 'Kaja Buttoning in Process'),
(10, 'kaja Buttoning Completed'),
(11, 'Finishing in Process'),
(12, 'Finishing Completed'),
(13, 'Fusing in Process'),
(14, 'Fusing Completed'),
(15, 'Dispatched');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_orders`
--

CREATE TABLE IF NOT EXISTS `purchase_orders` (
  `order_id` int(11) NOT NULL,
  `order_title` varchar(100) NOT NULL,
  `PAN_Number` varchar(10) DEFAULT NULL,
  `GST_Number` int(16) DEFAULT NULL,
  `CIN_Number` varchar(100) DEFAULT NULL,
  `buyer_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `style_id` int(11) DEFAULT NULL,
  `date_issued` date DEFAULT NULL,
  `poc_name` varchar(100) DEFAULT NULL,
  `quantity` int(100) DEFAULT NULL,
  `value` float DEFAULT NULL,
  `currency` varchar(100) DEFAULT NULL,
  `delivery_terms` text,
  `payment_terms` text,
  `season` varchar(100) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_orders`
--

INSERT INTO `purchase_orders` (`order_id`, `order_title`, `PAN_Number`, `GST_Number`, `CIN_Number`, `buyer_id`, `company_id`, `style_id`, `date_issued`, `poc_name`, `quantity`, `value`, `currency`, `delivery_terms`, `payment_terms`, `season`, `created_by`, `created_on`) VALUES
(1, 'Order 1', 'FDFDF2345D', 1232132423, '434234dfferr', 1, 1, 1, '2020-02-20', 'Samir', 50000, 100, 'INR', 'Test Terms', 'Test Terms', 'Winter', 2, '2020-02-20 10:41:00'),
(2, 'Order 2', 'dsfdd4444f', 2147483647, '2132142142', 2, 1, 3, '2020-02-29', 'Samir', 50000, 3000000, 'INR', 'Test Terms', 'Test Terms', 'Winter', 2, '2020-02-29 12:14:51');

-- --------------------------------------------------------

--
-- Table structure for table `service_history`
--

CREATE TABLE IF NOT EXISTS `service_history` (
  `service_id` int(11) NOT NULL,
  `machine` int(11) NOT NULL,
  `date` date NOT NULL,
  `status` varchar(100) NOT NULL,
  `remark` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `service_history`
--

INSERT INTO `service_history` (`service_id`, `machine`, `date`, `status`, `remark`, `created_by`, `created_on`) VALUES
(1, 1, '2020-03-18', 'Not Working', 'sasa', 17, '2020-03-07 10:55:08'),
(2, 1, '2020-03-25', 'Working', 'aaaaaaaffffffffffff', 17, '2020-03-07 10:56:19');

-- --------------------------------------------------------

--
-- Table structure for table `sewing_production_data`
--

CREATE TABLE IF NOT EXISTS `sewing_production_data` (
  `id` int(11) NOT NULL,
  `erp` int(11) NOT NULL,
  `company` int(11) NOT NULL,
  `unit` int(11) NOT NULL,
  `section` varchar(100) DEFAULT NULL,
  `line` int(11) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `production_hour` varchar(100) DEFAULT NULL,
  `output` varchar(100) DEFAULT NULL,
  `spi` int(100) DEFAULT NULL,
  `inches_covered` float DEFAULT NULL,
  `compound_movement` int(100) DEFAULT NULL,
  `machineries` int(11) DEFAULT NULL,
  `helpers` int(11) DEFAULT NULL,
  `tailers` int(11) DEFAULT NULL,
  `ironers` int(11) NOT NULL,
  `checkers` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sewing_production_data`
--

INSERT INTO `sewing_production_data` (`id`, `erp`, `company`, `unit`, `section`, `line`, `datetime`, `production_hour`, `output`, `spi`, `inches_covered`, `compound_movement`, `machineries`, `helpers`, `tailers`, `ironers`, `checkers`, `created_by`, `created_on`) VALUES
(1, 1, 1, 1, 'D', 5, '2020-02-27 08:20:03', '12:00 - 1:00', 'Test Output', 300, 33, 332, 100, 100, 100, 100, 100, 8, '2020-02-27 07:20:03');

-- --------------------------------------------------------

--
-- Table structure for table `styles`
--

CREATE TABLE IF NOT EXISTS `styles` (
  `style_id` int(11) NOT NULL,
  `style_code` varchar(100) NOT NULL,
  `style_name` varchar(100) DEFAULT NULL,
  `season` varchar(100) DEFAULT NULL,
  `brand` int(100) DEFAULT NULL,
  `company` int(100) DEFAULT NULL,
  `garment_type` varchar(100) DEFAULT NULL,
  `garment_fit` varchar(100) DEFAULT NULL,
  `garment_remark` text,
  `created_by` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `styles`
--

INSERT INTO `styles` (`style_id`, `style_code`, `style_name`, `season`, `brand`, `company`, `garment_type`, `garment_fit`, `garment_remark`, `created_by`, `created_on`) VALUES
(1, 'SS11', 'Style 1', 'Winter', 4, 1, 'Tops', 'Slim', 'Test Remark', 3, '2020-02-20 08:24:17'),
(2, 'PD345', 'Style 2', 'Winter', 2, 1, 'Tops', 'Regular', 'Test Remark', 2, '2020-02-27 08:47:42'),
(3, 'SS454', 'Style 2', 'Winter', 2, 1, 'Bottom', 'Slim', 'Test Remark', 2, '2020-02-29 12:14:42');

-- --------------------------------------------------------

--
-- Table structure for table `top_sizes`
--

CREATE TABLE IF NOT EXISTS `top_sizes` (
  `id` int(11) NOT NULL,
  `size` varchar(100) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `top_sizes`
--

INSERT INTO `top_sizes` (`id`, `size`, `created_on`) VALUES
(1, 'XS', '2019-12-28 18:25:53'),
(2, 'S', '2019-12-28 18:25:53'),
(3, 'M', '2019-12-28 18:26:06'),
(4, 'L', '2019-12-28 18:26:06'),
(5, 'XL', '2019-12-28 18:26:17'),
(6, 'XXL', '2019-12-28 18:26:17'),
(7, 'XXXL', '2019-12-28 18:26:48');

-- --------------------------------------------------------

--
-- Table structure for table `trim_fabric_requirement`
--

CREATE TABLE IF NOT EXISTS `trim_fabric_requirement` (
  `requirement_id` int(11) NOT NULL,
  `erp` int(11) NOT NULL,
  `requirement_type` varchar(100) NOT NULL,
  `category` varchar(100) DEFAULT NULL,
  `item_code` varchar(100) DEFAULT NULL,
  `item_color` varchar(100) DEFAULT NULL,
  `item_color_code` varchar(100) NOT NULL,
  `item_description` text,
  `cotton_content` varchar(100) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `consumption` float DEFAULT NULL,
  `quantity` int(100) DEFAULT NULL,
  `booked_quantity` int(100) DEFAULT NULL,
  `extra_needed` float DEFAULT NULL,
  `rate` float DEFAULT NULL,
  `colorwise_booking` tinyint(1) DEFAULT NULL,
  `sizewise_booking` tinyint(1) DEFAULT NULL,
  `booked_status` tinyint(1) DEFAULT NULL,
  `dispatch_status` tinyint(1) DEFAULT NULL,
  `dispatch_date` date DEFAULT NULL,
  `ETA` date DEFAULT NULL,
  `delivery_terms` text,
  `payment_terms` text,
  `created_by` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trim_fabric_requirement`
--

INSERT INTO `trim_fabric_requirement` (`requirement_id`, `erp`, `requirement_type`, `category`, `item_code`, `item_color`, `item_color_code`, `item_description`, `cotton_content`, `width`, `consumption`, `quantity`, `booked_quantity`, `extra_needed`, `rate`, `colorwise_booking`, `sizewise_booking`, `booked_status`, `dispatch_status`, `dispatch_date`, `ETA`, `delivery_terms`, `payment_terms`, `created_by`, `created_on`) VALUES
(1, 1, 'Trim', 'Buttons', 'T24', 'Black', '#0000', 'Test Description', NULL, NULL, 100, 3000, 3000, 1000, 50, 0, 0, 0, 0, '2020-03-20', '2020-03-15', 'Test Terms', 'Test Terms', 2, '2020-02-20 11:47:14'),
(2, 1, 'Fabric', 'body', 'F34', 'Black', '#0000', 'Test Description', '20', 200, 100, 60000, 60000, 5000, 300, 0, 0, 0, 0, '2020-03-26', '2020-02-18', 'Test Terms', 'Test Terms', 2, '2020-02-20 13:21:01'),
(3, 2, 'trim', 'Buttons', 'T456', 'Blue', '#5fgr', 'Test Description', NULL, NULL, 400, 10000, 10000, 0, 200, 1, 0, 1, 1, '2020-02-28', '2020-04-28', 'Test Terms', 'Test Terms', 2, '2020-02-27 06:07:30'),
(4, 2, 'fabric', 'trims', 'F453', 'Pink', '#67eef', 'Test Description', '30', 300, 100, 20000, 20000, 5000, 300, 1, 0, 1, 0, '2020-02-29', '2020-02-29', 'Test Terms', 'Test Terms', 2, '2020-02-27 06:09:53');

-- --------------------------------------------------------

--
-- Table structure for table `trim_inward_log`
--

CREATE TABLE IF NOT EXISTS `trim_inward_log` (
  `inward_id` int(11) NOT NULL,
  `erp` int(11) NOT NULL,
  `company` int(11) NOT NULL,
  `unit` int(11) NOT NULL,
  `trim_code` varchar(100) NOT NULL,
  `material_unit` varchar(100) DEFAULT NULL,
  `received_quantity` int(100) NOT NULL,
  `rejected_quantity` int(100) DEFAULT NULL,
  `barcode` varchar(100) DEFAULT NULL,
  `grn_no` varchar(100) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `trim_inward_log`
--

INSERT INTO `trim_inward_log` (`inward_id`, `erp`, `company`, `unit`, `trim_code`, `material_unit`, `received_quantity`, `rejected_quantity`, `barcode`, `grn_no`, `created_by`, `created_on`) VALUES
(1, 1, 1, 1, 'T24', 'Meters', 40000, 5000, '101011111', '23453244', 6, '2020-02-20 13:37:20');

-- --------------------------------------------------------

--
-- Table structure for table `trim_outward_log`
--

CREATE TABLE IF NOT EXISTS `trim_outward_log` (
  `outward_id` int(11) NOT NULL,
  `inward_id` int(11) NOT NULL,
  `quantity` int(100) NOT NULL,
  `department` int(11) NOT NULL,
  `line` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `trim_outward_log`
--

INSERT INTO `trim_outward_log` (`outward_id`, `inward_id`, `quantity`, `department`, `line`, `created_by`, `created_on`) VALUES
(1, 1, 5000, 6, 2, 6, '2020-02-20 13:57:12');

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

CREATE TABLE IF NOT EXISTS `units` (
  `unit_id` int(11) NOT NULL,
  `unit_name` varchar(100) DEFAULT NULL,
  `manager_employee_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `zip` int(6) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `units`
--

INSERT INTO `units` (`unit_id`, `unit_name`, `manager_employee_id`, `company_id`, `address`, `city`, `country`, `zip`, `created_by`, `created_on`) VALUES
(1, 'Unit 1', 3, 1, 'Kharadi', 'Pune', 'India', 411022, 3, '2020-02-20 09:39:46'),
(2, 'Unit 2', 14, 1, 'Kharadi', 'Pune', 'India', 5436, 2, '2020-02-27 09:56:51');

-- --------------------------------------------------------

--
-- Table structure for table `unit_lines`
--

CREATE TABLE IF NOT EXISTS `unit_lines` (
  `line_id` int(11) NOT NULL,
  `line` varchar(100) NOT NULL,
  `company_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `dept_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `unit_lines`
--

INSERT INTO `unit_lines` (`line_id`, `line`, `company_id`, `unit_id`, `dept_id`, `created_by`, `created_on`) VALUES
(2, 'A', 1, 1, 5, 2, '2020-02-20 09:56:27'),
(3, 'B', 1, 1, 5, 2, '2020-02-20 09:56:41'),
(4, 'C', 1, 1, 5, 2, '2020-02-20 09:56:46'),
(5, 'A', 1, 1, 6, 2, '2020-02-20 09:56:52'),
(6, 'B', 1, 1, 6, 2, '2020-02-20 09:56:57'),
(7, 'C', 1, 1, 6, 2, '2020-02-20 09:57:02'),
(8, 'A', 1, 1, 7, 2, '2020-02-20 09:57:09'),
(9, 'B', 1, 1, 7, 2, '2020-02-20 09:57:13'),
(10, 'C', 1, 1, 7, 2, '2020-02-20 09:57:19'),
(11, 'A', 1, 1, 8, 2, '2020-02-20 09:57:24'),
(12, 'B', 1, 1, 8, 2, '2020-02-20 09:57:29'),
(13, 'C', 1, 1, 8, 2, '2020-02-20 09:57:35'),
(14, 'A', 1, 1, 9, 2, '2020-02-20 09:57:39'),
(15, 'B', 1, 1, 9, 2, '2020-02-20 09:57:44'),
(16, 'C', 1, 1, 9, 2, '2020-02-20 09:57:48');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `full_name` varchar(100) DEFAULT NULL,
  `user_type` varchar(100) NOT NULL DEFAULT 'employee',
  `user_role` int(11) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `buyer_id` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `full_name`, `user_type`, `user_role`, `employee_id`, `buyer_id`, `status`, `created_by`, `created_on`) VALUES
(1, 'admin123', '123456', 'Vaishali patil', 'employee', 1, 1, NULL, 1, 1, '2020-02-20 09:38:02'),
(2, 'merchant123', '123456', 'Sachin Labhade', 'employee', 2, 2, NULL, 1, 1, '2020-02-20 09:37:52'),
(3, 'buyer123', 'buyer123', 'Buyer 1', 'employee', 8, NULL, 1, 1, 2, '2020-02-20 09:39:11'),
(4, 'GM123', '123456', 'Sagar Shinde', 'employee', 2, 3, NULL, 1, 2, '2020-02-20 09:41:02'),
(5, 'fabric123', '123456', 'Veena Patil', 'employee', 3, 4, NULL, 1, 2, '2020-02-20 13:35:03'),
(6, 'trim123', '123456', 'Sadhana Shirke', 'employee', 3, 5, NULL, 1, 2, '2020-02-20 13:35:12'),
(7, 'cutting123', '123456', 'Sandesha', 'employee', 3, 8, NULL, 1, 2, '2020-02-20 14:14:54'),
(8, 'sewing123', '123456', 'Sameer ', 'employee', 3, 9, NULL, 1, 2, '2020-02-20 14:15:10'),
(9, 'kajabutton123', '123456', 'Spruha', 'employee', 3, 10, NULL, 1, 2, '2020-02-20 14:15:26'),
(10, 'finishing123', '123456', 'Sameera', 'employee', 3, 11, NULL, 1, 2, '2020-02-20 14:15:37'),
(11, 'fusing123', '123456', 'Sampada', 'employee', 3, 12, NULL, 1, 2, '2020-02-20 14:15:48'),
(12, 'Buyer 2', 'Buyer 2', 'Buyer 2', 'employee', 8, NULL, 2, 1, 2, '2020-02-27 08:36:58'),
(13, 'punit_md', '123456', 'Sanjay Singh', 'employee', 2, 13, NULL, 1, 1, '2020-02-27 09:11:15'),
(14, 'dispatch123', '123456', 'Samir', 'employee', 3, 19, NULL, 1, 2, '2020-02-28 07:45:15'),
(15, 'sarita123', '123456', 'Sarita', 'employee', 2, 20, NULL, 1, 1, '2020-03-06 05:54:50'),
(16, 'sanjay123', '123456', 'Sanjay', 'employee', 3, 21, NULL, 1, 2, '2020-03-06 10:29:30'),
(17, 'mechanic123', '123456', 'Sagar', 'employee', 3, 22, NULL, 1, 2, '2020-03-07 06:16:08');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE IF NOT EXISTS `user_roles` (
  `id` int(11) NOT NULL,
  `role` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(100) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`id`, `role`, `description`, `created_by`, `created_on`) VALUES
(1, 'Admin', 'this is admin', 2, '2020-02-05 11:35:43'),
(2, 'Superuser', 'superuser', 2, '2020-01-25 07:05:41'),
(3, 'User', 'User', 2, '2020-01-25 07:05:45'),
(4, 'Buyer', 'buyer', 3, '2020-03-06 07:10:27');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bottom_sizes`
--
ALTER TABLE `bottom_sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`brand_id`);

--
-- Indexes for table `buyers`
--
ALTER TABLE `buyers`
  ADD PRIMARY KEY (`buyer_id`);

--
-- Indexes for table `carton_content`
--
ALTER TABLE `carton_content`
  ADD PRIMARY KEY (`carton_id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `cutting_production_data`
--
ALTER TABLE `cutting_production_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`dept_id`);

--
-- Indexes for table `designations`
--
ALTER TABLE `designations`
  ADD PRIMARY KEY (`designation_id`);

--
-- Indexes for table `dispatch_lots`
--
ALTER TABLE `dispatch_lots`
  ADD PRIMARY KEY (`dispatch_id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`employee_id`);

--
-- Indexes for table `erp`
--
ALTER TABLE `erp`
  ADD PRIMARY KEY (`erp_id`);

--
-- Indexes for table `erp_colors`
--
ALTER TABLE `erp_colors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_statuses`
--
ALTER TABLE `erp_statuses`
  ADD PRIMARY KEY (`erp_status_id`);

--
-- Indexes for table `fabric_checking_log`
--
ALTER TABLE `fabric_checking_log`
  ADD PRIMARY KEY (`inspection_id`);

--
-- Indexes for table `fabric_inward_log`
--
ALTER TABLE `fabric_inward_log`
  ADD PRIMARY KEY (`inward_id`);

--
-- Indexes for table `fabric_outward_log`
--
ALTER TABLE `fabric_outward_log`
  ADD PRIMARY KEY (`outward_id`);

--
-- Indexes for table `finishing_production_data`
--
ALTER TABLE `finishing_production_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fusing_production_data`
--
ALTER TABLE `fusing_production_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kaja_button_production_data`
--
ALTER TABLE `kaja_button_production_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `machines`
--
ALTER TABLE `machines`
  ADD PRIMARY KEY (`machine_id`);

--
-- Indexes for table `material_received`
--
ALTER TABLE `material_received`
  ADD PRIMARY KEY (`inward_id`);

--
-- Indexes for table `material_requirement`
--
ALTER TABLE `material_requirement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `outward_logs`
--
ALTER TABLE `outward_logs`
  ADD PRIMARY KEY (`outward_id`);

--
-- Indexes for table `production_statuses`
--
ALTER TABLE `production_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_orders`
--
ALTER TABLE `purchase_orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `service_history`
--
ALTER TABLE `service_history`
  ADD PRIMARY KEY (`service_id`);

--
-- Indexes for table `sewing_production_data`
--
ALTER TABLE `sewing_production_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `styles`
--
ALTER TABLE `styles`
  ADD PRIMARY KEY (`style_id`);

--
-- Indexes for table `top_sizes`
--
ALTER TABLE `top_sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trim_fabric_requirement`
--
ALTER TABLE `trim_fabric_requirement`
  ADD PRIMARY KEY (`requirement_id`);

--
-- Indexes for table `trim_inward_log`
--
ALTER TABLE `trim_inward_log`
  ADD PRIMARY KEY (`inward_id`);

--
-- Indexes for table `trim_outward_log`
--
ALTER TABLE `trim_outward_log`
  ADD PRIMARY KEY (`outward_id`);

--
-- Indexes for table `units`
--
ALTER TABLE `units`
  ADD PRIMARY KEY (`unit_id`);

--
-- Indexes for table `unit_lines`
--
ALTER TABLE `unit_lines`
  ADD PRIMARY KEY (`line_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bottom_sizes`
--
ALTER TABLE `bottom_sizes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `brand_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `buyers`
--
ALTER TABLE `buyers`
  MODIFY `buyer_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `carton_content`
--
ALTER TABLE `carton_content`
  MODIFY `carton_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `cutting_production_data`
--
ALTER TABLE `cutting_production_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `dept_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `designations`
--
ALTER TABLE `designations`
  MODIFY `designation_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `dispatch_lots`
--
ALTER TABLE `dispatch_lots`
  MODIFY `dispatch_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `employee_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `erp`
--
ALTER TABLE `erp`
  MODIFY `erp_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `erp_colors`
--
ALTER TABLE `erp_colors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `erp_statuses`
--
ALTER TABLE `erp_statuses`
  MODIFY `erp_status_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `fabric_checking_log`
--
ALTER TABLE `fabric_checking_log`
  MODIFY `inspection_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `fabric_inward_log`
--
ALTER TABLE `fabric_inward_log`
  MODIFY `inward_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `fabric_outward_log`
--
ALTER TABLE `fabric_outward_log`
  MODIFY `outward_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `finishing_production_data`
--
ALTER TABLE `finishing_production_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `fusing_production_data`
--
ALTER TABLE `fusing_production_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `kaja_button_production_data`
--
ALTER TABLE `kaja_button_production_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `machines`
--
ALTER TABLE `machines`
  MODIFY `machine_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `material_received`
--
ALTER TABLE `material_received`
  MODIFY `inward_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `material_requirement`
--
ALTER TABLE `material_requirement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `outward_logs`
--
ALTER TABLE `outward_logs`
  MODIFY `outward_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `production_statuses`
--
ALTER TABLE `production_statuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `purchase_orders`
--
ALTER TABLE `purchase_orders`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `service_history`
--
ALTER TABLE `service_history`
  MODIFY `service_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sewing_production_data`
--
ALTER TABLE `sewing_production_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `styles`
--
ALTER TABLE `styles`
  MODIFY `style_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `trim_fabric_requirement`
--
ALTER TABLE `trim_fabric_requirement`
  MODIFY `requirement_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `trim_inward_log`
--
ALTER TABLE `trim_inward_log`
  MODIFY `inward_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `trim_outward_log`
--
ALTER TABLE `trim_outward_log`
  MODIFY `outward_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `units`
--
ALTER TABLE `units`
  MODIFY `unit_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `unit_lines`
--
ALTER TABLE `unit_lines`
  MODIFY `line_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
