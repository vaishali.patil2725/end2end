import { Component, OnInit, ElementRef, ViewChild, HostListener } from '@angular/core';
import { CompanyService } from '../../_services/company.service';
import { EmployeeService } from '../../_services/employee.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from "@angular/forms";

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.css']
})
export class CompanyComponent implements OnInit {
  @ViewChild('closeBtn', {static: false}) closeBtn: ElementRef<HTMLElement>;
  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    this.closeBtn.nativeElement.click();
    this.submitted = false;
  }
  companies: any;
  NewCompanyForm: FormGroup;
  submitted = false;
  p:number=1;
  managers:[];
  constructor(private api:CompanyService,
    private empService:EmployeeService, 
    private toastr:ToastrManager, 
    private fb: FormBuilder) { }

  ngOnInit() {
    this.getCompanyList();
    this.getEmployees();
    this.createNewCompanyForm();
  }

  createNewCompanyForm(){
    this.submitted = false;
    this.NewCompanyForm = this.fb.group({
      name: ["", [Validators.required]],
      number: ["", [Validators.required]],
      manager_employee_id:["", [Validators.required]],
      address: ["", Validators.required],
      city: ["", Validators.required],
      country: ["", Validators.required],
      zip: ["", Validators.required],
      company_id:[""]
    });
  }
  get formControls() {
    return this.NewCompanyForm.controls;
  }

  getCompanyList(){
    this.api.getCompanyList().subscribe( response => {	
      this.companies =  response;
    });
  }

  getEmployees(){
    this.empService.getAllCompanyManagers().subscribe( response => {	
      this.managers =  response;
    });
  }

  editCompany(data){
    this.submitted = false;
    this.NewCompanyForm = this.fb.group({
      name: [data.name, [Validators.required]],
      number: [data.number, [Validators.required]],
      manager_employee_id:[data.manager_employee_id, [Validators.required]],
      address: [data.address, Validators.required],
      city: [data.city, Validators.required],
      country: [data.country, Validators.required],
      zip: [data.zip, Validators.required],
      company_id:[data.company_id]
    });
  }

  addorEditCompany(){
    this.submitted = true;
    if (this.NewCompanyForm.invalid) {
      return;
    }
    this.api.addorEditCompany(this.NewCompanyForm.value).subscribe(response => {
      if (response!= 0) {
        if(this.NewCompanyForm.value.company_id != ''){
          this.toastr.successToastr("Company details updated successfully.");
        }else{
          this.toastr.successToastr("New Company added successfully.");

        }
        this.closeBtn.nativeElement.click();
        this.getCompanyList();
      }
    });
  }

}
