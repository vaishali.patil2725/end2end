import { Component, OnInit, ElementRef, ViewChild, HostListener } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ToastrManager } from 'ng6-toastr-notifications';

import { EmployeeService } from '../../_services/employee.service';
import { UnitService } from '../../_services/unit.service';
import { CompanyService } from '../../_services/company.service';
import { CommonSettings } from '../../_services/CommonSettings';

@Component({
  selector: 'app-units',
  templateUrl: './units.component.html',
  styleUrls: ['./units.component.css']
})
export class UnitsComponent implements OnInit {
  @ViewChild('closeBtn', {static: false}) closeBtn: ElementRef<HTMLElement>;
  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    this.closeBtn.nativeElement.click();
    this.submitted = false;
  }

  NewUnitForm: FormGroup;
  filterForm: FormGroup;

  units:[];
  employees:[];
  companies:[];
  submitted = false;
  p:number=1;
  userRole:number;
  ADMIN:Number;

  constructor(private api:UnitService, 
    private empService:EmployeeService,
    private company:CompanyService,
    private toastr:ToastrManager, 
    private fb: FormBuilder) { 
      this.ADMIN = CommonSettings.ROLE_ADMIN;
    }

  ngOnInit() {
    this.filterForm = this.fb.group({
      company:[""],
    });

    this.userRole = parseInt(localStorage.getItem('userRole'));
    this.createUnitForm();
    this.getUnits();
    this.getFactoryManagerEmployees();

    if( this.userRole == this.ADMIN){
      this.getCompanyList();
    }
  }

  getUnits(){
    this.api.getFilteredUnits(this.filterForm.value).subscribe( response => {
      this.units =  response;
    });
  }

  getCompanyList(){
    this.company.getCompanyList().subscribe( response => {
      this.companies =  response;
    });
  }

  getFactoryManagerEmployees(){
    this.empService.getFactoryManagerEmployees().subscribe( response => {
       this.employees =  response;
     });
  }
 
  resetFilters(){
    this.filterForm.reset();
    this.getUnits();
  }

  addorEditNewUnit(){
    this.submitted = true;
    if (this.NewUnitForm.invalid) {
      return;
    }
    this.api.addorEditNewUnit(this.NewUnitForm.value).subscribe(response => {
      if (response!= 0) {
        if(this.NewUnitForm.value.unit_id != ''){
          this.toastr.successToastr("Unit details updated successfully.");
        }else{
          this.toastr.successToastr("New unit added successfully.");
        }
        this.closeBtn.nativeElement.click();
        this.getUnits();
      } else{
        this.toastr.successToastr("Error! While adding unit");
      }
    });
  }
  
  createUnitForm(){
    this.submitted =  false;
    this.NewUnitForm = this.fb.group({
      unit_name: ["", [Validators.required]],
      manager_employee_id: ["", [Validators.required]],
      address: ["", Validators.required],
      city: ["", Validators.required],
      country: ["", Validators.required],
      zip: ["", [Validators.required, Validators.maxLength(6),Validators.minLength(6),Validators.pattern('^[0-9]{1,6}$')]],
      unit_id:[""]
    });
  }

  editUnitinfo(data){
    this.NewUnitForm = this.fb.group({
      unit_name: [data.unit_name, [Validators.required]],
      manager_employee_id: [data.manager_employee_id, [Validators.required]],
      address: [data.address, Validators.required],
      city: [data.city, Validators.required],
      country: [data.country, Validators.required],
      zip: [data.zip, [Validators.required, Validators.maxLength(6),Validators.minLength(6),Validators.pattern('^[0-9]{1,6}$')]],
      unit_id:[data.unit_id]
    });
  }

  get formControls() {
    return this.NewUnitForm.controls;
  }

}
