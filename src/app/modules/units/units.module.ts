import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxPaginationModule } from 'ngx-pagination';
import { ReactiveFormsModule , FormsModule } from '@angular/forms';

import { UnitsRoutingModule } from './units-routing.module';
import { UnitsComponent } from './units.component';


@NgModule({
  declarations: [UnitsComponent],
  imports: [
    CommonModule,
    UnitsRoutingModule,
    NgxPaginationModule,
    ReactiveFormsModule
  ]
})
export class UnitsModule { }
