import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UnitlinesComponent } from './unitlines.component';

const routes: Routes = [{ path: '', component: UnitlinesComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UnitlinesRoutingModule { }
