import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { UnitlinesRoutingModule } from './unitlines-routing.module';
import { UnitlinesComponent } from './unitlines.component';

@NgModule({
  declarations: [UnitlinesComponent],
  imports: [
    CommonModule,
    UnitlinesRoutingModule,
    ReactiveFormsModule,
    NgxPaginationModule
  ]
})
export class UnitlinesModule { }
