import { Component, OnInit, ElementRef, ViewChild, HostListener} from '@angular/core';
import { ToastrManager } from 'ng6-toastr-notifications';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';

import { UnitlinesService } from '../../_services/unitlines.service';
import { SharedService } from '../../_services/shared.service';

@Component({
  selector: 'app-unitlines',
  templateUrl: './unitlines.component.html',
  styleUrls: ['./unitlines.component.css']
})
export class UnitlinesComponent implements OnInit {
  @ViewChild('closeBtn', {static: false}) closeBtn: ElementRef<HTMLElement>;
  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    this.closeBtn.nativeElement.click();
    this.submitted = false;
  }

  UnitLinesForm:FormGroup;
   unitlines:any;
   submitted=false;
   userRole='';
   p:number=1;
   departments:[];
  constructor(private fb:FormBuilder, 
    private toastr:ToastrManager, 
    private api:UnitlinesService,
    private service:SharedService) { }

  ngOnInit() {
    this.userRole = localStorage.getItem('userRole');
    this.createUnitLinesForm();
    this.getUnitLines();
    this.getDepartments();
  }

  createUnitLinesForm(){
    this.submitted=false;
    this.UnitLinesForm=this.fb.group({
      line:['', Validators.required],
      dept_id:['', Validators.required],
      line_id:['']
    });
  }

  getDepartments(){
    this.service.getDepartments().subscribe(res=>{
      this.departments=res;
   });
  }

  editUnitlines(data){
    this.UnitLinesForm=this.fb.group({
      line:[data.line, Validators.required],
      dept_id:[data.dept_id, Validators.required],
      line_id:[data.line_id]
    });
  }

  get formControls(){
    return this.UnitLinesForm.controls;
 }

 getUnitLines(){
   this.api.getAllLines().subscribe(res=>{
      this.unitlines=res;
   });
 }
 addorEditUnitline(){
  this.submitted = true;
  if (this.UnitLinesForm.invalid) {
    return;
  }
  this.api.addorEditUnitline(this.UnitLinesForm.value).subscribe(res=>{
   if (res!= 0) {
     if(this.UnitLinesForm.value.brand_id!=''){
       this.toastr.successToastr("Section details updated successfully.");
     }else{
       this.toastr.successToastr("New Section added successfully.");
     }
     this.closeBtn.nativeElement.click();
     this.getUnitLines();
   } else{
     this.toastr.successToastr("Error! While adding Section");
   }
  });
}
}
