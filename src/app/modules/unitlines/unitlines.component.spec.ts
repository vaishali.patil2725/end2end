import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnitlinesComponent } from './unitlines.component';

describe('UnitlinesComponent', () => {
  let component: UnitlinesComponent;
  let fixture: ComponentFixture<UnitlinesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnitlinesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnitlinesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
