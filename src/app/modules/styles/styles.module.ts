import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { ReactiveFormsModule , FormsModule } from '@angular/forms';

import { StylesRoutingModule } from './styles-routing.module';
import { StylesComponent } from './styles.component';

@NgModule({
  declarations: [StylesComponent],
  imports: [
    CommonModule,
    StylesRoutingModule,
    SharedModule,
    NgxPaginationModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class StylesModule { }
