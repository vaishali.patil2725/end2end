import { Component, OnInit, ViewChild, ElementRef, HostListener } from '@angular/core';
import { ToastrManager } from 'ng6-toastr-notifications';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { CommonSettings } from '../../_services/CommonSettings';
import { StyleService } from '../../_services/style.service';
import { BrandService } from '../../_services/brand.service';
import { ViewStyleComponent } from '../shared/view-style/view-style.component';
import { CompanyService } from '../../_services/company.service';

@Component({
  selector: 'app-styles',
  templateUrl: './styles.component.html',
  styleUrls: ['./styles.component.css']
})
export class StylesComponent implements OnInit {
  @ViewChild(ViewStyleComponent, {static: true}) style: ViewStyleComponent;
  @ViewChild('closeBtn', {static: false}) closeBtn: ElementRef<HTMLElement>;
  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    this.closeBtn.nativeElement.click();
    this.submitted = false;
  }
  NewStyleForm: FormGroup;
  filterForm:FormGroup;
  companies:[];
  brands:[];
  styles:[];

  submitted = false;
  p:number=1;
  userRole:number;
  ADMIN:number;

  constructor(private api:StyleService, 
    private brandService:BrandService,
    private company:CompanyService,
    private toastr:ToastrManager, 
    private fb: FormBuilder) {
      this.ADMIN = CommonSettings.ROLE_ADMIN;
     }

  ngOnInit() {
    this.filterForm = this.fb.group({
      company:[""],
    });
    
    this.userRole = parseInt(localStorage.getItem('userRole'));
    this.createNewStyleFormGroup();
    this.getFilteredStyles();
    this.getBrands();

    if( this.userRole == this.ADMIN){
      this.getCompanyList();
    }
  }

  getFilteredStyles(){
    this.api.getFilteredStyles(this.filterForm.value).subscribe( response => {
      this.styles =  response;
    });
  }

  getBrands(){
    this.brandService.getBrands().subscribe( response => {
      this.brands =  response;
    });
  }
  
  getCompanyList(){
    this.company.getCompanyList().subscribe( response => {
      this.companies =  response;
    });
  }

  resetForm(){
    this.NewStyleForm.reset();
  }

  resetFilters(){
    this.filterForm.reset();
    this.getFilteredStyles();
  }

  addorEditNewStyle(){
    this.submitted = true;
    if (this.NewStyleForm.invalid) {
      return;
    }
    this.api.addorEditNewStyle(this.NewStyleForm.value).subscribe(response => {
      if (response!= 0) {
        if(this.NewStyleForm.value.style_id != ''){
          this.toastr.successToastr("Style Updated successfully.");
        }else{
          this.toastr.successToastr("Style added successfully.");
        }
        this.closeBtn.nativeElement.click();
        this.getFilteredStyles();
        this.NewStyleForm.reset()
      } else{
        this.toastr.successToastr("Error! While adding Style");
      }
    });
  }

  
  createNewStyleFormGroup(){
    this.submitted = false;
    this.NewStyleForm = this.fb.group({
      style_code: ["", [Validators.required]],
      style_name: ["", [Validators.required]],
      brand:["",[Validators.required]],
      season: ["", [Validators.required]],
      garment_type: ["", [Validators.required]],
      garment_fit: ["", [Validators.required]],
      garment_remark: ["", [Validators.required]],
      style_id:[""]
    });
  }

  editStyle(data){
    this.submitted = false;
    this.NewStyleForm = this.fb.group({
      style_code: [data.style_code, [Validators.required]],
      style_name: [data.style_name, [Validators.required]],
      brand:[data.brand,[Validators.required]],
      season: [data.season, [Validators.required]],
      garment_type: [data.garment_type, [Validators.required]],
      garment_fit: [data.garment_fit, [Validators.required]],
      garment_remark: [data.garment_remark, [Validators.required]],
      style_id:[data.style_id]
    });
  }

  viewStyle(style_id){
    this.style.showStyleDetails(style_id);
  }

  get formControls() {
    return this.NewStyleForm.controls;
  }


}
