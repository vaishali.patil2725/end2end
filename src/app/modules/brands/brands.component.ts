import { Component, OnInit, ElementRef, ViewChild, HostListener } from '@angular/core';
import { BrandService } from '../../_services/brand.service'
import { ToastrManager } from 'ng6-toastr-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from "@angular/forms";
import { CommonSettings } from '../../_services/CommonSettings';
import { CompanyService } from '../../_services/company.service';

@Component({
  selector: 'app-brands',
  templateUrl: './brands.component.html',
  styleUrls: ['./brands.component.css']
})
export class BrandsComponent implements OnInit {
  @ViewChild('closeBtn', {static: false}) closeBtn: ElementRef<HTMLElement>;
  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    this.closeBtn.nativeElement.click();
    this.submitted = false;
  }

  NewBrandForm: FormGroup;
  filterForm: FormGroup;

  brands:[];
  companies:[];
  submitted = false;
  userRole:number;
  p:number=1;
  ADMIN:number;

  constructor(private api:BrandService, 
    private company:CompanyService,
    private toastr:ToastrManager, 
    private fb: FormBuilder) { 
      this.ADMIN = CommonSettings.ROLE_ADMIN;
    }

  ngOnInit() {
    this.filterForm = this.fb.group({
      company:[""],
    });

    this.userRole = parseInt(localStorage.getItem('userRole'));

    this.createBrandForm();
    this.getBrands();
    
    if( this.userRole == this.ADMIN ){
      this.getCompanyList();
    }
  }

  getBrands(){
    this.api.getFilteredBrands(this.filterForm.value).subscribe( response => {
      this.brands =  response;
    });
  }

  getCompanyList(){
    this.company.getCompanyList().subscribe( response => {
      this.companies =  response;
    });
  }

  resetFilters(){
    this.filterForm.reset();
    this.getBrands();
  }

  addorEditBrand(){
    this.submitted = true;
    if (this.NewBrandForm.invalid) {
      return;
    }
    this.api.addorEditBrand(this.NewBrandForm.value).subscribe(response => {
      if (response!= 0) {
        if(this.NewBrandForm.value.brand_id!=''){
          this.toastr.successToastr("Brand details updated successfully.");
        }else{
          this.toastr.successToastr("New Brand added successfully.");
        }
        this.closeBtn.nativeElement.click();
        this.getBrands();
      } else{
        this.toastr.successToastr("Error! While adding brand");
      }
    });
  }

  createBrandForm(){
    this.submitted = false;
    this.NewBrandForm = this.fb.group({
      brand_name: ["", [Validators.required]],
      brand_id:[""]
    });
  }

  editBrand(data){
    this.NewBrandForm = this.fb.group({
      brand_name: [data.brand_name, [Validators.required]],
      brand_id:[data.brand_id]
    });
  }

  get formControls() {
    return this.NewBrandForm.controls;
  }

}
