import { Component, OnInit, ElementRef, ViewChild, HostListener } from '@angular/core';
import { MachineService } from '../../../_services/machine.service'
import { ToastrManager } from 'ng6-toastr-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from "@angular/forms";

@Component({
  selector: 'app-list-machines',
  templateUrl: './list-machines.component.html',
  styleUrls: ['./list-machines.component.css']
})
export class ListMachinesComponent implements OnInit {

  @ViewChild('closeBtn', {static: false}) closeBtn: ElementRef<HTMLElement>;
  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    this.closeBtn.nativeElement.click();
    this.submitted = false;
  }

  NewMachineForm: FormGroup;

  machines: [];
  history:[]
  submitted = false;
  p:number=1;
  action = 'add';
  userRole = '';
  selectedMachine:{};
  boolExistError=false;

  constructor(private api:MachineService, 
    private toastr:ToastrManager, 
    private fb: FormBuilder) { }

  ngOnInit() {
    this.userRole = localStorage.getItem('userRole');
    this.createMachineFormGroup();
    this.getAllMachines();
  }

  getAllMachines(){
    this.api.getAllMachines().subscribe( response => {
      this.machines =  response;
    });
  }

  viewServiceHistory(m){
    this.selectedMachine = m;
    this.api.getServiceHistory({'machine':m.machine_id}).subscribe( response => {
      this.history =  response;
    });
  }

  addorEditMachine(){
    this.submitted = true;
    this.validateMachinCode(this.NewMachineForm.value.serial_number);
    if (this.NewMachineForm.invalid || this.boolExistError==true) {
      return;
    }else{
      this.api.addorEditMachine(this.NewMachineForm.value).subscribe(response => {
        console.log(response);
        if (response!= 0) {
          if(this.NewMachineForm.value.machine_id!=''){
            this.toastr.successToastr("Machine Data updated successfully.");
          }else{
            this.toastr.successToastr("New Machine added successfully.");
          }
          this.closeBtn.nativeElement.click();
          this.getAllMachines();
        } else{
          this.toastr.successToastr("Error! While adding Machine");
        }
      });
    
    }
  }

  validateMachinCode(serial_number){
    this.api.validateMachinCode({'serial_number':serial_number, 'machine_id':this.NewMachineForm.value.machine_id}).subscribe(response => {
        if(response == 0){
          this.boolExistError = true;
        }else{
          this.boolExistError = false;
        }
    });
  }

  createMachineFormGroup(){
    this.submitted = false;
    this.NewMachineForm = this.fb.group({
      serial_number: ["", [Validators.required]],
      model: ["", [Validators.required]],
      machine_type:["",[Validators.required]],
      make: ["", [Validators.required]],
      machine_id: [""]
    });
  }

  editMachine(data){
    this.submitted = false;
    this.NewMachineForm = this.fb.group({
      serial_number: [data.serial_number, [Validators.required]],
      model: [data.model, [Validators.required]],
      machine_type:[data.machine_type,[Validators.required]],
      make: [data.make, [Validators.required]],
      machine_id: [data.machine_id]
    });
  }

  get formControls() {
    return this.NewMachineForm.controls;
  }

  closeDialog(){
    this.boolExistError = false;
  }

}
