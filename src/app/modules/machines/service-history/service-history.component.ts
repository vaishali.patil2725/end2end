import { Component, OnInit, ElementRef, ViewChild, HostListener } from '@angular/core';
import { MachineService } from '../../../_services/machine.service'
import { ToastrManager } from 'ng6-toastr-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from "@angular/forms";

@Component({
  selector: 'app-service-history',
  templateUrl: './service-history.component.html',
  styleUrls: ['./service-history.component.css']
})
export class ServiceHistoryComponent implements OnInit {

  @ViewChild('closeBtn', {static: false}) closeBtn: ElementRef<HTMLElement>;
  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    this.closeBtn.nativeElement.click();
    this.submitted = false;
  }

  NewServiceForm: FormGroup;
  filterForm:FormGroup;

  history : [];
  machines : [];
  submitted = false;
  p:number=1;
  action = 'add';
  userRole = '';

  constructor(private api:MachineService, 
    private toastr:ToastrManager, 
    private fb: FormBuilder) { }

  ngOnInit() {
    this.filterForm = this.fb.group({
      machine:[""],
    });
    
    this.userRole = localStorage.getItem('userRole');
    this.createFormGroup();
    this.getServiceHistory();
    this.getAllMachines();
  }

  getServiceHistory(){
    this.api.getServiceHistory(this.filterForm.value).subscribe( response => {
      this.history =  response;
    });
  }

  getAllMachines(){
    this.api.getAllMachines().subscribe( response => {
      this.machines =  response;
    });
  }
  
  resetFilters(){
    this.filterForm = this.fb.group({
      machine:[""],
    });
    this.getServiceHistory();
  }

  addorEditServiceHostory(){
    this.submitted = true;
    if (this.NewServiceForm.invalid) {
      return;
    }
    this.api.addorEditServiceHostory(this.NewServiceForm.value).subscribe(response => {
      if (response!= 0) {
        if(this.NewServiceForm.value.buyer_id!=''){
          this.toastr.successToastr("History updated successfully.");
        }else{
          this.toastr.successToastr("History added successfully.");
        }
        this.closeBtn.nativeElement.click();
        this.getServiceHistory();
      } else{
        this.toastr.successToastr("Error! While adding Service History");
      }
    });
  }

  createFormGroup(){
    this.submitted = false;
    this.NewServiceForm = this.fb.group({
      machine: ["", [Validators.required]],
      date: ["", [Validators.required]],
      status:["",[Validators.required]],
      remark: ["", [Validators.required]],
      service_id: [""]
    });
  }

  editHistory(data){
    this.submitted = false;
    this.NewServiceForm = this.fb.group({
      machine: [data.machine, [Validators.required]],
      date: [data.date, [Validators.required]],
      status:[data.status,[Validators.required]],
      remark: [data.remark, [Validators.required]],
      service_id: [data.service_id]
    });
  }

  get formControls() {
    return this.NewServiceForm.controls;
  }

}
