import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxPaginationModule } from 'ngx-pagination';
import { ReactiveFormsModule , FormsModule } from '@angular/forms';

import { MachinesRoutingModule } from './machines-routing.module';
import { MachinesComponent } from './machines.component';
import { ListMachinesComponent } from './list-machines/list-machines.component';
import { ServiceHistoryComponent } from './service-history/service-history.component';


@NgModule({
  declarations: [MachinesComponent, ListMachinesComponent, ServiceHistoryComponent],
  imports: [
    CommonModule,
    MachinesRoutingModule,
    NgxPaginationModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class MachinesModule { }
