import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MachinesComponent } from './machines.component';
import { ListMachinesComponent } from './list-machines/list-machines.component';
import { ServiceHistoryComponent } from './service-history/service-history.component';

const routes: Routes = [
  { path: '', component: MachinesComponent },
  { path: 'list-machines', component: ListMachinesComponent },
  { path: 'service-history', component: ServiceHistoryComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MachinesRoutingModule { }
