import { Component, OnInit, ViewChild, ElementRef, HostListener } from '@angular/core';
import { PurchaseOrderService } from '../../../_services/purchase-order.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ViewStyleComponent } from '../../shared/view-style/view-style.component';
import { ExcelService } from '../../../_services/excel.service';

declare var $;

@Component({
  selector: 'app-list-orders',
  templateUrl: './list-orders.component.html',
  styleUrls: ['./list-orders.component.css']
})
export class ListOrdersComponent implements OnInit {
  @ViewChild('closeBtn', {static: false}) closeBtn: ElementRef<HTMLElement>;
  @ViewChild(ViewStyleComponent, {static: true}) style: ViewStyleComponent;
  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    this.closeBtn.nativeElement.click();
    this.submitted = false;
  }
  
  PurchaseOrderForm: FormGroup;
  filterForm: FormGroup;

  purchase_orders=[];
  styles:[];
  buyers:[];
  companies:[];
  p:number=1;
  submitted = false;
  userRole = '';

  constructor(private api:PurchaseOrderService, 
    private excelService: ExcelService,
    private toastr:ToastrManager, 
    private fb: FormBuilder) { }

  ngOnInit() {
    this.filterForm = this.fb.group({
      po_company:[""],
      po_title:[""],
      po_style:[""],
      po_date_issued:[""],
    });

    this.userRole = localStorage.getItem('userRole');
    this.createPurchaseOrderFormGroup();
    this.getFilteredPurchaseOrders();
    this.getRequiredInitialData();
  }

  getFilteredPurchaseOrders(){
    this.api.getFilteredPurchaseOrders(this.filterForm.value).subscribe(response => {
      this.purchase_orders =  response;
    });
  }

  getRequiredInitialData(){
    this.api.getRequiredInitialData().subscribe( response => {
      this.companies =  response.companies;
      this.styles =  response.styles;
      this.buyers =  response.buyers;
    });
  }

  addorEditPurchaseOrder(){
    this.submitted = true;
    console.log(this.PurchaseOrderForm);
    if (this.PurchaseOrderForm.invalid) {
      return;
    }
    this.api.addorEditPurchaseOrder(this.PurchaseOrderForm.value).subscribe(response => {
      if (response!= 0) {
        if(this.PurchaseOrderForm.value.order_id != ''){
          this.toastr.successToastr("Purchase Order updated successfully.");
        }else{
          this.toastr.successToastr("New Purchase Order added successfully.");
        }
      } else{
        this.toastr.errorToastr("Error! While adding Purchase Order");
      }
      this.closeBtn.nativeElement.click();
      this.getFilteredPurchaseOrders();
    });
  }

  createPurchaseOrderFormGroup(){
    this.submitted = false;
    this.PurchaseOrderForm = this.fb.group({
      order_title:["",Validators.required],
      date_issued: ["", Validators.required],
      season: ["", Validators.required],
      PAN_Number: ["", [Validators.required,Validators.maxLength(10),Validators.pattern("^[A-Za-z]{5}[0-9]{4}[A-Za-z]$")]],
      GST_Number: ["",[Validators.required,Validators.maxLength(15)]],
      //CIN_Number:["",[Validators.required,Validators.maxLength(21), Validators.pattern("^[0-9]{2}[A-Za-z]{5}[0-9]{4}[A-Za-z]{1}[0-9]{1}[A-Za-z]{1}[0-9]{1}$")]],
      CIN_Number:["",[Validators.required,Validators.maxLength(21)]],
      buyer_id: ["", Validators.required],
      poc_name: ["", Validators.required],
      style_id: ["", Validators.required],
      quantity: ["", Validators.required],                                                
      value: ["",Validators.required],
      currency: ["", Validators.required],
      delivery_terms: ["",Validators.required],
      payment_terms: ["", Validators.required],
      order_id:[""]
    });
  }

  editPurchaseOrder(po){
    this.PurchaseOrderForm = this.fb.group({
      order_title:[po.order_title,[Validators.required]],
      PAN_Number: [po.PAN_Number, [Validators.required]],
      GST_Number: [po.GST_Number, [Validators.required]],
      CIN_Number:[po.CIN_Number,[Validators.required]],
      buyer_id: [po.buyer_id, [Validators.required]],
      poc_name: [po.poc_name, [Validators.required]],
      style_id: [po.style_id, [Validators.required]],
      quantity: [po.quantity, [Validators.required]],
      value: [po.value, [Validators.required]],
      currency: [po.currency, [Validators.required]],
      season: [po.season, [Validators.required]],
      payment_terms: [po.payment_terms, [Validators.required]],
      delivery_terms: [po.delivery_terms, [Validators.required]],
      date_issued: [po.date_issued, [Validators.required]],
      order_id:[po.order_id]
    });
  }

  resetFilters(){
    this.filterForm.reset();
    this.getFilteredPurchaseOrders();
  }

  get formControls() {
    return this.PurchaseOrderForm.controls;
  }

  viewStyle(style_id){
    this.style.showStyleDetails(style_id);
  }

  generateExcel() {
    let title = 'Purchase Orders Report';
    let header = ['Purchase Order', 'PAN Number', 'GST Number', 'CIN Number', 'Style Code', 'Buyer', 'Buyer Email', 'POC', 'Date Issued', 'Quantity', 'Value', 'Currency', 'Delivery Terms', 'Payment Terms', 'Season', 'Created By', 'Created On'];
    let data = this.purchase_orders
    var result = [];
    Object.keys(data).forEach(function(key) {
      let data_row = [];
      data_row.push(data[key]["order_title"]);
      data_row.push(data[key]["PAN_Number"]);
      data_row.push(data[key]["GST_Number"]);
      data_row.push(data[key]["CIN_Number"]);
      data_row.push(data[key]["style"]);
      data_row.push(data[key]["buyer"]);
      data_row.push(data[key]["buyer_email"]);
      data_row.push(data[key]["poc_name"]);
      data_row.push(data[key]["date_issued"]);
      data_row.push(data[key]["quantity"]);
      data_row.push(data[key]["value"]);
      data_row.push(data[key]["currency"]);
      data_row.push(data[key]["delivery_terms"]);
      data_row.push(data[key]["payment_terms"]);
      data_row.push(data[key]["season"]);
      data_row.push(data[key]["created_by_name"]);
      data_row.push(data[key]["created_on"]);

      result.push(data_row);
    });
    this.excelService.generateExcel(title, header, result);
  }
}
