import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PurchaseOrderComponent } from './purchase-order.component';
import { ViewOrderComponent } from './view-order/view-order.component';
import { ListOrdersComponent } from './list-orders/list-orders.component';

const routes: Routes = [
  { path: '',
      component:PurchaseOrderComponent,
      children :[
          { path: '', component: ListOrdersComponent},
          { path: 'view-order/:id', component: ViewOrderComponent}
      ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PurchaseOrderRoutingModule { }
