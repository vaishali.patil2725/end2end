import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PurchaseOrderService } from '../../../_services/purchase-order.service';

@Component({
  selector: 'app-view-order',
  templateUrl: './view-order.component.html',
  styleUrls: ['./view-order.component.css']
})
export class ViewOrderComponent implements OnInit {

  order_id:number;
  purchase_order={
    order_id:''
  };

  constructor(private route: ActivatedRoute,
    private api: PurchaseOrderService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.order_id = params['id'];
    });
    this.getPurchaseOrderDetails();
  }

  getPurchaseOrderDetails(){
    this.api.getPurchaseOrderDetails({'order_id':this.order_id}).subscribe( response => {
      this.purchase_order = response;
    });
  }

}
