import { Component, OnInit, ViewChild} from '@angular/core';
import { Router } from '@angular/router';
import { SharedService } from '../../_services/shared.service';
import { UnitService } from '../../_services/unit.service';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { CuttingDataComponent } from '../../modules/production/cutting-data/cutting-data.component';
import { ProductionService } from '../../_services/production.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  @ViewChild(CuttingDataComponent, {static: false}) cutting: CuttingDataComponent;

  filterForm: FormGroup;
  
  departments:[];
  units:[];
  company:any;
  unit:any;
  submitted=false;

  constructor(private shared:SharedService,
    private unitService:UnitService,
    private api:ProductionService,
    private fb: FormBuilder,
    private route:Router) {}

  ngOnInit() {
    if(sessionStorage.getItem('userToken') === null){
      this.route.navigate(['/']);
    }

    this.company = localStorage.getItem('company');
    this.unit = localStorage.getItem('unit');

    if(this.unit == 'null' || this.unit == null || this.unit == 0){
      this.unit = "";
      this.unitService.getUnits().subscribe(response => {
        this.units =  response;
      });
    }
    
    this.filterForm = this.fb.group({
      from_date:[""],
      to_date:[""],
      unit:[this.unit, [Validators.required]],
      report_type:["", [Validators.required]]
    });

    this.getDepartments();

  }

  getDepartments(){
    this.shared.getDepartments().subscribe(response => {
      this.departments =  response;
    });
  }

  getReportsData(){
    if(this.filterForm.invalid){
      this.submitted = true;
      return false;
    }

    this.shared.getReportsData(this.filterForm.value).subscribe(response => {
      this.api.downloadReports(this.filterForm.value.report_type, response);
    });
  }

  resetFilters(){
    this.filterForm.reset();
    this.filterForm.patchValue({unit:this.unit})
  }

  get formControls() {
    return this.filterForm.controls;
  }

}
