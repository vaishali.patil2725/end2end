import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxPaginationModule } from 'ngx-pagination';
import { ReactiveFormsModule , FormsModule } from '@angular/forms';

import { BuyersRoutingModule } from './buyers-routing.module';
import { BuyersComponent } from './buyers.component';
import { ListBuyersComponent } from './list-buyers/list-buyers.component';
import { ViewBuyerComponent } from './view-buyer/view-buyer.component';


@NgModule({
  declarations: [BuyersComponent, ListBuyersComponent, ViewBuyerComponent],
  imports: [
    CommonModule,
    BuyersRoutingModule,
    NgxPaginationModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class BuyersModule { }
