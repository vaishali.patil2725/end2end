import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { BuyerService } from '../../_services/buyer.service'
import { Router } from "@angular/router";
import { ToastrManager } from 'ng6-toastr-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from "@angular/forms";
declare var $;

@Component({
  selector: 'app-buyers',
  templateUrl: './buyers.component.html',
  styleUrls: ['./buyers.component.css']
})
export class BuyersComponent implements OnInit {
  @ViewChild('closeBtn', {static: false}) closeBtn: ElementRef<HTMLElement>;

  buyers: [];
  NewBuyerForm: FormGroup;
  submitted = false;
  p:number=1;

  constructor(private api:BuyerService, 
    private toastr:ToastrManager, 
    private fb: FormBuilder) { }

  ngOnInit() {
    this.createBuyerFormGroup();
    this.getBuyers();
  }

  createBuyerFormGroup(){
    this.submitted = false;
    this.NewBuyerForm = this.fb.group({
      full_name: ["", [Validators.required]],
      email: ["", [Validators.required]],
      phone:["",[Validators.required]],
      username: ["", [Validators.required]],
      password: ["", [Validators.required]],
      address: ["", [Validators.required]],
      city: ["", [Validators.required]],
      country: ["", [Validators.required]],
      zip: ["", [Validators.required]],
      buyer_id: [""]
    });
  }

  get formControls() {
    return this.NewBuyerForm.controls;
  }

  getBuyers(){
    this.api.getBuyers().subscribe( response => {
      this.buyers =  response;
    });
  }

  loadNewBuyerForm(){
    this.createBuyerFormGroup();
    $('#addNewBuyer').modal('show');
  }

  addorEditNewBuyer(){
    this.submitted = true;
    if (this.NewBuyerForm.invalid) {
      return;
    }
    this.api.addorEditNewBuyer(this.NewBuyerForm.value).subscribe(response => {
      if (response!= 0) {
        if(this.NewBuyerForm.value.buyer_id!=''){
          this.toastr.successToastr("Buyer updated successfully.");
        }else{
          this.toastr.successToastr("Buyer added successfully.");
        }
        this.closeBtn.nativeElement.click();
        this.getBuyers();
      } else{
        this.toastr.successToastr("Error! While adding Buyer");
      }
    });
  }

  editBuyerInformation(buyer){
    this.NewBuyerForm = this.fb.group({
      full_name: [buyer.full_name, [Validators.required]],
      email: [buyer.email, [Validators.required]],
      phone:[buyer.phone,[Validators.required]],
      username: ["buyer.username", [Validators.required]],
      password: ["sds", [Validators.required]],
      address: [buyer.address, [Validators.required]],
      city: [buyer.city, [Validators.required]],
      country: [buyer.country, [Validators.required]],
      zip: [buyer.zip, [Validators.required]],
      buyer_id: [buyer.buyer_id]
    });
  }


}
