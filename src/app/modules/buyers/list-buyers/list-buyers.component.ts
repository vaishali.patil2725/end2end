import { Component, OnInit, ElementRef, ViewChild, HostListener } from '@angular/core';
import { BuyerService } from '../../../_services/buyer.service'
import { ToastrManager } from 'ng6-toastr-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from "@angular/forms";

@Component({
  selector: 'app-list-buyers',
  templateUrl: './list-buyers.component.html',
  styleUrls: ['./list-buyers.component.css']
})

export class ListBuyersComponent implements OnInit {
  @ViewChild('closeBtn', {static: false}) closeBtn: ElementRef<HTMLElement>;
  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    this.closeBtn.nativeElement.click();
    this.submitted = false;
  }

  NewBuyerForm: FormGroup;

  buyers: [];
  submitted = false;
  p:number=1;
  action = 'add';
  userRole = '';

  constructor(private api:BuyerService, 
    private toastr:ToastrManager, 
    private fb: FormBuilder) { }

  ngOnInit() {
    this.userRole = localStorage.getItem('userRole');
    this.createBuyerFormGroup();
    this.getBuyers();
  }

  getBuyers(){
    this.api.getBuyers().subscribe( response => {
      this.buyers =  response;
    });
  }

  addorEditNewBuyer(){
    this.submitted = true;
    console.log(this.NewBuyerForm);
    if (this.NewBuyerForm.invalid) {
      return;
    }
    this.api.addorEditNewBuyer(this.NewBuyerForm.value).subscribe(response => {
      if (response!= 0) {
        if(this.NewBuyerForm.value.buyer_id!=''){
          this.toastr.successToastr("Buyer updated successfully.");
        }else{
          this.toastr.successToastr("Buyer added successfully.");
        }
        this.closeBtn.nativeElement.click();
        this.getBuyers();
      } else{
        this.toastr.successToastr("Error! While adding Buyer");
      }
    });
  }

  createBuyerFormGroup(){
    this.submitted = false;
    this.action = 'add';
    this.NewBuyerForm = this.fb.group({
      full_name: ["", [Validators.required]],
      email: ["", [Validators.required,Validators.email,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      phone:["",[Validators.required]],
      username: ["", [Validators.required]],
      password: ["", [Validators.required, Validators.minLength(6)]],
      address: ["", [Validators.required]],
      city: ["", [Validators.required]],
      country: ["", [Validators.required]],
      zip: ["", [Validators.required, Validators.pattern('^[0-9]{1,6}$')]],
      buyer_id: [""]
    });
  }

  editBuyerInformation(buyer){
    console.log(buyer);
    this.submitted = false;
    this.action = 'update';
    this.NewBuyerForm = this.fb.group({
      full_name: [buyer.full_name, [Validators.required]],
      email: [buyer.email,[Validators.required,Validators.email,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      phone:[buyer.phone,[Validators.required]],
      username: [""],
      password: [""],
      address: [buyer.address, [Validators.required]],
      city: [buyer.city, [Validators.required]],
      country: [buyer.country, [Validators.required]],
      zip: [buyer.zip, [Validators.required, Validators.pattern('^[0-9]{1,6}$')]],
      buyer_id: [buyer.buyer_id]
    });
  }

  get formControls() {
    return this.NewBuyerForm.controls;
  }

}
