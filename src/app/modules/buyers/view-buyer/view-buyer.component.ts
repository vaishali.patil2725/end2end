import { Component, OnInit } from '@angular/core';
import { BuyerService } from '../../../_services/buyer.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-view-buyer',
  templateUrl: './view-buyer.component.html',
  styleUrls: ['./view-buyer.component.css']
})

export class ViewBuyerComponent implements OnInit {

  purchase_orders = [];

  buyer_id:any;
  buyer={};

  constructor(private api:BuyerService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.buyer_id = this.route.snapshot.paramMap.get("id");
    this.getBuyerDetails();
  }

  getBuyerDetails(){
    this.api.getBuyerDetails(this.buyer_id).subscribe( response => {
      this.buyer = response.buyer_data;
      this.purchase_orders = response.purchase_orders;
    });
  }

}
