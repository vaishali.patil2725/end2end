import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BuyersComponent } from './buyers.component';
import { ListBuyersComponent } from './list-buyers/list-buyers.component';
import { ViewBuyerComponent } from './view-buyer/view-buyer.component';

const routes: Routes = [
  { path: '',
      component:BuyersComponent,
      children :[
          { path: '', component: ListBuyersComponent},
          { path: 'view-buyer/:id', component: ViewBuyerComponent}
      ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BuyersRoutingModule { }
