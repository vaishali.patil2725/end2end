import { Component, OnInit, ViewChild, ElementRef, Input, HostListener } from '@angular/core';
import { ErpService } from '../../../_services/erp.service';
import { PurchaseOrderService } from '../../../_services/purchase-order.service';
import { EmployeeService } from '../../../_services/employee.service';
import { UnitService } from '../../../_services/unit.service';

import { ToastrManager } from 'ng6-toastr-notifications';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

@Component({
  selector: 'app-delivery-schedule',
  templateUrl: './delivery-schedule.component.html',
  styleUrls: ['./delivery-schedule.component.css']
})
export class DeliveryScheduleComponent implements OnInit {
  @ViewChild('closeBtn', {static: false}) closeBtn: ElementRef<HTMLElement>;
  @Input() erp_id: number;
  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    this.closeBtn.nativeElement.click();
    this.submitted = false;
  }

  deliveryForm:FormGroup;

  schedules=[];
  dispatch_lots:[];
  units:[];

  submitted = false;
  userRole = '';
  po_number:number;
 
  userDesignation = '';

  constructor(private api:ErpService, 
    private poService:PurchaseOrderService,
    private empService:EmployeeService,
    private unitService:UnitService,
    private toastr:ToastrManager, 
    private fb: FormBuilder) { }

  ngOnInit() {
    this.createDeliveryForm();
    this.userRole = localStorage.getItem('userRole');
    this.userDesignation = localStorage.getItem('userDesignation');
    this.getDeliverySchedules();
    this.getUnits(); 
    this.api.getErpInformaton(this.erp_id).subscribe( response => {
      this.po_number = response.po_number;
      this.getDispatchLots({'order_id':response.po_number});
    });
  }

  getDeliverySchedules(){
    this.api.getDeliverySchedules(this.erp_id).subscribe( response => {
      this.schedules = response;
    });
  }
  
  getDispatchLots(order){
    this.poService.getDispatchLots().subscribe( response => {
      this.dispatch_lots = response;
    });
  }

  getUnits(){
    this.unitService.getUnits().subscribe( response => {
      this.units = response;
    });
  }

  addorUpdateDeliverySchedule(){
    this.submitted = true;
    if (this.deliveryForm.invalid) {
      return;
    }
    this.api.addorUpdateDeliverySchedule(this.deliveryForm.value).subscribe(response => {
      if (response!= 0) {
        if(this.deliveryForm.value.id != ''){
          this.toastr.successToastr("Delivery schedule updated successfully.");
        }else{
          this.toastr.successToastr("Delivery schedule added successfully.");
        }
        this.getDeliverySchedules();
        this.closeBtn.nativeElement.click();
      } else{
        this.toastr.errorToastr("Error! Please try later.");
      }
    });
  }

  createDeliveryForm(){
    this.submitted = false;
    this.deliveryForm = this.fb.group({
      lot_number: ["", [Validators.required]],
      dispatch_date: ["", [Validators.required]],
      unit: ["", [Validators.required]],
      mode: ["",[Validators.required]],
      destination: ["", [Validators.required]],
      country: ["", [Validators.required]],
      zip: ["", [Validators.required]],
      helper:["",[Validators.required]],
      erp:[this.erp_id]
    });

    this.poService.getLotsToAddDeliverySchedule(this.po_number, this.erp_id).subscribe( response => {
      this.dispatch_lots = response;
    });
  }

  editDeliverySchedule(data){
    this.deliveryForm = this.fb.group({
      lot_number: [data.lot_number, [Validators.required]],
      dispatch_date: [data.dispatch_date, [Validators.required]],
      unit: [data.unit, [Validators.required]],
      mode: [data.mode,[Validators.required]],
      destination: [data.destination, [Validators.required]],
      country: [data.country, [Validators.required]],
      zip: [data.zip, [Validators.required]],
      helper:[data.helper,[Validators.required]],
      erp:[this.erp_id],
      id:[data.id]
    });
  }

  get formControls() {
    return this.deliveryForm.controls;
  }

}
