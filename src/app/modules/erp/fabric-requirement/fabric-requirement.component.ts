import { Component, OnInit, ViewChild, ElementRef, Input, HostListener } from '@angular/core';
import { ErpService } from '../../../_services/erp.service';

import { ToastrManager } from 'ng6-toastr-notifications';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

@Component({
  selector: 'app-fabric-requirement',
  templateUrl: './fabric-requirement.component.html',
  styleUrls: ['./fabric-requirement.component.css']
})
export class FabricRequirementComponent implements OnInit {
  @ViewChild('closeBtn', {static: false}) closeBtn: ElementRef<HTMLElement>;
  @Input() erp_id: number;
  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    this.closeBtn.nativeElement.click();
    this.submitted = false;
  }

  requirementForm:FormGroup;

  requirements=[];
  selected_data:{};
  submitted = false;
  userRole = '';
  userDesignation = '';

  constructor(private api:ErpService, 
    private toastr:ToastrManager, 
    private fb: FormBuilder) { }

  ngOnInit() {
    this.userRole = localStorage.getItem('userRole');
    this.userDesignation = localStorage.getItem('userDesignation');
    this.createRequirementForm();
    this.getFabricRequirements();
  }

  getFabricRequirements(){
    this.api.getFabricRequirements(this.erp_id).subscribe( response => {
      this.requirements = response;
    });
  }

  addorUpdateRequirementLog(){
    this.submitted = true;
    if (this.requirementForm.invalid) {
      return;
    }
    this.api.addorUpdateRequirementLog(this.requirementForm.value).subscribe(response => {
     console.log(response)
      if (response!= 0) {
        if(this.requirementForm.value.requirement_id != ''){
          this.toastr.successToastr("Fabric Requirment data updated successfully.");
        }else{
          this.toastr.successToastr("Fabric Requirement added successfully.");
        }
        this.getFabricRequirements();
        //this.closeBtn.nativeElement.click();
      } else{
        this.toastr.errorToastr("Error! Please try later.");
      }
    });
  }

  createRequirementForm(){
    this.submitted = false;
    this.requirementForm = this.fb.group({
      erp:[this.erp_id],
      requirement_type:["fabric"],
      category: ["", [Validators.required]],
      item_color: ["", [Validators.required]],
      item_color_code: ["",[Validators.required]],
      item_code: ["", [Validators.required]],
      item_description: ["", [Validators.required]],
      cotton_content: ["", [Validators.required]],
      width:["",[Validators.required]],
      consumption:["", [Validators.required]],
      rate: ["", [Validators.required]],
      quantity: ["", [Validators.required]],
      booked_quantity: ["",[Validators.required]],
      extra_needed: ["", [Validators.required]],
      colorwise_booking: ["", [Validators.required]],
      sizewise_booking: ["", [Validators.required]],
      booked_status:["",[Validators.required]],
      dispatch_status:["", [Validators.required]],
      dispatch_date: ["", [Validators.required]],
      ETA: ["", [Validators.required]],
      payment_terms:["",[Validators.required]],
      delivery_terms:["", [Validators.required]],
      requirement_id:[""]
    });

  }
  
  editRequirementData(data){
    this.requirementForm = this.fb.group({
      erp:[this.erp_id],
      requirement_type:["Fabric"],
      category: [data.category, [Validators.required]],
      item_color: [data.item_color, [Validators.required]],
      item_color_code: [data.item_color_code,[Validators.required]],
      item_code: [data.item_code, [Validators.required]],
      item_description: [data.item_description, [Validators.required]],
      cotton_content: [data.cotton_content, [Validators.required]],
      width:[data.width,[Validators.required]],
      consumption:[data.consumption, [Validators.required]],
      rate: [data.rate, [Validators.required]],
      quantity: [data.quantity, [Validators.required]],
      booked_quantity: [data.booked_quantity,[Validators.required]],
      extra_needed: [data.extra_needed, [Validators.required]],
      colorwise_booking: [data.colorwise_booking, [Validators.required]],
      sizewise_booking: [data.sizewise_booking, [Validators.required]],
      booked_status:[data.booked_status,[Validators.required]],
      dispatch_status:[data.dispatch_status, [Validators.required]],
      dispatch_date: [data.dispatch_date, [Validators.required]],
      ETA: [data.ETA, [Validators.required]],
      payment_terms:[data.payment_terms,[Validators.required]],
      delivery_terms:[data.delivery_terms, [Validators.required]],
      requirement_id:[data.requirement_id]
    });
  }

  viewFabricRequirement(data){
    this.selected_data = data;
  }


  get formControls() {
    return this.requirementForm.controls;
  }


}
