import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FabricRequirementComponent } from './fabric-requirement.component';

describe('FabricRequirementComponent', () => {
  let component: FabricRequirementComponent;
  let fixture: ComponentFixture<FabricRequirementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FabricRequirementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FabricRequirementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
