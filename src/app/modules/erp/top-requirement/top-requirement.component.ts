import { Component, OnInit, Input, ViewChild, ElementRef, HostListener } from '@angular/core';
import { ErpService } from '../../../_services/erp.service';

import { ToastrManager } from 'ng6-toastr-notifications';
import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";

@Component({
  selector: 'app-top-requirement',
  templateUrl: './top-requirement.component.html',
  styleUrls: ['./top-requirement.component.css']
})
export class TopRequirementComponent implements OnInit {
  @ViewChild('closeTopBtn', {static: false}) closeTopBtn: ElementRef<HTMLElement>;
  @Input() erp_id: number;
  @Input() sizing_type: number;

  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    this.closeTopBtn.nativeElement.click();
    this.submitted = false;
  }

  topMaterialForm:FormGroup;
  alphaNumericForm:FormGroup;
  numericForm:FormGroup;
  numericFormFields:FormGroup;

  tops = [];
  numbers = [];
  numericSizes = [];

  submitted = false;
  userRole = '';
  userDesignation = '';
  
  constructor(private api:ErpService,
    private toastr: ToastrManager, 
    private fb: FormBuilder) {
      this.numbers = new Array(100).fill(1).map((x,i)=>i+1);
    }

  ngOnInit() {
    this.userRole = localStorage.getItem('userRole');
    this.userDesignation = localStorage.getItem('userDesignation');
    this.createTopRequirementForm();
    this.getTopMaterialRequirements();
    this.alphaNumericForm = this.fb.group({
      xs:[""],
      s:[""],
      m:[""],
      l:[""],
      xl:[""],
      xxl:[""],
      xxxl:[""],
    });
    this.numericForm = this.fb.group({
      start:["", Validators.required],
      end:["", Validators.required],
    });
    this.numericFormFields = new FormGroup({});
  }
  
  addNumericForm(){
    const fromDataObj = {};
    if(this.numericForm.valid){
      if(this.numericForm.value.start > this.numericForm.value.end){
        alert('please enter correct range');
        return;
      }
      this.numericSizes = [];
      for(let i=this.numericForm.value.start; i<=this.numericForm.value.end;i++){
        fromDataObj['size'+i] = new FormControl("")
        this.numericSizes.push('size'+i);
      }
      this.numericFormFields = new FormGroup(fromDataObj);
    }
  }

  getTopMaterialRequirements(){
    this.api.getTopMaterialRequirements(this.erp_id).subscribe( response => {
      this.tops = response;
    });
  }

  addorEditTopMaterialRequirement(){
    this.submitted = true;
    if (this.topMaterialForm.invalid) {
      return;
    }
    if(this.sizing_type == 1){
      this.topMaterialForm.addControl('sizes', this.alphaNumericForm);
    }else{
      this.topMaterialForm.addControl('sizes', this.numericFormFields);
      this.topMaterialForm.addControl('range', this.numericForm);
    }
    this.api.addorEditTopMaterialRequirement(this.topMaterialForm.value).subscribe(response => {
      if (response!= 0) {
        if(this.topMaterialForm.value.id != ''){
          this.toastr.successToastr("Requirement updated successfully.");
        }else{
          this.toastr.successToastr("Requirement added successfully.");
        }
        this.getTopMaterialRequirements();
        this.closeTopBtn.nativeElement.click();

      } else{
        this.toastr.errorToastr("Error! Please try later.");
      }
    });
  }

  createTopRequirementForm(){
    this.submitted = false;
    this.topMaterialForm = this.fb.group({
      color:["",[Validators.required]],
      color_code:["",[Validators.required]],
      erp_id:[this.erp_id],
      sizing_type:[this.sizing_type],
      id:[]
    });
    this.numericSizes = [];
    this.alphaNumericForm = this.fb.group({
      xs:[""],
      s:[""],
      m:[""],
      l:[""],
      xl:[""],
      xxl:[""],
      xxxl:[""],
    });
    this.numericForm = this.fb.group({
      start:["", Validators.required],
      end:["", Validators.required],
    });
    this.numericFormFields = new FormGroup({});
  }
  
  editTopMaterialRequirement(data){
    this.submitted = false;
    this.topMaterialForm = this.fb.group({
      color:[data.color,[Validators.required]],
      color_code:[data.color_code,[Validators.required]],
      erp_id:[this.erp_id],
      sizing_type:[this.sizing_type],
      id:[data.id]
    });

    if(this.sizing_type == 1){
      this.alphaNumericForm = this.fb.group({
        xs:[data.top_requirements.xs],
        s:[data.top_requirements.s],
        m:[data.top_requirements.m],
        l:[data.top_requirements.l],
        xl:[data.top_requirements.xl],
        xxl:[data.top_requirements.xxl],
        xxxl:[data.top_requirements.xxxl],
      });
    }
    if(this.sizing_type == 0){
      const fromDataObj = {};
      this.numericSizes = [];
      let ns = [];
      let ps = [];
      let i= 0;
      Object.keys(data.top_requirements).forEach(function(key) {
        var k = key;
        i++;
        console.log(i);
        if(i==1 || Object.keys(data.top_requirements).length == i){
          ps.push(parseInt(k.replace('size','')))
          console.log(ps);
        }
        fromDataObj[key] = new FormControl(data.top_requirements[key])
        ns.push(key);
      });

      this.numericForm = this.fb.group({
        start:[ps[0], Validators.required],
        end:[ps[1], Validators.required],
      }); 
      this.numericSizes = ns;
      this.numericFormFields = new FormGroup(fromDataObj);
    }
  }
  
  get formTopControls() {
    return this.topMaterialForm.controls;
  }

}
