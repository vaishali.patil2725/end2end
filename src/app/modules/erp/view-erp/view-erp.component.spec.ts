import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewErpComponent } from './view-erp.component';

describe('ViewErpComponent', () => {
  let component: ViewErpComponent;
  let fixture: ComponentFixture<ViewErpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewErpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewErpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
