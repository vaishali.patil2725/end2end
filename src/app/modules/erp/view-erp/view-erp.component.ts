import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ErpService } from '../../../_services/erp.service';

@Component({
  selector: 'app-view-erp',
  templateUrl: './view-erp.component.html',
  styleUrls: ['./view-erp.component.css']
})
export class ViewErpComponent implements OnInit {

  erp_id:number;
  sizing_type:number;
  submitted = false;
  userDesignation = '';

  erp={
    erp_title:'',
    quantity:'',
    rate:'',
    currency:'',
    status:'',
    season:'',
    wash_type:'',
    style_name:'',
    order_title:''
  };
  
  constructor(private route: ActivatedRoute, 
    private api:ErpService) { }

  ngOnInit() {
    this.userDesignation = localStorage.getItem('userDesignation');
    this.route.params.subscribe(params => {
      this.erp_id = params['id'];
    });
    this.api.getErpInformaton(this.erp_id).subscribe( response => {
      this.erp =  response;
      this.sizing_type = response.sizing_type;
    });
  }
}
