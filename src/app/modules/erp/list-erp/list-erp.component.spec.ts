import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListErpComponent } from './list-erp.component';

describe('ListErpComponent', () => {
  let component: ListErpComponent;
  let fixture: ComponentFixture<ListErpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListErpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListErpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
