import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrimRequirementComponent } from './trim-requirement.component';

describe('TrimRequirementComponent', () => {
  let component: TrimRequirementComponent;
  let fixture: ComponentFixture<TrimRequirementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrimRequirementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrimRequirementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
