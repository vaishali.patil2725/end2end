import { Component, OnInit, ViewChild, ElementRef, Input, HostListener } from '@angular/core';
import { ErpService } from '../../../_services/erp.service';

import { ToastrManager } from 'ng6-toastr-notifications';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

@Component({
  selector: 'app-trim-requirement',
  templateUrl: './trim-requirement.component.html',
  styleUrls: ['./trim-requirement.component.css']
})
export class TrimRequirementComponent implements OnInit {

  @ViewChild('closeBtn', {static: false}) closeBtn: ElementRef<HTMLElement>;
  @Input() erp_id: number;
  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    this.closeBtn.nativeElement.click();
    this.submitted = false;
  }

  requirementForm:FormGroup;

  requirements=[];
  submitted = false;
  userRole = '';
  userDesignation = '';
  selected_data:{};

  constructor(private api:ErpService, 
    private toastr:ToastrManager, 
    private fb: FormBuilder) { }

  ngOnInit() {
    this.userRole = localStorage.getItem('userRole');
    this.userDesignation = localStorage.getItem('userDesignation');
    this.createRequirementForm();
    this.getTrimRequirements();
  }

  getTrimRequirements(){
    this.api.getTrimRequirements(this.erp_id).subscribe( response => {
      this.requirements = response;
    });
  }

  addorUpdateRequirementLog(){
    this.submitted = true;
    if (this.requirementForm.invalid) {
      return;
    }
    this.api.addorUpdateRequirementLog(this.requirementForm.value).subscribe(response => {
      if (response!= 0) {
        if(this.requirementForm.value.requirement_id != ''){
          this.toastr.successToastr("Trim Requirment data updated successfully.");
        }else{
          this.toastr.successToastr("Trim Requirement added successfully.");
        }
        this.getTrimRequirements();
        this.closeBtn.nativeElement.click();
      } else{
        this.toastr.errorToastr("Error! Please try later.");
      }
    });
  }

  createRequirementForm(){
    this.submitted = false;
    this.requirementForm = this.fb.group({
      erp:[this.erp_id],
      requirement_type:["trim"],
      category: ["", [Validators.required]],
      item_color: ["", [Validators.required]],
      item_color_code: ["",[Validators.required]],
      item_code: ["", [Validators.required]],
      item_description: ["", [Validators.required]],
      consumption:["", [Validators.required]],
      rate: ["", [Validators.required]],
      quantity: ["", [Validators.required]],
      booked_quantity: ["",[Validators.required]],
      extra_needed: ["", [Validators.required]],
      colorwise_booking: ["", [Validators.required]],
      sizewise_booking: ["", [Validators.required]],
      booked_status:["",[Validators.required]],
      dispatch_status:["", [Validators.required]],
      dispatch_date: ["", [Validators.required]],
      ETA: ["", [Validators.required]],
      payment_terms:["",[Validators.required]],
      delivery_terms:["", [Validators.required]],
      requirement_id:[""]
    });

  }
  
  editRequirementData(data){
    this.requirementForm = this.fb.group({
      erp:[this.erp_id],
      requirement_type:["Trim"],
      category: [data.category, [Validators.required]],
      item_color: [data.item_color, [Validators.required]],
      item_color_code: [data.item_color_code,[Validators.required]],
      item_code: [data.item_code, [Validators.required]],
      item_description: [data.item_description, [Validators.required]],
      consumption:[data.consumption, [Validators.required]],
      rate: [data.rate, [Validators.required]],
      quantity: [data.quantity, [Validators.required]],
      booked_quantity: [data.booked_quantity,[Validators.required]],
      extra_needed: [data.extra_needed, [Validators.required]],
      colorwise_booking: [data.colorwise_booking, [Validators.required]],
      sizewise_booking: [data.sizewise_booking, [Validators.required]],
      booked_status:[data.booked_status,[Validators.required]],
      dispatch_status:[data.dispatch_status, [Validators.required]],
      dispatch_date: [data.dispatch_date, [Validators.required]],
      ETA: [data.ETA, [Validators.required]],
      payment_terms:[data.payment_terms,[Validators.required]],
      delivery_terms:[data.delivery_terms, [Validators.required]],
      requirement_id:[data.requirement_id]
    });
  }

  viewTrimRequirement(data){
    this.selected_data = data;
  }

  get formControls() {
    return this.requirementForm.controls;
  }

}
