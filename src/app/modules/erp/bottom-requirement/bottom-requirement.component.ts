import { Component, OnInit, ViewChild, ElementRef, Input, HostListener } from '@angular/core';
import { ErpService } from '../../../_services/erp.service';

import { ToastrManager } from 'ng6-toastr-notifications';
import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";

@Component({
  selector: 'app-bottom-requirement',
  templateUrl: './bottom-requirement.component.html',
  styleUrls: ['./bottom-requirement.component.css']
})
export class BottomRequirementComponent implements OnInit {
  @ViewChild('closeBottomBtn', {static: false}) closeBottomBtn: ElementRef<HTMLElement>;
  @Input() erp_id: number;
  @Input() sizing_type: number;

  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    this.closeBottomBtn.nativeElement.click();
    this.submitted = false;
  }

  bottomMaterialForm:FormGroup;
  alphaNumericForm:FormGroup;
  numericForm:FormGroup;
  numericFormFields:FormGroup;

  numbers = [];
  numericSizes = [];
  bottoms=[];
  submitted = false;
  userRole = '';
  userDesignation = '';

  constructor(private api:ErpService,
    private toastr: ToastrManager, 
    private fb: FormBuilder) {
      this.numbers = new Array(100).fill(1).map((x,i)=>i+1);
     }

  ngOnInit() {
    this.userRole = localStorage.getItem('userRole');
    this.userDesignation = localStorage.getItem('userDesignation');
    this.createBottomMaterialForm();
    this.getBottomMaterialRequirements();
    this.alphaNumericForm = this.fb.group({
      xs:[""],
      s:[""],
      m:[""],
      l:[""],
      xl:[""],
      xxl:[""],
      xxxl:[""],
    });
    this.numericForm = this.fb.group({
      start:["", Validators.required],
      end:["", Validators.required],
    });
    this.numericFormFields = new FormGroup({});
  }

  addNumericForm(){
    const fromDataObj = {};
    if(this.numericForm.valid){
      if(this.numericForm.value.start > this.numericForm.value.end){
        alert('please enter correct range');
        return;
      }
      this.numericSizes = [];
      for(let i=this.numericForm.value.start; i<=this.numericForm.value.end;i++){
        fromDataObj['size'+i] = new FormControl("")
        this.numericSizes.push('size'+i);
      }
      this.numericFormFields = new FormGroup(fromDataObj);
    }
  }

  getBottomMaterialRequirements(){
    this.api.getBottomMaterialRequirements(this.erp_id).subscribe( response => {
      this.bottoms = response;
    });
  }

  addorEditBottomMaterialRequirement(){
    this.submitted = true;
    if (this.bottomMaterialForm.invalid) {
      return;
    }

    if(this.sizing_type == 1){
      this.bottomMaterialForm.addControl('sizes', this.alphaNumericForm);
    }else{
      this.bottomMaterialForm.addControl('sizes', this.numericFormFields);
      this.bottomMaterialForm.addControl('range', this.numericForm);
    }

    this.api.addorEditBottomMaterialRequirement(this.bottomMaterialForm.value).subscribe(response => {
      if (response!= 0) {
        if(this.bottomMaterialForm.value.id != ''){
          this.toastr.successToastr("Requirement updated successfully.");
        }else{
          this.toastr.successToastr("Requirement added successfully.");
        }
        this.getBottomMaterialRequirements();
        this.closeBottomBtn.nativeElement.click();

      } else{
        this.toastr.errorToastr("Error! Please try later.");
      }
    });
  }

  
  createBottomMaterialForm(){
    this.bottomMaterialForm = this.fb.group({
      color:["",[Validators.required]],
      color_code:["",[Validators.required]],
      erp_id:[this.erp_id],
      sizing_type:[this.sizing_type],
      id:[""]
    });
    this.numericSizes = [];
    this.alphaNumericForm = this.fb.group({
      xs:[""],
      s:[""],
      m:[""],
      l:[""],
      xl:[""],
      xxl:[""],
      xxxl:[""],
    });
    this.numericForm = this.fb.group({
      start:["", Validators.required],
      end:["", Validators.required],
    });
    this.numericFormFields = new FormGroup({});
  }

  editBottomMaterialRequirement(data){
    this.bottomMaterialForm = this.fb.group({
      color:[data.color,[Validators.required]],
      color_code:[data.color_code,[Validators.required]],
      erp_id:[this.erp_id],
      sizing_type:[this.sizing_type],
      id:[data.id]
    });

    if(this.sizing_type == 1){
      this.alphaNumericForm = this.fb.group({
        xs:[data.bottom_requirements.xs],
        s:[data.bottom_requirements.s],
        m:[data.bottom_requirements.m],
        l:[data.bottom_requirements.l],
        xl:[data.bottom_requirements.xl],
        xxl:[data.bottom_requirements.xxl],
        xxxl:[data.bottom_requirements.xxxl],
      });
    }
    if(this.sizing_type == 0){
      const fromDataObj = {};
      this.numericSizes = [];
      let ns = [];
      let ps = [];
      let i= 0;
      Object.keys(data.bottom_requirements).forEach(function(key) {
        var k = key;
        i++;
        console.log(i);
        if(i==1 || Object.keys(data.bottom_requirements).length == i){
          ps.push(parseInt(k.replace('size','')))
          console.log(ps);
        }
        fromDataObj[key] = new FormControl(data.bottom_requirements[key])
        ns.push(key);
      });

      this.numericForm = this.fb.group({
        start:[ps[0], Validators.required],
        end:[ps[1], Validators.required],
      }); 
      this.numericSizes = ns;
      this.numericFormFields = new FormGroup(fromDataObj);
    }
  }

  get formBottomControls() {
    return this.bottomMaterialForm.controls;
  }

}
