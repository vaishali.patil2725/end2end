import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BottomRequirementComponent } from './bottom-requirement.component';

describe('BottomRequirementComponent', () => {
  let component: BottomRequirementComponent;
  let fixture: ComponentFixture<BottomRequirementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BottomRequirementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BottomRequirementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
