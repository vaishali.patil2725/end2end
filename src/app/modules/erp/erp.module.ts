import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule , FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { SharedModule } from '../shared/shared.module';

import { ErpRoutingModule } from './erp-routing.module';
import { ErpComponent } from './erp.component';
import { ListErpComponent } from './list-erp/list-erp.component';
import { ViewErpComponent } from './view-erp/view-erp.component';
import { TopRequirementComponent } from './top-requirement/top-requirement.component';
import { BottomRequirementComponent } from './bottom-requirement/bottom-requirement.component';
import { TrimRequirementComponent } from './trim-requirement/trim-requirement.component';
import { FabricRequirementComponent } from './fabric-requirement/fabric-requirement.component';
import { DeliveryScheduleComponent } from './delivery-schedule/delivery-schedule.component';


@NgModule({
  declarations: [ErpComponent, ListErpComponent, ViewErpComponent, TopRequirementComponent, BottomRequirementComponent, TrimRequirementComponent, FabricRequirementComponent, DeliveryScheduleComponent],
  imports: [
    CommonModule,
    ErpRoutingModule,
    ReactiveFormsModule,
    SharedModule,
    NgxPaginationModule,
  ]
})
export class ErpModule { }
