import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ErpComponent } from './erp.component';
import { ListErpComponent } from './list-erp/list-erp.component';
import { ViewErpComponent } from './view-erp/view-erp.component';

const routes: Routes = [
  { path: '', 
    component: ErpComponent,
    children :[
      { path: '', component: ListErpComponent},
      { path: 'view-erp/:id', component: ViewErpComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ErpRoutingModule { }
