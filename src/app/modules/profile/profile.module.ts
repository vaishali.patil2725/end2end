import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileComponent } from './profile.component';
import { ReactiveFormsModule , FormsModule } from '@angular/forms';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { ViewProfileComponent } from './view-profile/view-profile.component';


@NgModule({
  declarations: [ProfileComponent, ChangePasswordComponent, ViewProfileComponent],
  imports: [
    CommonModule,
    ProfileRoutingModule,
	  ReactiveFormsModule,
    FormsModule
  ]
})
export class ProfileModule { }
