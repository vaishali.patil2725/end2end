import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrManager } from 'ng6-toastr-notifications';

import { AuthService } from '../../../_services/auth.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
changePassword:FormGroup
  submitted:boolean=false;
  userRole:any;
  
  constructor(public fb:FormBuilder,public api:AuthService, private toastr: ToastrManager) { }

  ngOnInit() {
	this.userRole = localStorage.getItem('userRole');
	this.changePassword=this.fb.group({
		old_password:['',Validators.required],
		password:['',Validators.required],
		cpassword:['',Validators.required],
	})
  }
  

  
   get f() { 
    return this.changePassword.controls; 
  }
  


ChangePassword()
{
	this.submitted=true;
	if(this.changePassword.invalid)
	{
		return;
	}
	
	this.api.ChangePassword(this.changePassword.value).subscribe(res=>{
		console.log(res);
		 if(res==1)
		  {
			  this.toastr.successToastr('Password updated successfully');
		  }else{
				this.toastr.errorToastr("Error! While updatins password");
		  }
	})
}

}
