import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrManager } from 'ng6-toastr-notifications';

import { AuthService } from '../../../_services/auth.service';

@Component({
	selector: 'app-view-profile',
	templateUrl: './view-profile.component.html',
	styleUrls: ['./view-profile.component.css']
})
export class ViewProfileComponent implements OnInit {
	updateProfile: FormGroup;
	submitted: boolean = false;
	userRole: any;

	constructor(public fb: FormBuilder, public api: AuthService, private toastr: ToastrManager) { }

	ngOnInit() {
		this.userRole = localStorage.getItem('userRole');
		this.getEmployeeData()

		this.updateProfile = this.fb.group({
			username: ['', Validators.required],
			full_name: ['', Validators.required],
			email: ['', Validators.required],
			phone: ['', Validators.required],
			address: ['', Validators.required],
			city: ['', Validators.required],
			country: ['', Validators.required],
			zip: ['', Validators.required]
		})

	}

	getEmployeeData() {
		this.api.getEmployeeData().subscribe(res => {
			console.log(res);
			this.updateProfile.patchValue(res)
		})
	}

	get f() {
		return this.updateProfile.controls;
	}

	UpdateProfile() {
		this.submitted = true;
		if (this.updateProfile.invalid) {
			return;
		}
		this.api.updateProfile(this.updateProfile.value).subscribe(res => {
			console.log(res);
			if (res == 1) {
				this.toastr.successToastr('Updated successfully');
			} else {
				this.toastr.errorToastr("Error! While updatins Data");
			}
		})

	}

}
