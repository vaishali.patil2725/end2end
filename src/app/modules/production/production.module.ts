import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { ReactiveFormsModule , FormsModule } from '@angular/forms';

import { ProductionRoutingModule } from './production-routing.module';
import { ProductionComponent } from './production.component';
import { OutwardEntriesComponent } from './outward-entries/outward-entries.component';
import { SewingDataComponent } from './sewing-data/sewing-data.component';
import { FinishingDataComponent } from './finishing-data/finishing-data.component';
import { FusingDataComponent } from './fusing-data/fusing-data.component';
import { KajaButtoningDataComponent } from './kaja-buttoning-data/kaja-buttoning-data.component';
import { CuttingDataComponent } from './cutting-data/cutting-data.component';
import { FabricInspectionLogComponent } from './fabric-inspection-log/fabric-inspection-log.component';

import { AddTrimInwardsComponent } from './trim-inwards/add-trim-inwards/add-trim-inwards.component';
import { ViewTrimInwardDetailsComponent } from './trim-inwards/view-trim-inward-details/view-trim-inward-details.component';
import { ListTrimInwardsComponent } from './trim-inwards/list-trim-inwards/list-trim-inwards.component';

import { AddFabricInwardsComponent } from './fabric-inwards/add-fabric-inwards/add-fabric-inwards.component';
import { ListFabricInwardsComponent } from './fabric-inwards/list-fabric-inwards/list-fabric-inwards.component';
import { ViewFabricInwardDetailsComponent } from './fabric-inwards/view-fabric-inward-details/view-fabric-inward-details.component';
import { ProductionNavbarComponent } from './production-navbar/production-navbar.component';
import { DispatchDataComponent } from './dispatch-data/dispatch-data.component';

@NgModule({
  declarations: [ProductionComponent, OutwardEntriesComponent, SewingDataComponent, FinishingDataComponent, FusingDataComponent, KajaButtoningDataComponent, CuttingDataComponent, FabricInspectionLogComponent, AddTrimInwardsComponent, ViewTrimInwardDetailsComponent, ListTrimInwardsComponent, AddFabricInwardsComponent, ListFabricInwardsComponent, ViewFabricInwardDetailsComponent, ProductionNavbarComponent, DispatchDataComponent],
  imports: [
    CommonModule,
    ProductionRoutingModule,
    SharedModule,
    NgxPaginationModule,
    ReactiveFormsModule,
    FormsModule
  ],
  exports:[ProductionNavbarComponent, CuttingDataComponent, SewingDataComponent, FinishingDataComponent, FusingDataComponent, KajaButtoningDataComponent]
})
export class ProductionModule { }
