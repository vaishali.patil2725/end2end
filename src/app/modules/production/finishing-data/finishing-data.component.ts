import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProductionService } from '../../../_services/production.service';
import { UnitlinesService } from '../../../_services/unitlines.service';
import { ErpService } from '../../../_services/erp.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-finishing-data',
  templateUrl: './finishing-data.component.html',
  styleUrls: ['./finishing-data.component.css']
})
export class FinishingDataComponent implements OnInit {
  @ViewChild('closeBtn', {static: false}) closeBtn: ElementRef<HTMLElement>;
  
  finishingDataForm: FormGroup;
  filterForm:FormGroup;

  erps: [];
  lines:[];
  production_data:[];
  userDesignation:string;
  submitted = false;
  selected_data:{};

  constructor(private fb: FormBuilder,
    private api: ProductionService,
    private lineService:UnitlinesService,
    private erp: ErpService,
    private toastr: ToastrManager) { }

  ngOnInit() {    
    this.filterForm = this.fb.group({
      f_erp:[""],
      f_date:[""],
      f_line:[""],
      table: 'finishing_production_data'
    });

    this.createFinishingDataForm();
    this.getErps();
    this.getProductionData();
    this.getLines();
  }

  getLines(){
    this.lineService.getUnitLines({'department':7}).subscribe( response => {
      this.lines = response;
    });
  }
  getErps(){
    this.erp.getErpList().subscribe( response => {
      this.erps = response;
    });
  }

  getProductionData(){
    this.api.getProductionData(this.filterForm.value).subscribe( response => {
      this.production_data = response;
    });
  }

  addorEditFininshingData() {
    this.submitted = true;
    console.log(this.finishingDataForm);
    if (this.finishingDataForm.invalid) {
      return;
    }
    this.api.addorEditProductionData(this.finishingDataForm.value).subscribe( response => {
      if (response!= 0) {
        this.getProductionData();
        if(this.finishingDataForm.value.id!=''){
          this.toastr.successToastr("Data updated successfully.");
        }else{
          this.toastr.successToastr("Data added successfully.");
        }
        this.closeBtn.nativeElement.click();
      } else{
        this.toastr.errorToastr("Error! While adding Data");
      }
    });
  }

  createFinishingDataForm() {
    this.submitted = false;
    this.finishingDataForm = this.fb.group({
      erp: ['', Validators.required],
      section: ['', Validators.required],
      line: ['', Validators.required],
      production_hour: ['', Validators.required],
      output: ['', Validators.required],
      helpers: ['', Validators.required],
      compound_movement: ['', Validators.required],
      ironers: ['', Validators.required],
      checkers: ['', Validators.required],
      id:[''],
      table:['finishing_production_data']
    });
  }

  editFinishingData(data){
    this.submitted = false;
    this.finishingDataForm = this.fb.group({
      erp: [data.erp, Validators.required],
      section: [data.section, Validators.required],
      line: [data.line, Validators.required],
      production_hour: [data.production_hour, Validators.required],
      output: [data.output, Validators.required],
      helpers: [data.helpers, Validators.required],
      compound_movement: [data.compound_movement, Validators.required],
      ironers: [data.ironers, Validators.required],
      checkers: [data.checkers, Validators.required],
      id:[data.id],
      table:['finishing_production_data']
    });
  }

  get formcontrol() { 
    return this.finishingDataForm.controls; 
  }

  viewProductionData(data){
    this.selected_data = data;
  }

  resetFilters(){
    this.filterForm.reset();
    this.filterForm.patchValue({table:"finishing_production_data"})
    this.getProductionData();
  }

  generateExcel() {
    this.api.downloadReports('Finishing', this.production_data);
  }
}
