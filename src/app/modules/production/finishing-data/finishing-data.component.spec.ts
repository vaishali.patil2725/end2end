import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinishingDataComponent } from './finishing-data.component';

describe('FinishingDataComponent', () => {
  let component: FinishingDataComponent;
  let fixture: ComponentFixture<FinishingDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinishingDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinishingDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
