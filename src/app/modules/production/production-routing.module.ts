import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductionComponent } from './production.component';
import { OutwardEntriesComponent } from './outward-entries/outward-entries.component';
import { FabricInspectionLogComponent } from './fabric-inspection-log/fabric-inspection-log.component';

import { SewingDataComponent } from './sewing-data/sewing-data.component';
import { FinishingDataComponent } from './finishing-data/finishing-data.component';
import { FusingDataComponent } from './fusing-data/fusing-data.component';
import { KajaButtoningDataComponent } from './kaja-buttoning-data/kaja-buttoning-data.component';
import { CuttingDataComponent } from './cutting-data/cutting-data.component';

import { AddTrimInwardsComponent } from './trim-inwards/add-trim-inwards/add-trim-inwards.component';
import { ViewTrimInwardDetailsComponent } from './trim-inwards/view-trim-inward-details/view-trim-inward-details.component';
import { ListTrimInwardsComponent } from './trim-inwards/list-trim-inwards/list-trim-inwards.component';

import { AddFabricInwardsComponent } from './fabric-inwards/add-fabric-inwards/add-fabric-inwards.component';
import { ListFabricInwardsComponent } from './fabric-inwards/list-fabric-inwards/list-fabric-inwards.component';
import { ViewFabricInwardDetailsComponent } from './fabric-inwards/view-fabric-inward-details/view-fabric-inward-details.component';
import { DispatchDataComponent } from './dispatch-data/dispatch-data.component';

const routes: Routes = [
  { path: '',
      component:ProductionComponent,
      children :[
          { path: '', component: ProductionComponent},
          { path :'outward-logs', component:OutwardEntriesComponent},
          { path: 'cutting-data', component:CuttingDataComponent},
          { path: 'sewing-data', component:SewingDataComponent},
          { path: 'finishing-data', component:FinishingDataComponent},
          { path: 'fusing-data', component:FusingDataComponent},
          { path: 'kaja-buttoning-data', component:KajaButtoningDataComponent},
          { path: 'fabric-inspection', component:FabricInspectionLogComponent},
          { path: 'dispatch-details', component:DispatchDataComponent},

          { path: 'trim-inwards', component: ListTrimInwardsComponent},
          { path: 'add-inward-log', component:AddTrimInwardsComponent},
          { path: 'edit-inward-log/:id', component:AddTrimInwardsComponent},
          { path: 'view-inward-details/:id', component:ViewTrimInwardDetailsComponent},

          { path: 'fabric-inwards', component: ListFabricInwardsComponent},
          { path: 'add-fabric-inward-log', component:AddFabricInwardsComponent},
          { path: 'edit-fabric-inward-log/:id', component:AddFabricInwardsComponent},
          { path: 'view-fabric-inward-details/:id', component:ViewFabricInwardDetailsComponent},

      ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductionRoutingModule { }
