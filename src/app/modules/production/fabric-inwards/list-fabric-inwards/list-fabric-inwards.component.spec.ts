import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListFabricInwardsComponent } from './list-fabric-inwards.component';

describe('ListFabricInwardsComponent', () => {
  let component: ListFabricInwardsComponent;
  let fixture: ComponentFixture<ListFabricInwardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListFabricInwardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListFabricInwardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
