import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ProductionService } from '../../../../_services/production.service';
import { ErpService } from '../../../../_services/erp.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ExcelService } from '../../../../_services/excel.service';

@Component({
  selector: 'app-list-fabric-inwards',
  templateUrl: './list-fabric-inwards.component.html',
  styleUrls: ['./list-fabric-inwards.component.css']
})
export class ListFabricInwardsComponent implements OnInit {

  filterForm:FormGroup;
  material:[];
  erps:[];
  
  constructor(private api: ProductionService,
    private erp:ErpService,
    private excelService:ExcelService,
    private fb:FormBuilder) { }

  ngOnInit() {
    this.filterForm = this.fb.group({
      f_erp:[""],
      f_date:[""]
    });

    this.getFilteredFabricInwards();
    this.getErps();
  }

  getFilteredFabricInwards(){
    this.api.getFilteredFabricInwards(this.filterForm.value).subscribe( response => {
      this.material = response;
    });
  }

  getErps(){
    this.erp.getErpList().subscribe( response => {
      this.erps = response;
    });
  }
  
  resetFilters(){
    this.filterForm.reset();
    this.getFilteredFabricInwards();
  }
  
  generateExcel() {
    let title = 'Fabric Inward Report';
    let header = ['ERP', 'Fabric Code', 'Number of Rolls', 'Quantity', 'GRN Number', 'Barcode', 'Added By', 'Added On'];
    let data = this.material
    var result = [];
    Object.keys(data).forEach(function(key) {
      let data_row = [];
      data_row.push(data[key]["erp_title"]);
      data_row.push(data[key]["fabric_code"]);
      data_row.push(data[key]["rolls"]);
      data_row.push(data[key]["received_quantity"]);
      data_row.push(data[key]["grn_no"]);
      data_row.push(data[key]["barcode"]);
      data_row.push(data[key]['added_by']);
      data_row.push(data[key]['created_on']);

      result.push(data_row);
    });
    this.excelService.generateExcel(title, header, result);
  }

}
