import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddFabricInwardsComponent } from './add-fabric-inwards.component';

describe('AddFabricInwardsComponent', () => {
  let component: AddFabricInwardsComponent;
  let fixture: ComponentFixture<AddFabricInwardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddFabricInwardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddFabricInwardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
