import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProductionService } from '../../../../_services/production.service';
import { ErpService } from '../../../../_services/erp.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-add-fabric-inwards',
  templateUrl: './add-fabric-inwards.component.html',
  styleUrls: ['./add-fabric-inwards.component.css']
})
export class AddFabricInwardsComponent implements OnInit {

  InwardEntriesForm: FormGroup;

  erps: [];
  itemcodes: [];
  submitted = false;
  inward_id = '';
  action = 'Add';

  constructor(private fb: FormBuilder,
    private api: ProductionService,
    private erp: ErpService,
    private toastr: ToastrManager,
    private router:Router,
    private route: ActivatedRoute) { }

  ngOnInit() {   
    this.inward_id = this.route.snapshot.paramMap.get("id");
    this.submitted = false;
    this.InwardEntriesForm = this.fb.group({
      erp: ['', Validators.required],
      fabric_code: ['', Validators.required],
      rolls: ['', Validators.required],
      received_quantity: ['', Validators.required],
      barcode: ['', Validators.required],
      grn_no: ['', Validators.required],
      inward_id: [],
      table:['fabric_inward_log']
    });

    if(this.inward_id != '' && this.inward_id != null){
      this.action = 'Update';
      this.api.getFabricInwardDetails({'inward_id':this.inward_id}).subscribe( response => {
        this.InwardEntriesForm = this.fb.group({
          erp: [response.erp, Validators.required],
          fabric_code: [response.fabric_code, Validators.required],
          rolls: [response.rolls, Validators.required],
          received_quantity: [response.received_quantity, Validators.required],
          barcode: [response.barcode, Validators.required],
          grn_no: [response.grn_no, Validators.required],
          inward_id: [response.inward_id],
          table:['fabric_inward_log']
        });
        this.getItemCodes();
      });
    }
    this.getErps();
  }
  
  getErps(){
    this.erp.getErpList().subscribe( response => {
      this.erps = response;
    });
  }

  getItemCodes(){
    this.api.getItemCodes({'requirement_type':'Fabric', 'erp_id':this.InwardEntriesForm.value.erp}).subscribe( response => {
      this.itemcodes = response;
    });
  }

  addorUpdateInwardEntry() {
    this.submitted = true;
    if (this.InwardEntriesForm.invalid) {
      return;
    }
    this.api.addorUpdateInwardEntry(this.InwardEntriesForm.value).subscribe( response => {
      if (response!= 0) {
        if(this.InwardEntriesForm.value.inward_id != ''){
          this.toastr.successToastr("Data updated successfully.");
        }else{
          this.toastr.successToastr("Data added successfully.");
        }
        this.router.navigate(['/production/fabric-inwards']);
      } else{
        this.toastr.errorToastr("Error! While adding Data");
      }
    });
  }

  get formcontrol() { 
    return this.InwardEntriesForm.controls; 
  }

}
