import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProductionService } from '../../../../_services/production.service';
import { SharedService } from '../../../../_services/shared.service';
import { UnitlinesService } from '../../../../_services/unitlines.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-view-fabric-inward-details',
  templateUrl: './view-fabric-inward-details.component.html',
  styleUrls: ['./view-fabric-inward-details.component.css']
})
export class ViewFabricInwardDetailsComponent implements OnInit {

  @ViewChild('closeBtn', {static: false}) closeBtn: ElementRef<HTMLElement>;

  OutwardEntriesForm: FormGroup;
  departments: [];
  lines: [];
  outwards:[];
  fabric_data:{};
  inward_id:any;
  submitted=false;
  stock_quantity:number;
  stock_quantity_error = false;

  constructor(private fb: FormBuilder,
    private api: ProductionService,
    private sharedService:SharedService,
    private lineService:UnitlinesService,
    private toastr: ToastrManager,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.inward_id = this.route.snapshot.paramMap.get("id");
    this.api.getFabricInwardDetails({'inward_id':this.inward_id}).subscribe( response => {
      this.fabric_data = response;
      this.stock_quantity = response.received_quantity;
      this.getFabricOutwardLogData();  
    }); 
    this.CreateOutwardEntriesForm();
    this.getDepartments();
  }

  getFabricOutwardLogData(){
    this.api.getFabricOutwardLogData({'inward_id':this.inward_id}).subscribe( response => {
      this.outwards = response;
      if(response.length > 0){
        response.forEach(res => {
          this.stock_quantity = this.stock_quantity - res.quantity;
        });
      }
    });
  }

  getLines(department){
    this.lineService.getUnitLines({'department':department}).subscribe( response => {
      this.lines = response;
    });
  }

  getDepartments(){
    this.sharedService.getDepartments().subscribe( response => {
      this.departments = response;
    });
  }

  addorEditOutwardEntry(){
    this.submitted = true;
    if (this.OutwardEntriesForm.invalid) {
      return;
    }

    if(this.OutwardEntriesForm.value.outward_id == '' && this.OutwardEntriesForm.value.quantity > this.stock_quantity){
      this.stock_quantity_error = true;
      return;
    }
    this.api.addorEditOutwardEntry(this.OutwardEntriesForm.value).subscribe( response => {
      if (response!= 0) {
        if(this.OutwardEntriesForm.value.outward_id!=''){
          this.toastr.successToastr("Data updated successfully.");
        }else{
          this.toastr.successToastr("Data added successfully.");
        }
        this.getFabricOutwardLogData();
        this.closeBtn.nativeElement.click();
      } else{
        this.toastr.errorToastr("Error! While adding Data");
      }
    });
  }

  CreateOutwardEntriesForm() {
    this.submitted = false;
    this.OutwardEntriesForm = this.fb.group({
      department: ['', Validators.required],
      line: ['', Validators.required],
      quantity: ['', Validators.required],
      outward_id: [''],
      inward_id: [this.inward_id],
      table:['fabric_outward_log']
    });
  }
  
  editOutwardEntries(data){
    this.lineService.getUnitLines({'department':data.department}).subscribe( response => {
      this.lines = response;
    });
    this.submitted = false;
    this.OutwardEntriesForm = this.fb.group({
      department: [data.department, Validators.required],
      line: [data.line, Validators.required],
      quantity: [data.quantity, Validators.required],
      outward_id: [data.outward_id],
      inward_id: [this.inward_id],
      table:['fabric_outward_log']
    });
  }

  get formcontrol() { return this.OutwardEntriesForm.controls; }


}
