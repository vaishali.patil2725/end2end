import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewFabricInwardDetailsComponent } from './view-fabric-inward-details.component';

describe('ViewFabricInwardDetailsComponent', () => {
  let component: ViewFabricInwardDetailsComponent;
  let fixture: ComponentFixture<ViewFabricInwardDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewFabricInwardDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewFabricInwardDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
