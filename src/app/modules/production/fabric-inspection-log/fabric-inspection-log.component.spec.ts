import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FabricInspectionLogComponent } from './fabric-inspection-log.component';

describe('FabricInspectionLogComponent', () => {
  let component: FabricInspectionLogComponent;
  let fixture: ComponentFixture<FabricInspectionLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FabricInspectionLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FabricInspectionLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
