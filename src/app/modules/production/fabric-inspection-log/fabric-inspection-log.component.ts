import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProductionService } from '../../../_services/production.service';
import { ErpService } from '../../../_services/erp.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import { ExcelService } from '../../../_services/excel.service';

@Component({
  selector: 'app-fabric-inspection-log',
  templateUrl: './fabric-inspection-log.component.html',
  styleUrls: ['./fabric-inspection-log.component.css']
})
export class FabricInspectionLogComponent implements OnInit {

  @ViewChild('closeBtn', {static: false}) closeBtn: ElementRef<HTMLElement>;

  fabricInspectionForm: FormGroup;
  filterForm:FormGroup;

  erps: [];
  itemcodes: [];
  logs:[];
  submitted = false;
  userDesignation:string;
  department:string;

  constructor(private fb: FormBuilder,
    private api: ProductionService,
    private erp: ErpService,
    private excelService:ExcelService,
    private toastr: ToastrManager) { }

  ngOnInit() {
    this.userDesignation = localStorage.getItem('userDesignation');
    if(this.userDesignation == 'Trim Head'){
      this.department = 'Trim';
    }else{
      this.department = 'Fabric';
    }

    this.filterForm = this.fb.group({
      f_erp:[""],
      f_date:[""]
    });

    this.CreateFabricInspectionForm();
    this.getErps();
    this.getFabricInspectionLogs();
  }

  getErps(){
    this.erp.getErpList().subscribe( response => {
      this.erps = response;
    });
  }

  getFabricInspectionLogs(){
    this.api.getFabricInspectionLogs(this.filterForm.value).subscribe( response => {
      this.logs = response;
    });
  }

  addorEditFabricInspectionEntry() {
    this.submitted = true;
    if (this.fabricInspectionForm.invalid) {
      return;
    }
    this.api.addorEditFabricInspectionEntry(this.fabricInspectionForm.value).subscribe( response => {
      if (response!= 0) {
        this.getFabricInspectionLogs();
        if(this.fabricInspectionForm.value.inspection_id!=''){
          this.toastr.successToastr("Data updated successfully.");
        }else{
          this.toastr.successToastr("Data added successfully.");
        }
        this.closeBtn.nativeElement.click();
      } else{
        this.toastr.errorToastr("Error! While adding Data");
      }
    });
  }

  CreateFabricInspectionForm() {
    this.submitted = false;
    this.fabricInspectionForm = this.fb.group({
      erp: ['', Validators.required],
      roll_no: ['', Validators.required],
      color: ['', Validators.required],
      quantity: ['', Validators.required],
      rejections: ['', Validators.required],
      barcode: ['', Validators.required],
      inspection_id:['']
    });
  }

  editFabricInspection(data){
    this.submitted = false;
    this.fabricInspectionForm = this.fb.group({   
      erp: [data.erp, Validators.required],
      roll_no: [data.roll_no, Validators.required],
      color: [data.color, Validators.required],
      quantity: [data.quantity, Validators.required],
      rejections: [data.rejections, Validators.required],
      barcode: [data.barcode, Validators.required],
      inspection_id:[data.inspection_id]
    });
  }

  get formcontrol() { 
    return this.fabricInspectionForm.controls; 
  }

  resetFilters(){
    this.filterForm.reset();
    this.getFabricInspectionLogs();
  }

  generateExcel() {
    let title = 'Fabric Inspection Log Report';
    let header = ['ERP', 'Color', 'Roll Number', 'Total Quantity', 'Rejected Quantity', 'Barcode', 'Added By', 'Added On'];
    let data = this.logs
    var result = [];
    Object.keys(data).forEach(function(key) {
      let data_row = [];
      data_row.push(data[key]["erp_title"]);
      data_row.push(data[key]["color"]);
      data_row.push(data[key]["roll_no"]);
      data_row.push(data[key]["quantity"]);
      data_row.push(data[key]["rejections"]);
      data_row.push(data[key]["barcode"]);
      data_row.push(data[key]['added_by']);
      data_row.push(data[key]['created_on']);

      result.push(data_row);
    });
    this.excelService.generateExcel(title, header, result);
  }

}
