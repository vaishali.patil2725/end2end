import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ProductionService } from '../../../_services/production.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ErpService } from '../../../_services/erp.service';
import { ExcelService } from '../../../_services/excel.service';

@Component({
  selector: 'app-outward-entries',
  templateUrl: './outward-entries.component.html',
  styleUrls: ['./outward-entries.component.css']
})
export class OutwardEntriesComponent implements OnInit {

  filterForm:FormGroup;

  outwards:[];
  erps: [];

  constructor(private api:ProductionService,
    private excelService:ExcelService,
    private erp: ErpService,
    private fb: FormBuilder) { }

  ngOnInit() { 
    this.filterForm = this.fb.group({
      f_erp:[""],
      f_date:[""]
    });

    this.getOutwardLogData();
    this.getErps();
  }

  getErps(){
    this.erp.getErpList().subscribe( response => {
      this.erps = response;
    });
  }

  getOutwardLogData(){   
    this.api.getOutwardLogData().subscribe( response => {
      this.outwards = response;
    });
  }

  resetFilters(){
    this.filterForm.reset();
    this.getOutwardLogData();
  }

  generateExcel() {
    let title = 'Outward Report';
    let header = ['ERP', 'Fabric Code', 'Received Quantity', 'Outward Quantity', 'GRN Number','Barcode', 'Department', 'Line', 'DateTime', 'Added By'];
    let data = this.outwards
    var result = [];
    Object.keys(data).forEach(function(key) {
      let data_row = [];
      data_row.push(data[key]["erp_title"]);
      data_row.push(data[key]["outward_quantity"]);
      data_row.push(data[key]["outward_quantity"]);
      data_row.push(data[key]["grn_no"]);
      data_row.push(data[key]["barcode"]);
      data_row.push(data[key]["dept_name"]);
      data_row.push(data[key]["line_name"]);
      data_row.push(data[key]['created_on']);
      data_row.push(data[key]['added_by']);

      result.push(data_row);
    });
    this.excelService.generateExcel(title, header, result);
  }
}
