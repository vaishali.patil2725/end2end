import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutwardEntriesComponent } from './outward-entries.component';

describe('OutwardEntriesComponent', () => {
  let component: OutwardEntriesComponent;
  let fixture: ComponentFixture<OutwardEntriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutwardEntriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutwardEntriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
