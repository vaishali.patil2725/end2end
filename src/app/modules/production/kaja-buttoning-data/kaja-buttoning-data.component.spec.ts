import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KajaButtoningDataComponent } from './kaja-buttoning-data.component';

describe('KajaButtoningDataComponent', () => {
  let component: KajaButtoningDataComponent;
  let fixture: ComponentFixture<KajaButtoningDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KajaButtoningDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KajaButtoningDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
