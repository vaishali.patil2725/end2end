import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CuttingDataComponent } from './cutting-data.component';

describe('CuttingDataComponent', () => {
  let component: CuttingDataComponent;
  let fixture: ComponentFixture<CuttingDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CuttingDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CuttingDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
