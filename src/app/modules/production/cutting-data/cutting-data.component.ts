import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UnitlinesService } from '../../../_services/unitlines.service';
import { ProductionService } from '../../../_services/production.service';
import { ErpService } from '../../../_services/erp.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-cutting-data',
  templateUrl: './cutting-data.component.html',
  styleUrls: ['./cutting-data.component.css']
})
export class CuttingDataComponent implements OnInit {

  @ViewChild('closeBtn', {static: false}) closeBtn: ElementRef<HTMLElement>;

  filterForm:FormGroup;

  cutters = true;
  layerers = false;
  userDesignation:string;
  cuttingDataForm: FormGroup;
  erps:[];
  lines:[];
  production_data:[];
  submitted = false;
  selected_data:{};

  constructor(private fb: FormBuilder,
    private api: ProductionService,
    private lineService:UnitlinesService,
    private erp: ErpService,
    private toastr: ToastrManager
    ) { }

  ngOnInit() {   
    this.filterForm = this.fb.group({
      f_erp:[""],
      f_date:[""],
      f_section:[""],
      f_line:[""],
      table: 'cutting_production_data'
    });

    this.createCuttingDataForm();
    this.getErps();
    this.getProductionData();
    this.getLines();
  }

  getErps(){
    this.erp.getErpList().subscribe( response => {
      this.erps = response;
    });
  }

  getLines(){
    this.lineService.getUnitLines({'department':5}).subscribe( response => {
      this.lines = response;
    });
  }

  getProductionData(){
    this.api.getProductionData(this.filterForm.value).subscribe( response => {
      this.production_data = response;
    });
  }

  onSectionChange(value){
    if(value == 'cutting'){
      this.cutters = true;
      this.layerers = false;
    }else{
      this.layerers = true;
      this.cutters = false;
    }
  }

  createCuttingDataForm() {
    this.submitted = false;
    this.cuttingDataForm = this.fb.group({
      erp: ['', Validators.required],
      section: ['', Validators.required],
      line: ['', Validators.required],
      production_hour: ['', Validators.required],
      output: ['', Validators.required],
      machineries: ['', Validators.required],
      helpers: ['', Validators.required],
      cutters: [''],
      layerers: [''],
      id:[''],
      table:['cutting_production_data']
    });
  }

  editCuttingData(data){
    this.submitted = false;
    this.cuttingDataForm = this.fb.group({
      erp: [data.erp, Validators.required],
      section: [data.section, Validators.required],
      line: [data.line, Validators.required],
      production_hour: [data.production_hour, Validators.required],
      output: [data.output, Validators.required],
      machineries: [data.machineries, Validators.required],
      helpers: [data.helpers, Validators.required],
      cutters: [data.cutters],
      layerers: [data.layerers],
      id:[data.id],
      table:['cutting_production_data']
    });
  }

  get formcontrol() { 
    return this.cuttingDataForm.controls; 
  }

  addorEditCuttingData() {
    this.submitted = true;
    if (this.cuttingDataForm.invalid) {
      return;
    }
    this.api.addorEditProductionData(this.cuttingDataForm.value).subscribe( response => {
      if (response!= 0) {
        this.getProductionData();
        if(this.cuttingDataForm.value.id!=''){
          this.toastr.successToastr("Data updated successfully.");
        }else{
          this.toastr.successToastr("Data added successfully.");
        }
        this.closeBtn.nativeElement.click();
      } else{
        this.toastr.errorToastr("Error! While adding Data");
      }
    });
  }

  viewProductionData(data){
    this.selected_data = data;
  }

  resetFilters(){
    this.filterForm.reset();
    this.filterForm.patchValue({table:"cutting_production_data"})
    this.getProductionData();
  }

  generateExcel() {
    this.api.downloadReports('Cutting', this.production_data);
  }

}
