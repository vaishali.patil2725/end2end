import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DispatchDataComponent } from './dispatch-data.component';

describe('DispatchDataComponent', () => {
  let component: DispatchDataComponent;
  let fixture: ComponentFixture<DispatchDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DispatchDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DispatchDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
