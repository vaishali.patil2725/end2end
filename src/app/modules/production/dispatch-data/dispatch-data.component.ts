import { Component, OnInit, ViewChild, ElementRef, Input, HostListener } from '@angular/core';
import { PurchaseOrderService } from '../../../_services/purchase-order.service';
import { ErpService } from '../../../_services/erp.service';

import { ToastrManager } from 'ng6-toastr-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from "@angular/forms";
declare var $;

@Component({
  selector: 'app-dispatch-data',
  templateUrl: './dispatch-data.component.html',
  styleUrls: ['./dispatch-data.component.css']
})
export class DispatchDataComponent implements OnInit {

  @ViewChild('closeBtn', {static: false}) closeBtn: ElementRef<HTMLElement>;
  @Input() orderId: number;
  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    this.closeBtn.nativeElement.click();
    this.submitted = false;
  }

  dispatchLotForm:FormGroup;
  cartonContentForm:FormGroup;
  lots:[];
  erps:[];
  cartons:[];
  submitted = false;
  selectedLot={};
  userRole = '';

  constructor(private api: PurchaseOrderService, 
      private erpService:ErpService,
      private toastr: ToastrManager, 
      private fb: FormBuilder) { }

  ngOnInit() {
    this.userRole = localStorage.getItem('userRole');
    this.createDispatchLotForm();
    this.getDispatchLots();
    this.getErpList();
    this.cartonContentForm = this.fb.group({
      dispatch_no: [""],
      carton_number: ["", [Validators.required]],
      size: ["", [Validators.required]],
      quantity: ["", [Validators.required]],
      carton_id:[""]
    });
  }

  getDispatchLots(){
    this.api.getDispatchLots().subscribe( response => {
      this.lots = response;
    });
  }

  getErpList(){
    this.erpService.getErpList().subscribe( response => {
      console.log(response);
      this.erps = response;
    });
  }
  
  createDispatchLotForm(){
    this.submitted = false;
    this.dispatchLotForm = this.fb.group({
      erp: ["", [Validators.required]],
      dispatch_lot: ["", [Validators.required]],
      total_cartons: ["", [Validators.required]],
      quantity: ["", [Validators.required]],
      helpers:["",[Validators.required]],
      address:["",[Validators.required]],
      country:["",[Validators.required]],
      zip:["",[Validators.required]],
      dispatch_id:[""]
    });
  }

  editDispatchLot(data)
  {
    this.submitted = false;
    this.dispatchLotForm = this.fb.group({
      erp: [data.erp, [Validators.required]],
      dispatch_lot: [data.dispatch_lot, [Validators.required]],
      total_cartons: [data.total_cartons, [Validators.required]],
      quantity: [data.quantity, [Validators.required]],
      helpers:[data.helpers,[Validators.required]],
      address:[data.address,[Validators.required]],
      country:[data.country,[Validators.required]],
      zip:[data.zip,[Validators.required]],
      dispatch_id:[""]
    });
  }

  viewCartonContent(lot){
    this.selectedLot = lot;
    this.cartonContentForm = this.fb.group({
      dispatch_no: [lot.dispatch_id],
      carton_number: ["", [Validators.required]],
      size: ["", [Validators.required]],
      quantity: ["", [Validators.required]],
      carton_id:[""]
    });
    this.api.getCartonContent({'dispatch_id': lot.dispatch_id}).subscribe( response => {
      this.cartons = response;
    });
  }

  addorEditDispatchLot(){
    console.log(this.dispatchLotForm)
    this.submitted = true;
    if (this.dispatchLotForm.invalid) {
      return;
    }
    this.api.addorEditDispatchLot(this.dispatchLotForm.value).subscribe(response => {
      if (response!= 0) {
        if(this.dispatchLotForm.value.dispatch_id != ''){
          this.toastr.successToastr("Dispatch Lot updated successfully.");
        }else{
          this.toastr.successToastr("Dispatch Lot added successfully.");
        }
        this.closeBtn.nativeElement.click();
        this.getDispatchLots();
      } else{
        this.toastr.errorToastr("Error! Please try later.");
      }
    });
  }

  addorEditCartonContent(){
    this.submitted = true;
    if (this.cartonContentForm.invalid) {
      return;
    }
    this.api.addorEditCartonContent(this.cartonContentForm.value).subscribe(response => {
      if (response!= 0) {
        if(this.dispatchLotForm.value.dispatch_id != ''){
          this.toastr.successToastr("Carton Content updated successfully.");
        }else{
          this.toastr.successToastr("Carton Content added successfully.");
        }
        this.api.getCartonContent({'dispatch_id': this.cartonContentForm.value.dispatch_no}).subscribe( response => {
          this.cartons = response;
        });
      } else{
        this.toastr.errorToastr("Error! Please try later.");
      }
    });
  }

  get f() {
    return this.dispatchLotForm.controls;
  }

  get f1() {
    return this.cartonContentForm.controls;
  }

}
