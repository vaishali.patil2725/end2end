import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ProductionService } from '../../../../_services/production.service';
import { ExcelService } from '../../../../_services/excel.service';
import { ErpService } from '../../../../_services/erp.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-list-trim-inwards',
  templateUrl: './list-trim-inwards.component.html',
  styleUrls: ['./list-trim-inwards.component.css']
})
export class ListTrimInwardsComponent implements OnInit {

  filterForm:FormGroup;

  material:[];
  erps:[];

  constructor(private api: ProductionService,
    private excelService:ExcelService,
    private fb: FormBuilder,
    private erp: ErpService) { }

  ngOnInit() {
    this.filterForm = this.fb.group({
      f_erp:[""],
      f_date:[""]
    });

    this.getTrimInwardEntries();
    this.getErps();
  }

  getErps(){
    this.erp.getErpList().subscribe( response => {
      this.erps = response;
    });
  }

  getTrimInwardEntries(){
    this.api.getTrimInwardEntries(this.filterForm.value).subscribe( response => {
      this.material = response;
    });
  }

  
  resetFilters(){
    this.filterForm.reset();
    this.getTrimInwardEntries();
  }

  generateExcel() {
    let title = 'Trim Inward Report';
    let header = ['ERP', 'Trim Code', 'Unit', 'Received Quantity', 'Rejected Quantity', 'Barcode', 'GRN Number', 'DateTime', 'Added By'];
    let data = this.material
    var result = [];
    Object.keys(data).forEach(function(key) {
      let data_row = [];
      data_row.push(data[key]["erp_title"]);
      data_row.push(data[key]["trim_code"]);
      data_row.push(data[key]["received_quantity"]);
      data_row.push(data[key]["rejected_quantity"]);
      data_row.push(data[key]["grn_no"]);
      data_row.push(data[key]["barcode"]);
      data_row.push(data[key]["grn_no"]);
      data_row.push(data[key]['created_on']);
      data_row.push(data[key]['added_by']);

      result.push(data_row);
    });
    this.excelService.generateExcel(title, header, result);
  }
}
