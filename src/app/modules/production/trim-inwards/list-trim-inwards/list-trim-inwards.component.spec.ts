import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListTrimInwardsComponent } from './list-trim-inwards.component';

describe('ListTrimInwardsComponent', () => {
  let component: ListTrimInwardsComponent;
  let fixture: ComponentFixture<ListTrimInwardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListTrimInwardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListTrimInwardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
