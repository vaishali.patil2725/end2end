import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTrimInwardsComponent } from './add-trim-inwards.component';

describe('AddTrimInwardsComponent', () => {
  let component: AddTrimInwardsComponent;
  let fixture: ComponentFixture<AddTrimInwardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddTrimInwardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTrimInwardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
