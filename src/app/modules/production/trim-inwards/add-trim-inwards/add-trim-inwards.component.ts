import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProductionService } from '../../../../_services/production.service';
import { ErpService } from '../../../../_services/erp.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-add-trim-inwards',
  templateUrl: './add-trim-inwards.component.html',
  styleUrls: ['./add-trim-inwards.component.css']
})
export class AddTrimInwardsComponent implements OnInit {

  InwardEntriesForm: FormGroup;

  erps: [];
  itemcodes: [];
  submitted = false;
  inward_id = '';
  action = 'Add';

  constructor(private fb: FormBuilder,
    private api: ProductionService,
    private erp: ErpService,
    private toastr: ToastrManager,
    private router:Router,
    private route: ActivatedRoute) { }

  ngOnInit() {   
    this.inward_id = this.route.snapshot.paramMap.get("id");
    this.submitted = false;
    this.InwardEntriesForm = this.fb.group({
      erp: ['', Validators.required],
      trim_code: ['', Validators.required],
      received_quantity: ['', Validators.required],
      rejected_quantity: ['', Validators.required],
      barcode: ['', Validators.required],
      grn_no: ['', Validators.required],
      material_unit:['', Validators.required],
      inward_id: [''],
      table:['trim_inward_log']
    });

    if(this.inward_id != '' && this.inward_id != null){
      this.action = 'Update';
      this.api.getTrimInwardDetails({'inward_id':this.inward_id}).subscribe( response => {
        this.InwardEntriesForm = this.fb.group({
          erp: [response.erp, Validators.required],
          trim_code: [response.trim_code, Validators.required],
          received_quantity: [response.received_quantity, Validators.required],
          rejected_quantity: [response.rejected_quantity, Validators.required],
          barcode: [response.barcode, Validators.required],
          grn_no: [response.grn_no, Validators.required],
          material_unit:[response.material_unit, Validators.required],
          inward_id: [response.inward_id],
          table:['trim_inward_log']
        });
        this.getItemCodes();
      });
    }
    this.getErps();
  }
  
  getErps(){
    this.erp.getErpList().subscribe( response => {
      this.erps = response;
    });
  }

  getItemCodes(){
    this.api.getItemCodes({'requirement_type':'Trim', 'erp_id':this.InwardEntriesForm.value.erp}).subscribe( response => {
      this.itemcodes = response;
    });
  }

  addorUpdateTrimInwardEntry() {
    this.submitted = true;
    if (this.InwardEntriesForm.invalid) {
      return;
    }
    this.api.addorUpdateInwardEntry(this.InwardEntriesForm.value).subscribe( response => {
      if (response!= 0) {
        if(this.InwardEntriesForm.value.inward_id != ''){
          this.toastr.successToastr("Data updated successfully.");
        }else{
          this.toastr.successToastr("Data added successfully.");
        }
        this.router.navigate(['/production/trim-inwards']);
      } else{
        this.toastr.errorToastr("Error! While adding Data");
      }
    });
  }

  get formcontrol() { 
    return this.InwardEntriesForm.controls; 
  }

}
