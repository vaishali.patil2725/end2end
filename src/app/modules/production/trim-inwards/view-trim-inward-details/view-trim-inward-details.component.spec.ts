import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewTrimInwardDetailsComponent } from './view-trim-inward-details.component';

describe('ViewTrimInwardDetailsComponent', () => {
  let component: ViewTrimInwardDetailsComponent;
  let fixture: ComponentFixture<ViewTrimInwardDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewTrimInwardDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewTrimInwardDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
