import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProductionService } from '../../../_services/production.service';
import { UnitlinesService } from '../../../_services/unitlines.service';
import { ErpService } from '../../../_services/erp.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-sewing-data',
  templateUrl: './sewing-data.component.html',
  styleUrls: ['./sewing-data.component.css']
})
export class SewingDataComponent implements OnInit {
  @ViewChild('closeBtn', {static: false}) closeBtn: ElementRef<HTMLElement>;

  sewingDataForm: FormGroup;
  filterForm:FormGroup;

  erps: [];
  lines:[];
  production_data:[];
  submitted = false;
  userDesignation:string;
  selected_data:{};
  
  constructor(private fb: FormBuilder,
    private api: ProductionService,
    private lineService:UnitlinesService,
    private erp: ErpService,
    private toastr: ToastrManager) { }

  ngOnInit() {    
    this.filterForm = this.fb.group({
      f_erp:[""],
      f_date:[""],
      f_line:[""],
      table: 'sewing_production_data'
    });

    this.createSewingDataForm();
    this.getErps();
    this.getProductionData();
    this.getLines();
  }

  getLines(){
    this.lineService.getUnitLines({'department':6}).subscribe( response => {
      this.lines = response;
    });
  }

  getErps(){
    this.erp.getErpList().subscribe( response => {
      this.erps = response;
    });
  }

  getProductionData(){
    console.log(this.filterForm.value);
    this.api.getProductionData(this.filterForm.value).subscribe( response => {
      this.production_data = response;
    });
  }

  createSewingDataForm() {
    this.submitted = false;
    this.sewingDataForm = this.fb.group({
      erp: ['', Validators.required],
      section: ['', Validators.required],
      line: ['', Validators.required],
      production_hour: ['', Validators.required],
      output: ['', Validators.required],
      machineries: ['', Validators.required],
      helpers: ['', Validators.required],
      spi: ['', Validators.required],
      inches_covered: ['', Validators.required],
      compound_movement: ['', Validators.required],
      tailers: ['', Validators.required],
      ironers: ['', Validators.required],
      checkers: ['', Validators.required],
      id:[''],
      table:['sewing_production_data']
    });
  }

  editSewingData(data){
    this.submitted = false;
    this.sewingDataForm = this.fb.group({
      erp: [data.erp, Validators.required],
      section: [data.section, Validators.required],
      line: [data.line, Validators.required],
      production_hour: [data.production_hour, Validators.required],
      output: [data.output, Validators.required],
      machineries: [data.machineries, Validators.required],
      helpers: [data.helpers, Validators.required],
      spi: [data.spi, Validators.required],
      inches_covered: [data.inches_covered, Validators.required],
      compound_movement: [data.compound_movement, Validators.required],
      tailers: [data.tailers, Validators.required],
      ironers: [data.ironers, Validators.required],
      checkers: [data.checkers, Validators.required],
      id:[data.id],
      table:['sewing_production_data']
    });
  }

  get formcontrol() { 
    return this.sewingDataForm.controls; 
  }

  addorEditSewingData() {
    this.submitted = true;
    if (this.sewingDataForm.invalid) {
      return;
    }
    this.api.addorEditProductionData(this.sewingDataForm.value).subscribe( response => {
      if (response!= 0) {
        this.getProductionData();
        if(this.sewingDataForm.value.id!=''){
          this.toastr.successToastr("Data updated successfully.");
        }else{
          this.toastr.successToastr("Data added successfully.");
        }
        this.closeBtn.nativeElement.click();
      } else{
        this.toastr.errorToastr("Error! While adding Data");
      }
    });
  }

  viewProductionData(data){
    this.selected_data = data;
  }

  resetFilters(){
    this.filterForm.reset();
    this.filterForm.patchValue({table:"sewing_production_data"})
    this.getProductionData();
  }

  generateExcel() {
    this.api.downloadReports('Sewing', this.production_data);  }
}
