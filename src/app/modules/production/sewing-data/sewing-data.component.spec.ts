import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SewingDataComponent } from './sewing-data.component';

describe('SewingDataComponent', () => {
  let component: SewingDataComponent;
  let fixture: ComponentFixture<SewingDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SewingDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SewingDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
