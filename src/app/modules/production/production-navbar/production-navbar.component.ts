import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../../_services/auth.service';

@Component({
  selector: 'app-production-navbar',
  templateUrl: './production-navbar.component.html',
  styleUrls: ['./production-navbar.component.css']
})
export class ProductionNavbarComponent implements OnInit {

  user:any;
  public activeRoute = '';
  public parentRoute = '';
  userDesignation:any;
  
  constructor(private router:Router, private route: ActivatedRoute, private api: AuthService) {
    this.parentRoute = route.snapshot.parent.routeConfig.path;
    this.activeRoute = route.snapshot.routeConfig.path;
   }

  ngOnInit() {
    this.userDesignation = localStorage.getItem('userDesignation');

    this.getLoggedInUserData();
  }

  getLoggedInUserData(){
    this.api.getLoggedInUserData().subscribe(response=>{
      this.user=response;
    },err=>{
      console.log(err)
    });
  }

  logout(){
    localStorage.removeItem('userToken');
    sessionStorage.removeItem('userToken');
    localStorage.removeItem('userRole');
    localStorage.removeItem('userDesignation');
    this.router.navigate(['/']); 
  }

}
