import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FusingDataComponent } from './fusing-data.component';

describe('FusingDataComponent', () => {
  let component: FusingDataComponent;
  let fixture: ComponentFixture<FusingDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FusingDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FusingDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
