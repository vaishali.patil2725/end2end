import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProductionService } from '../../../_services/production.service';
import { UnitlinesService } from '../../../_services/unitlines.service';
import { ErpService } from '../../../_services/erp.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-fusing-data',
  templateUrl: './fusing-data.component.html',
  styleUrls: ['./fusing-data.component.css']
})
export class FusingDataComponent implements OnInit {
  @ViewChild('closeBtn', {static: false}) closeBtn: ElementRef<HTMLElement>;
 
  userDesignation:string;
  fusingDataForm: FormGroup;
  filterForm:FormGroup;

  erps: [];
  lines:[];
  production_data:[];
  submitted = false;
  selected_data:{};

  constructor(private fb: FormBuilder,
    private api: ProductionService,
    private lineService:UnitlinesService,
    private erp: ErpService,
    private toastr: ToastrManager) { }

  ngOnInit() {  
    this.filterForm = this.fb.group({
      f_erp:[""],
      f_date:[""],
      f_line:[""],
      table: 'fusing_production_data'
    });

    this.createFusingDataForm();
    this.getErps();
    this.getProductionData();
    this.getLines();
  }

  getLines(){
    this.lineService.getUnitLines({'department':6}).subscribe( response => {
      this.lines = response;
    });
  }

  getErps(){
    this.erp.getErpList().subscribe( response => {
      this.erps = response;
    });
  }

  getProductionData(){
    this.api.getProductionData(this.filterForm.value).subscribe( response => {
      this.production_data = response;
    });
  }


  createFusingDataForm() {
    this.submitted = false;
    this.fusingDataForm = this.fb.group({
      erp: ['', Validators.required],
      section: ['', Validators.required],
      line: ['', Validators.required],
      machine_no: ['', Validators.required],
      production_hour: ['', Validators.required],
      output: ['', Validators.required],
      helpers: ['', Validators.required],
      length: ['', Validators.required],
      width: ['', Validators.required],
      conveyer_speed: ['', Validators.required],
      converyer_width: ['', Validators.required],
      id:[''],
      table:['fusing_production_data']
    });
  }

  editFusingData(data){
    this.submitted = false;
    this.fusingDataForm = this.fb.group({
      erp: [data.erp, Validators.required],
      section: [data.section, Validators.required],
      line: [data.line, Validators.required],
      machine_no: [data.machine_no, Validators.required],
      production_hour: [data.production_hour, Validators.required],
      output: [data.output, Validators.required],
      helpers: [data.helpers, Validators.required],
      length: [data.length, Validators.required],
      width: [data.width, Validators.required],
      conveyer_speed: [data.conveyer_speed, Validators.required],
      converyer_width: [data.converyer_width, Validators.required],
      id:[data.id],
      table:['fusing_production_data']
    });
  }

  get formcontrol() { 
    return this.fusingDataForm.controls; 
  }

  addorEditFusingData() {
    this.submitted = true;
    console.log(this.fusingDataForm);
    if (this.fusingDataForm.invalid) {
      return;
    }
    this.api.addorEditProductionData(this.fusingDataForm.value).subscribe( response => {
      if (response!= 0) {
        this.getProductionData();
        if(this.fusingDataForm.value.id!=''){
          this.toastr.successToastr("Data updated successfully.");
        }else{
          this.toastr.successToastr("Data added successfully.");
        }
        this.closeBtn.nativeElement.click();
      } else{
        this.toastr.errorToastr("Error! While adding Data");
      }
    });
  }
  
  viewProductionData(data){
    this.selected_data = data;
  }

  resetFilters(){
    this.filterForm.reset();
    this.filterForm.patchValue({table:"fusing_production_data"})
    this.getProductionData();
  }

  generateExcel() {
    this.api.downloadReports('Fusing', this.production_data);
  }

}
