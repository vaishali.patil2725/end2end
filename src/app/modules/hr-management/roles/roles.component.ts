import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ToastrManager } from 'ng6-toastr-notifications';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { SharedService } from '../../../_services/shared.service'

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.css']
})
export class RolesComponent implements OnInit {

  @ViewChild('closeBtn', {static: false}) closeBtn: ElementRef<HTMLElement>;

  roles:[];
  NewRoleForm: FormGroup;
  submitted = false;
  selected_role = {};
  p:number=1;
  userRole = '';
  public popoverTitle: string = 'Delete Designation';
  public popoverMessage: string = 'Are you sure  you want to delete this designation?';
  public cancelClicked: boolean = false;
  public superUserAccess = ['admin', 'superuser'];

  constructor(private api:SharedService,
    private toastr:ToastrManager,
    private fb:FormBuilder) { }

  ngOnInit() {
    this.userRole = localStorage.getItem('userRole');
    this.getUserRoles();
    this.createNewRoleForm();
  }
 
  getUserRoles(){
    this.api.getUserRoles().subscribe( response => {
      this.roles =  response;
    });
  }

  createNewRoleForm(){
    this.submitted = false;
    this.NewRoleForm = this.fb.group({
      role: ["", [Validators.required]],
      description: ["", [Validators.required]],
      id:[""]
    });
  }

  editRole(data){
    this.submitted = false;
    this.NewRoleForm = this.fb.group({
      role: [data.role, [Validators.required]],
      description: [data.description, [Validators.required]],
      id:[data.id]
    });
  }

  viewRole(data){
   this.selected_role = data;
  }

  addorEditUserRole(){
    this.submitted = true;
    if (this.NewRoleForm.invalid) {
      return;
    }
    this.api.addorEditUserRole(this.NewRoleForm.value).subscribe(response => {
      if (response!= 0) {
        if(this.NewRoleForm.value.id!=''){
          this.toastr.successToastr("Role updated successfully.");
        }else{
          this.toastr.successToastr("Role added successfully.");
        }
        this.closeBtn.nativeElement.click();
        this.getUserRoles();
      } else{
        this.toastr.errorToastr("Error! While adding Role");
      }
    });
  }

  deleteUserRole(dept_id){
    this.api.deleteUserRole(dept_id).subscribe(response => {
      if (response!= 0) {
        this.toastr.successToastr("Role deleted successfully.");
      } else{
        this.toastr.errorToastr("Error! While deleting Role");
      }
    });
  }

  closeModal(){
    this.NewRoleForm.reset();
  }
  
  get formControls() {
    return this.NewRoleForm.controls;
  }

}
