import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HrManagementComponent } from './hr-management.component';
import { DesignationsComponent } from './designations/designations.component';
import { DepartmentsComponent } from './departments/departments.component';
import { EmployeesComponent } from './employees/employees.component';
import { RolesComponent } from './roles/roles.component';
import { HrDashboardComponent } from './hr-dashboard/hr-dashboard.component';

const routes: Routes = [
  { path: '',
      component:HrManagementComponent,
      children :[
          { path: '', component: HrDashboardComponent},
          { path: 'employees', component: EmployeesComponent},
          { path: 'designations', component: DesignationsComponent},
          { path: 'departments', component: DepartmentsComponent},
          { path: 'user-roles', component: RolesComponent},
      ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HrManagementRoutingModule { }
