import { Component, OnInit, ViewChild, ElementRef, HostListener } from '@angular/core';
import { ToastrManager } from 'ng6-toastr-notifications';
import { FormBuilder, FormGroup, Validators, Form } from "@angular/forms";
import { EmployeeService } from '../../../_services/employee.service'

import { ViewEmployeeComponent } from '../../shared/view-employee/view-employee.component';
import { CommonSettings } from '../../../_services/CommonSettings';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {
  @ViewChild(ViewEmployeeComponent, {static: true}) employee: ViewEmployeeComponent;
  @ViewChild('closeBtn', {static: false}) closeBtn: ElementRef<HTMLElement>;
  @ViewChild('closeUserFormBtn', {static: false}) closeUserFormBtn: ElementRef<HTMLElement>;
  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    this.closeBtn.nativeElement.click();
    this.submitted = false;
  }
  
  NewEmployeeForm: FormGroup;
  userForm: FormGroup;
  filterForm: FormGroup;

  designations:[];
  departments:[];
  units:[];
  companies:[];
  employees: [];
  roles = [];
  
  submitted = false;
  p:number=1;
  userRole:any;
  searchFilter:string;
  sel_designation = 1;
  message:string;
  temp;
  show_unit=false;
  show_department = false;
  dept_message:string;
  ADMIN:number;
  USER:number;

  public popoverTitle: string = 'Delete Employee';
  public popoverMessage: string = 'Are you sure  you want to delete this employee?';
  public cancelClicked: boolean = false;
  public superUserAccess = ['admin', 'superuser'];

  constructor(private api:EmployeeService,
    private toastr:ToastrManager,
    private fb:FormBuilder) { 
      this.ADMIN = CommonSettings.ROLE_ADMIN;
      this.USER = CommonSettings.ROLE_USER;
    }

  ngOnInit() {
    this.filterForm = this.fb.group({
      company:[""],
      unit:[""]
    });

    this.userForm = this.fb.group({
      full_name: [""],
      username: ["", [Validators.required]],
      password: ["", [Validators.required]],
      user_role:["",[Validators.required]],
      employee_id:["",[Validators.required]]
    });
    this.userRole = localStorage.getItem('userRole');
    this.getEmployees();
    this.createNewEmployeeForm();
    this.getRequiredFormData();
  }
 
  getEmployees(){
    this.api.getFilteredEmployees(this.filterForm.value).subscribe( response => {
     console.log(response);
      this.employees =  response;
    });
  }
  
  getRequiredFormData(){
    this.api.getRequiredFormData().subscribe( response => {
      if(this.userRole == this.ADMIN){
        this.companies = response['companies'];
      }
      this.designations =  response['designations'];
      this.units =  response['units'];
      this.roles = response['user_roles'];
      this.departments = response.departments;
    });
  }

  designationSelected(event){   
    let selectedIndex:number = event.target["selectedIndex"];
    let val = event.target.options[selectedIndex].getAttribute("value");
    let role = event.target.options[selectedIndex].getAttribute("data-role");
    
    if(role == this.USER){
      this.show_department = true;
    }else{
      this.show_department = false;
    }

    this.sel_designation = val;
    if(val == 1 || val == 2 || val == 3 || val == 4 ){
      this.show_unit = false;
    }else{
      this.show_unit = true;
    }
  }

  companySelected(val){
    if(this.show_unit == true){
      this.api.getUnits({'company_id':val}).subscribe( response => {
      this.units = response;
      });
    }
  }

  resetFilters(){
    this.filterForm.reset();
    this.getEmployees();
  }

  addorEditEmployee(){
    this.submitted = true;
    if (this.NewEmployeeForm.invalid) {
      return;
    }

    this.api.addorEditEmployee(this.NewEmployeeForm.value).subscribe(response => {
      if (response!= 0) {
        if(this.NewEmployeeForm.value.employee_id!=''){
          this.toastr.successToastr("Employee updated successfully.");
        }else{
          this.toastr.successToastr("Employee added successfully.");
        }
        this.closeBtn.nativeElement.click();
        this.getEmployees();
      } else{
        this.toastr.errorToastr("Error! While adding Employee");
      }
    });
  }
  
  createorEditUser(){
    this.submitted = true;
    if (this.userForm.invalid) {
      return;
    }
    this.api.createorEditUser(this.userForm.value).subscribe(response => {
      if (response!= 0) {
        if(this.userForm.value.user_id == ''){
          this.toastr.successToastr("User created successfully.");
        }else{
          this.toastr.successToastr("User updated successfully.");
        }
        this.closeUserFormBtn.nativeElement.click();
        this.getEmployees();
      } else{
        this.toastr.errorToastr("Error! While creating User");
      }
    });
  }

  deleteEmployee(employee_id){
    this.api.deleteEmployee(employee_id).subscribe(response => {
      if (response!= 0) {
        this.getEmployees();
        this.toastr.successToastr("Employee deleted successfully.");
      } else{
        this.toastr.errorToastr("Error! While deleting Employee");
      }
    });
  }

  createNewEmployeeForm(){
    this.submitted = false;
    this.NewEmployeeForm = this.fb.group({
      full_name: ["", Validators.required],
      email: ["", [Validators.required, Validators.email,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      phone: ["", [Validators.required]],
      designation_id: ["", [Validators.required]],
      company_id: [""],
      unit_id: [""],
      department_id: [""],
      address: ["", [Validators.required]],
      country: ["", [Validators.required]],
      city: ["", [Validators.required]],
      zip: ["", [Validators.required, Validators.maxLength(6),Validators.pattern('^[0-9]{1,6}$')]],
      employee_id:[""]
    });
  }

  editEmployee(data){
    this.submitted = false;
    if(data.unit_id != null && data.unit_id != 0){
      this.show_unit = true;
    }else{
      this.show_unit = false;
    }
    this.NewEmployeeForm = this.fb.group({
      full_name: [data.full_name, [Validators.required]],
      email: [data.email, [Validators.required, Validators.email,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      phone: [data.phone, [Validators.required]],
      designation_id: [data.designation_id, [Validators.required]],
      company_id: [data.company_id],
      unit_id: [data.unit_id, [Validators.required]],
      department_id: [data.department_id, [Validators.required]],
      address: [data.address, [Validators.required]],
      country: [data.country, [Validators.required]],
      city: [data.city, [Validators.required]],
      zip: [data.zip, [Validators.required, Validators.maxLength(6),Validators.pattern('^[0-9]{1,6}$')]],
      employee_id:[data.employee_id]
    });
  }

  createUserForm(employee){
    this.submitted = false;
    this.userForm = this.fb.group({
      full_name: [employee.full_name],
      username: [employee.username, [Validators.required]],
      password: ["", [Validators.required]],
      user_role:[employee.user_role],
      employee_id:[employee.employee_id],
      user_id:[employee.user_id]
    });
  }
  
  get formControls() {
    return this.NewEmployeeForm.controls;
  }
  
  get formControl() {
    return this.userForm.controls;
  }

  viewEmployee(employee_id){
    this.employee.showEmployeeDetails(employee_id);
  }

}
