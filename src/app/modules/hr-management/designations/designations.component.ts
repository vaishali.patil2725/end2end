import { Component, OnInit, ViewChild, ElementRef, HostListener } from '@angular/core';
import { ToastrManager } from 'ng6-toastr-notifications';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { SharedService } from '../../../_services/shared.service'

@Component({
  selector: 'app-designations',
  templateUrl: './designations.component.html',
  styleUrls: ['./designations.component.css']
})

export class DesignationsComponent implements OnInit {
  @ViewChild('closeBtn', {static: false}) closeBtn: ElementRef<HTMLElement>;
  @ViewChild('closeBtn2', {static: false}) closeBtn2: ElementRef<HTMLElement>;

  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    this.closeBtn.nativeElement.click();
    this.closeBtn2.nativeElement.click();
    this.submitted = false;
  }

  NewDesignationForm: FormGroup;

  designations:[];
  roles:[];
  selected_designation = {};
  submitted = false;
  p:number=1;
  userRole = '';

  public popoverTitle: string = 'Delete Designation';
  public popoverMessage: string = 'Are you sure  you want to delete this designation?';
  public cancelClicked: boolean = false;
  public superUserAccess = ['admin', 'superuser'];

  constructor(private api:SharedService,
    private toastr:ToastrManager,
    private fb:FormBuilder) { }

  ngOnInit() {
    this.userRole = localStorage.getItem('userRole');
    this.getDesignations();
    this.getUserRoles();
    this.createNewDesignationForm();
  }
 
  getDesignations(){
    this.api.getDesignations().subscribe( response => {
      this.designations =  response;
    });
  }

  getUserRoles(){
    this.api.getUserRoles().subscribe( response => {
      this.roles =  response;
    });
  }

  addorEditDesignation(){
    this.submitted = true;
    if (this.NewDesignationForm.invalid) {
      return;
    }
    this.api.addorEditDesignation(this.NewDesignationForm.value).subscribe(response => {
      if (response!= 0) {
        if(this.NewDesignationForm.value.designation_id!=''){
          this.toastr.successToastr("Designation updated successfully.");
        }else{
          this.toastr.successToastr("Designation added successfully.");
        }
        this.closeBtn.nativeElement.click();
        this.getDesignations();
      } else{
        this.toastr.errorToastr("Error! While adding Designation");
      }
    });
  }
  
  viewDesignation(data){
    this.selected_designation = data;
  }

  deleteDesignation(designation_id){
    this.api.deleteDesignation(designation_id).subscribe(response => {
      if (response!= 0) {
        this.toastr.successToastr("Designation deleted successfully.");
      } else{
        this.toastr.errorToastr("Error! While deleting Designation");
      }
    });
  }
  
  createNewDesignationForm(){
    this.submitted = false;
    this.NewDesignationForm = this.fb.group({
      designation: ["", [Validators.required]],
      user_role: ["", [Validators.required]],
      description: ["", [Validators.required]],
      designation_id:[""]
    });
  }

  editDesignation(data){
    this.submitted = false;
    this.NewDesignationForm = this.fb.group({
      designation: [data.designation, [Validators.required]],
      user_role: [data.user_role, [Validators.required]],
      description: [data.description, [Validators.required]],
      designation_id:[data.designation_id]
    });
  }

  get formControls() {
    return this.NewDesignationForm.controls;
  }

}
