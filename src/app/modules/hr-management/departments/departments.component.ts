import { Component, OnInit, ViewChild, ElementRef, HostListener } from '@angular/core';
import { ToastrManager } from 'ng6-toastr-notifications';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { SharedService } from '../../../_services/shared.service'

@Component({
  selector: 'app-departments',
  templateUrl: './departments.component.html',
  styleUrls: ['./departments.component.css']
})
export class DepartmentsComponent implements OnInit {
  @ViewChild('closeBtn', {static: false}) closeBtn: ElementRef<HTMLElement>;
  @ViewChild('closeBtn2', {static: false}) closeBtn2: ElementRef<HTMLElement>;

  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    this.closeBtn.nativeElement.click();
    this.closeBtn2.nativeElement.click();
    this.submitted = false;
  }

  NewDepartmentForm: FormGroup;

  departments:[];
  selected_department = {};
  submitted = false;
  p:number=1;
  userRole = '';

  public popoverTitle: string = 'Delete Designation';
  public popoverMessage: string = 'Are you sure  you want to delete this designation?';
  public cancelClicked: boolean = false;
  public superUserAccess = ['admin', 'superuser'];

  constructor(private api:SharedService,
    private toastr:ToastrManager,
    private fb:FormBuilder) { }

  ngOnInit() {
    this.userRole = localStorage.getItem('userRole');
    this.getDepartments();
    this.createNewDepartmentForm();
  }
 
  getDepartments(){
    this.api.getDepartments().subscribe( response => {
      this.departments =  response;
    });
  }

  addorEditDepartment(){
    this.submitted = true;
    if (this.NewDepartmentForm.invalid) {
      return;
    }
    this.api.addorEditDepartment(this.NewDepartmentForm.value).subscribe(response => {
      if (response!= 0) {
        if(this.NewDepartmentForm.value.dept_id!=''){
          this.toastr.successToastr("Department updated successfully.");
        }else{
          this.toastr.successToastr("Department added successfully.");
        }
        this.closeBtn.nativeElement.click();
        this.getDepartments();
      } else{
        this.toastr.errorToastr("Error! While adding Department");
      }
    });
  }
  
  viewDepartment(data){
    this.selected_department = data;
   }

  deleteDepartment(dept_id){
    this.api.deleteDepartment(dept_id).subscribe(response => {
      if (response!= 0) {
        this.toastr.successToastr("Department deleted successfully.");
      } else{
        this.toastr.errorToastr("Error! While deleting Department");
      }
    });
  }
  
  createNewDepartmentForm(){
    this.submitted = false;
    this.NewDepartmentForm = this.fb.group({
      dept_name: ["", [Validators.required]],
      description: ["", [Validators.required]],
      dept_id:[""]
    });
  }

  editDepartment(data){
    this.submitted = false;
    this.NewDepartmentForm = this.fb.group({
      dept_name: [data.dept_name, [Validators.required]],
      description: [data.description, [Validators.required]],
      dept_id:[data.dept_id]
    });
  }
  
  get formControls() {
    return this.NewDepartmentForm.controls;
  }

}
