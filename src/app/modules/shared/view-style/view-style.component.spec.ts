import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewStyleComponent } from './view-style.component';

describe('ViewStyleComponent', () => {
  let component: ViewStyleComponent;
  let fixture: ComponentFixture<ViewStyleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewStyleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewStyleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
