import { Component, OnInit, HostListener, ViewChild, ElementRef} from '@angular/core';
import { StyleService } from '../../../_services/style.service';
declare var $;

@Component({
  selector: 'app-view-style',
  templateUrl: './view-style.component.html',
  styleUrls: ['./view-style.component.css']
})
export class ViewStyleComponent implements OnInit {
  @ViewChild('closeStyleBtn', {static: false}) closeStyleBtn: ElementRef<HTMLElement>;
  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    this.closeStyleBtn.nativeElement.click();
  }

  style={};

  constructor(private api:StyleService) { }

  ngOnInit() {
  }

  showStyleDetails(style_id){
    this.api.getStyleDetails(style_id).subscribe( response => {
      this.style = response;
      console.log(this.style);
      $('#viewStyle').modal('show');
    });
  }

}
