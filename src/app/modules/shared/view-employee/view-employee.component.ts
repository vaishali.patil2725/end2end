import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../../../_services/employee.service';

declare var $;

@Component({
  selector: 'app-view-employee',
  templateUrl: './view-employee.component.html',
  styleUrls: ['./view-employee.component.css']
})
export class ViewEmployeeComponent implements OnInit {

  employee={};
  constructor(private api:EmployeeService) { }

  ngOnInit() {
  }

  showEmployeeDetails(employee_id){
    this.api.getEmployeeInformaton(employee_id).subscribe( response => {
      this.employee = response;
      $('#viewEmployee').modal('show');
    });
  }

}
