import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedRoutingModule } from './shared-routing.module';
import { SharedComponent } from './shared.component';
import { ViewStyleComponent } from './view-style/view-style.component';
import { ViewEmployeeComponent } from './view-employee/view-employee.component';

@NgModule({
  declarations: [SharedComponent, ViewStyleComponent, ViewEmployeeComponent],
  imports: [
    CommonModule,
    SharedRoutingModule
  ],
  exports:[ViewStyleComponent, ViewEmployeeComponent]
})
export class SharedModule { }
