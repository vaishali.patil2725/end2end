import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../_services/auth.service';
import { Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';
import { CommonSettings } from '../../_services/CommonSettings';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;

  constructor(private formBuilder: FormBuilder,
    private api: AuthService,
    private route: Router, public toastr: ToastrManager) {
    }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', [Validators.required]]//, Validators.minLength(6)
    });

  }

  get formControls() {
    return this.loginForm.controls;
  }

  userLogin() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }
    this.api.userLogin(this.loginForm.value).subscribe(response => {
      console.log(response);
      if (response != 0) {
        this.toastr.successToastr('Login successful!');
        sessionStorage.setItem('userToken', response.token);
        localStorage.setItem('userRole', response.role);
        localStorage.setItem('company', response.company);
        localStorage.setItem('unit', response.unit);
        localStorage.setItem('department', response.department);
		    localStorage.setItem('designation', response.designation);
        window.location.reload();
      } else {
        this.toastr.errorToastr('Invalid credentials');
      }
    },
      err => {
        console.log(err)
      }
    );
  }

}
