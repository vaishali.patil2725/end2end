import { Component, OnInit, HostListener, Inject } from '@angular/core';
import { AuthService } from '../../_services/auth.service';
import { CommonSettings } from '../../_services/CommonSettings';
import { Router } from '@angular/router';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  
  user: any;
  userRole: any;
  unitSuperUser = false;
  toggle = false;
  department:number;
  designation:number;

  ADMIN : number;
  SUPERUSER : number;
  USER : number;
  BUYER : number;

  DEPT_TRIM:number;
  DEPT_FABRIC : number;
  DEPT_CUTTING : number;
  DEPT_SEWING : number;
  DEPT_FINISHING : number;
  DEPT_KAJA_BUTTON : number;
  DEPT_FUSING : number;
  DEPT_DISPATCH : number;
  DEPT_MECHANICAL : number;

  constructor(private api: AuthService,private router:Router, @Inject(DOCUMENT) document) {
    this.ADMIN = CommonSettings.ROLE_ADMIN;
    this.SUPERUSER = CommonSettings.ROLE_SUPERUSER;
    this.USER = CommonSettings.ROLE_USER;
    this.BUYER = CommonSettings.ROLE_BUYER;

    this.DEPT_TRIM = CommonSettings.DEPT_TRIM;
    this.DEPT_FABRIC = CommonSettings.DEPT_FABRIC;
    this.DEPT_CUTTING = CommonSettings.DEPT_CUTTING;
    this.DEPT_SEWING = CommonSettings.DEPT_SEWING;
    this.DEPT_FINISHING = CommonSettings.DEPT_FINISHING;
    this.DEPT_KAJA_BUTTON = CommonSettings.DEPT_KAJA_BUTTON;
    this.DEPT_FUSING = CommonSettings.DEPT_FUSING;
    this.DEPT_DISPATCH = CommonSettings.DEPT_DISPATCH;
    this.DEPT_MECHANICAL = CommonSettings.DEPT_MECHANICAL;

	this.designation = parseInt(localStorage.getItem('designation'));
  }

  ngOnInit() {
    this.userRole = localStorage.getItem('userRole');
    this.getLoggedInUserData();
  }

  @HostListener('window:scroll', ['$event'])
  onWindowScroll(e) {
    var header = document.getElementById("myTopnav");
      var sticky = header.offsetTop;
      if (window.pageYOffset > sticky) {
        header.classList.add("sticky");
      } else {
        header.classList.remove("sticky");
      }
  }

  getLoggedInUserData() {
    this.api.getLoggedInUserData().subscribe(response => {
      this.user = response;
      this.department = response.department_id;
      if (response.unit == 'null' || response.unit == null || response.unit == 0) {
        this.unitSuperUser = false;
      } else {
        this.unitSuperUser = true;
      }
    }, err => {
      console.log(err)
    });
  }

  logout() {
    sessionStorage.removeItem('userToken');
    localStorage.removeItem('userRole');
    localStorage.removeItem('company');
    localStorage.removeItem('unit');
		window.location.reload();
  }

  toggleNav(){
    if(this.toggle == false){
      this.toggle = true;
    }else{
      this.toggle = false;
    }
  }

}
