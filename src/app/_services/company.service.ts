import { Injectable } from '@angular/core';
import { map } from "rxjs/operators";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  public base_url: any;
  
  constructor(public http:HttpClient) { 
   this.base_url = environment.baseUrl;
  }

  getCompanyList() {
    return this.http.get<any>( this.base_url+'Company/getCompaniesList')
  .pipe(  
    map(res => {
      return res;
    })
    );
  }

  addorEditCompany(data){
    return this.http.post<any>( this.base_url+'Company/addorEditCompany', data)
    .pipe(  
      map(res => {
        return res;
      })
      );
  }

}
