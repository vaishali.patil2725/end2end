import { Injectable } from '@angular/core';
import { map } from "rxjs/operators";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class BrandService {

  public base_url: any;
  
  constructor(public http:HttpClient) { 
   this.base_url = environment.baseUrl;
  }

  getBrands(){
    return this.http.get<any>( this.base_url+'brands/getBrands')
  .pipe(  
    map(res => {
      return res;
    })
    );
  }

  getFilteredBrands(data){
    return this.http.post<any>( this.base_url+'brands/getFilteredBrands', data)
  .pipe(  
    map(res => {
      return res;
    })
    );
  }

  addorEditBrand(data){
    return this.http.post<any>( this.base_url+'brands/addorEditBrand', data)
    .pipe(  
      map(res => {
        return res;
      })
    );
  }
 
}
