import { Injectable } from '@angular/core';
import { map } from "rxjs/operators";
import { HttpClient,HttpHeaders } from "@angular/common/http";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class PurchaseOrderService {
  public base_url: any;
  
  constructor(public http:HttpClient) { 
   this.base_url = environment.baseUrl;
  }

  getRequiredInitialData(){
    return this.http.get<any>( this.base_url+'PurchaseOrder/getRequiredInitialData')
    .pipe(  
      map(res => {
        return res;
      })
    );
  }
  
  getFilteredPurchaseOrders(data){
    return this.http.post<any>( this.base_url+'PurchaseOrder/getFilteredPurchaseOrders', data)
    .pipe(  
      map(res => {
        return res;
      })
    );
  }

  getPurchaseOrders() {
    return this.http.get<any>( this.base_url+'PurchaseOrder/getPurchaseOrders')
  .pipe(  
    map(res => {
      return res;
    })
    );
  }

  addorEditPurchaseOrder(data){
    return this.http.post<any>( this.base_url+'PurchaseOrder/addorEditPurchaseOrder', data)
    .pipe(  
      map(res => {
        return res;
      })
    );
  }

  getPurchaseOrderDetails(order_id){
    return this.http.post<any>( this.base_url+'PurchaseOrder/getPurchaseOrderDetails', order_id)
    .pipe(  
      map(res => {
        return res;
      })
    );
  }

  getLotsToAddDeliverySchedule(order_id, erp_id){
    return this.http.post<any>( this.base_url+'PurchaseOrder/getLotsToAddDeliverySchedule', {'order_id':order_id, 'erp_id':erp_id})
    .pipe(  
      map(res => {
        return res;
      })
    );
  }

  getDispatchLots(){
    return this.http.get<any>( this.base_url+'PurchaseOrder/getDispatchLots')
    .pipe(  
      map(res => {
        return res;
      })
    );
  }

  getCartonContent(data){
    return this.http.post<any>( this.base_url+'PurchaseOrder/getCartonContent', data)
    .pipe(  
      map(res => {
        return res;
      })
    );
  }

  addorEditCartonContent(data){
    return this.http.post<any>( this.base_url+'PurchaseOrder/addorEditCartonContent', data)
    .pipe(  
      map(res => {
        return res;
      })
    );
  }
  
  addorEditDispatchLot(data){
    return this.http.post<any>( this.base_url+'PurchaseOrder/addorEditDispatchLot', data)
    .pipe(  
      map(res => {
        return res;
      })
    );
  }

}
