import { Injectable } from '@angular/core';
import { map } from "rxjs/operators";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  public base_url: any;
    
  constructor(public http:HttpClient) { 
  this.base_url = environment.baseUrl;
  }


/***************************Designations***************************/
  getDesignations(){
    return this.http.get<any>( this.base_url+'SharedController/getDesignations')
  .pipe(  
    map(res => {
      return res;
    })
    );
  }

  addorEditDesignation(data){
    return this.http.post<any>( this.base_url+'SharedController/addorEditDesignation', data)
    .pipe(  
      map(res => {
        return res;
      })
    );
  }

  deleteDesignation(designation_id){
    return this.http.post<any>( this.base_url+'SharedController/deleteDesignation', {'designation_id':designation_id})
    .pipe(  
      map(res => {
        return res;
      })
    );
  }

  /***************************Departments***************************/
  getDepartments(){
    return this.http.get<any>( this.base_url+'SharedController/getDepartments')
  .pipe(  
    map(res => {
      return res;
    })
    );
  }

  addorEditDepartment(data){
    return this.http.post<any>( this.base_url+'SharedController/addorEditDepartment', data)
    .pipe(  
      map(res => {
        return res;
      })
    );
  }

  deleteDepartment(dept_id){
    return this.http.post<any>( this.base_url+'SharedController/deleteDepartment', {'dept_id':dept_id})
    .pipe(  
      map(res => {
        return res;
      })
    );
  }

  /***************************User ROles***************************/
  getUserRoles(){
    return this.http.get<any>( this.base_url+'SharedController/getUserRoles')
  .pipe(  
    map(res => {
      return res;
    })
    );
  }

  addorEditUserRole(data){
    return this.http.post<any>( this.base_url+'SharedController/addorEditUserRole', data)
    .pipe(  
      map(res => {
        return res;
      })
    );
  }

  deleteUserRole(id){
    return this.http.post<any>( this.base_url+'SharedController/deleteUserRole', {'id':id})
    .pipe(  
      map(res => {
        return res;
      })
    );
  }

  /*****************************************Reports*************************** */

  getReportsData(data){
    return this.http.post<any>( this.base_url+'ReportsController/getReportsData', data)
  .pipe(  
    map(res => {
      return res;
    })
    );
  }
}
