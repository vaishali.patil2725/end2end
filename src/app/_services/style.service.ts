import { Injectable } from '@angular/core';
import { map } from "rxjs/operators";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class StyleService {

  public base_url: any;
  
  constructor(public http:HttpClient) { 
   this.base_url = environment.baseUrl;
  }

  getStyles(){
    return this.http.get<any>( this.base_url+'Styles/getStyles')
  .pipe(  
    map(res => {
      return res;
    })
    );
  }

  getFilteredStyles(data){
    return this.http.post<any>( this.base_url+'Styles/getFilteredStyles', data)
    .pipe(  
      map(res => {
        return res;
      })
    );
  }
 
  addorEditNewStyle(data){
    return this.http.post<any>( this.base_url+'Styles/addorEditNewStyle', data)
    .pipe(  
      map(res => {
        return res;
      })
      );
  }

  getStyleDetails(style_id){
    return this.http.post<any>( this.base_url+'Styles/getStyleDetails', {'style_id':style_id})
    .pipe(  
      map(res => {
        return res;
      })
    );
  }
}
