import { Injectable } from '@angular/core';
import { map } from "rxjs/operators";
import { HttpClient,HttpHeaders } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { ExcelService } from './excel.service';
import { from } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductionService {

  public base_url: any;
  
  constructor(public http:HttpClient,
    private excelService:ExcelService
    ) { 
   this.base_url = environment.baseUrl;
  }


  /*******************Trim Inward**************** */
  getTrimInwardEntries(data){
    return this.http.post<any>( this.base_url+'Production/getTrimInwardEntries', data)
    .pipe(  
      map(res => {
        return res;
      })
    );
  }

  getTrimInwardDetails(data){
    return this.http.post<any>( this.base_url+'Production/getTrimInwardDetails', data)
    .pipe(  
      map(res => {
        return res;
      })
    );
  }

  getTrimOutwardLogData(data){
    return this.http.post<any>( this.base_url+'Production/getTrimOutwardLogData', data)
    .pipe(  
      map(res => {
        return res;
      })
    );
  }

  /*************************Common Trim/Fabric Functions********** */

  addorUpdateInwardEntry(data){
    return this.http.post<any>( this.base_url+'Production/addorUpdateInwardEntry', data)
    .pipe(  
      map(res => {
        return res;
      })
    );
  }

  addorEditOutwardEntry(data){
    return this.http.post<any>( this.base_url+'Production/addorEditOutwardEntry', data)
    .pipe(  
      map(res => {
        return res;
      })
    );
  }

  /*******************Fabric Inward**************** */
  getFilteredFabricInwards(data){
    return this.http.post<any>( this.base_url+'Production/getFilteredFabricInwards', data)
    .pipe(  
      map(res => {
        return res;
      })
    );
  }

  getFabricInwardDetails(data){
    return this.http.post<any>( this.base_url+'Production/getFabricInwardDetails', data)
    .pipe(  
      map(res => {
        return res;
      })
    );
  }

  getFabricOutwardLogData(data){
    return this.http.post<any>( this.base_url+'Production/getFabricOutwardLogData', data)
    .pipe(  
      map(res => {
        return res;
      })
    );
  }

  /*************************************************** */

  getItemCodes(data){
    return this.http.post<any>( this.base_url+'Production/getItemCodes', data)
    .pipe(  
      map(res => {
        return res;
      })
    );
  }

  addorEditMaterialInwardEntry(data){
    return this.http.post<any>( this.base_url+'Production/addorEditMaterialInwardEntry', data)
    .pipe(  
      map(res => {
        return res;
      })
    );
  }

  getReceivedMaterialData(data){
    console.log(data);
    return this.http.post<any>( this.base_url+'Production/getReceivedMaterialData', data)
    .pipe(  
      map(res => {
        return res;
      })
    );
  }

  getOutwardLogData(){
    return this.http.get<any>( this.base_url+'Production/getOutwardLogData')
    .pipe(  
      map(res => {
        return res;
      })
    );
  }

  addorEditMaterialOutwardEntry(data){
    return this.http.post<any>( this.base_url+'Production/addorEditMaterialOutwardEntry', data)
    .pipe(  
      map(res => {
        return res;
      })
    );
  }

  addorEditProductionData(data){
    return this.http.post<any>( this.base_url+'Production/addorEditProductionData', data)
    .pipe(  
      map(res => {
        return res;
      })
    );
  }
  
  getProductionData(data){
    return this.http.post<any>( this.base_url+'Production/getProductionData', data)
    .pipe(  
      map(res => {
        return res;
      })
    );
  }

  addorEditFabricInspectionEntry(data){
    return this.http.post<any>( this.base_url+'Production/addorEditFabricInspectionEntry', data)
    .pipe(  
      map(res => {
        return res;
      })
    );
  }

  getFabricInspectionLogs(data){
    return this.http.post<any>( this.base_url+'Production/getFabricInspectionLogs', data)
    .pipe(  
      map(res => {
        return res;
      })
    );
  }

  /******************************Excel Reports************** */
  downloadReports(report_type, data){
    let title = '';
    let header = [];
    var result = [];

    if(report_type == 'cutting_production_report'){
      title = 'Cutting Production Report';
      header = ['ERP', 'Section', 'Line', 'Datetime', 'Production Hour', 'Mechaneries', 'Helpers', 'Cutters', 'Layerers', 'Added By'];
      Object.keys(data).forEach(function(key) {
        let data_row = [];
        data_row.push(data[key]["erp_title"]);
        data_row.push(data[key]["section"]);
        data_row.push(data[key]["line_name"]);
        data_row.push(data[key]["datetime"]);
        data_row.push(data[key]["production_hour"]);
        data_row.push(data[key]["machineries"]);
        data_row.push(data[key]["helpers"]);
        data_row.push(data[key]["cutters"]);
        data_row.push(data[key]["layerers"]);
        data_row.push(data[key]['full_name']);
  
        result.push(data_row);
      });
    } else if(report_type == 'sewing_production_report'){
      title = 'Sewing Production Report';
      header = ['ERP', 'Section', 'Line', 'Datetime', 'Production Hour', 'SPI', 'Inches Covered', 'Compound Movement', 'Machineries', 'Helpers', 'Ironers', 'Checkers', 'Added By'];
      Object.keys(data).forEach(function(key) {
        let data_row = [];
        data_row.push(data[key]["erp_title"]);
        data_row.push(data[key]["section"]);
        data_row.push(data[key]["line_name"]);
        data_row.push(data[key]["datetime"]);
        data_row.push(data[key]["production_hour"]);
        data_row.push(data[key]["spi"]);
        data_row.push(data[key]["inches_covered"]);
        data_row.push(data[key]["compound_movement"]);
        data_row.push(data[key]["machineries"]);
        data_row.push(data[key]["helpers"]);
        data_row.push(data[key]["ironers"]);
        data_row.push(data[key]["checkers"]);
        data_row.push(data[key]['full_name']);
  
        result.push(data_row);
      });
    } else if(report_type == 'finishing_production_report'){
      title = 'Finishing Production Report';
      header = ['ERP', 'Section', 'Line', 'Datetime', 'Production Hour', 'Compound Movement', 'Helpers', 'Ironers', 'Checkers', 'Added By'];
      Object.keys(data).forEach(function(key) {
        let data_row = [];
        data_row.push(data[key]["erp_title"]);
        data_row.push(data[key]["section"]);
        data_row.push(data[key]["line_name"]);
        data_row.push(data[key]["datetime"]);
        data_row.push(data[key]["production_hour"]);
        data_row.push(data[key]["compound_movement"]);
        data_row.push(data[key]["helpers"]);
        data_row.push(data[key]["ironers"]);
        data_row.push(data[key]["checkers"]);
        data_row.push(data[key]['full_name']);
  
        result.push(data_row);
      });
    }else if(report_type == 'kajabutton_production_report'){
      title = 'Kaja Button Production Report';
      header = ['ERP', 'Section', 'Line', 'Datetime', 'Production Hour', 'SPI', 'Inches Covered', 'Compound Movement', 'Machineries', 'Helpers', 'Ironers', 'Checkers', 'Added By'];
      Object.keys(data).forEach(function(key) {
        let data_row = [];
        data_row.push(data[key]["erp_title"]);
        data_row.push(data[key]["section"]);
        data_row.push(data[key]["line_name"]);
        data_row.push(data[key]["datetime"]);
        data_row.push(data[key]["production_hour"]);
        data_row.push(data[key]["spi"]);
        data_row.push(data[key]["inches_covered"]);
        data_row.push(data[key]["compound_movement"]);
        data_row.push(data[key]["machineries"]);
        data_row.push(data[key]["helpers"]);
        data_row.push(data[key]["ironers"]);
        data_row.push(data[key]["checkers"]);
        data_row.push(data[key]['full_name']);
  
        result.push(data_row);
      });
    }else if(report_type == 'fusing_production_report'){
      title = 'Fusing Production Report';
      header = ['ERP', 'Section', 'Line', 'Datetime', 'Production Hour', 'Machine Number', 'Length', 'Width', 'Converyer Width', 'Conveyer Speed', 'Helpers', 'Added By'];
      Object.keys(data).forEach(function(key) {
        let data_row = [];
        data_row.push(data[key]["erp_title"]);
        data_row.push(data[key]["section"]);
        data_row.push(data[key]["line_name"]);
        data_row.push(data[key]["datetime"]);
        data_row.push(data[key]["production_hour"]);
        data_row.push(data[key]["machin_no"]);
        data_row.push(data[key]["length"]);
        data_row.push(data[key]["width"]);
        data_row.push(data[key]["converyer_width"]);
        data_row.push(data[key]["conveyer_speed"]);
        data_row.push(data[key]["helpers"]);
        data_row.push(data[key]['full_name']);
  
        result.push(data_row);
      });
    }else if(report_type == 'dispatch_report'){
      
      title = 'Dispatch Report';

    }else if(report_type == 'trim_inward_report'){
      title = 'Trim Inward Report';
      header = ['ERP', 'Trim Code', 'Unit', 'Received Quantity', 'Rejected Quantity', 'Barcode', 'GRN Number', 'DateTime', 'Added By'];
      Object.keys(data).forEach(function(key) {
        let data_row = [];
        data_row.push(data[key]["erp_title"]);
        data_row.push(data[key]["trim_code"]);
        data_row.push(data[key]["received_quantity"]);
        data_row.push(data[key]["rejected_quantity"]);
        data_row.push(data[key]["grn_no"]);
        data_row.push(data[key]["barcode"]);
        data_row.push(data[key]["grn_no"]);
        data_row.push(data[key]['created_on']);
        data_row.push(data[key]['added_by']);
  
        result.push(data_row);
      });
    }else if(report_type == 'fabric_inward_report'){
      title = 'Fabric Inward Report';
      header = ['ERP', 'Fabric Code', 'Number of Rolls', 'Quantity', 'GRN Number', 'Barcode', 'Added By', 'Added On'];
      Object.keys(data).forEach(function(key) {
        let data_row = [];
        data_row.push(data[key]["erp_title"]);
        data_row.push(data[key]["fabric_code"]);
        data_row.push(data[key]["rolls"]);
        data_row.push(data[key]["received_quantity"]);
        data_row.push(data[key]["grn_no"]);
        data_row.push(data[key]["barcode"]);
        data_row.push(data[key]['added_by']);
        data_row.push(data[key]['created_on']);
  
        result.push(data_row);
      });
    }else if(report_type == 'trim_outward_report'){
      title = 'Trim Outward Report';
      header = ['ERP', 'Fabric Code', 'Received Quantity', 'Outward Quantity', 'GRN Number','Barcode', 'Department', 'Line', 'DateTime', 'Added By'];
      Object.keys(data).forEach(function(key) {
        let data_row = [];
        data_row.push(data[key]["erp_title"]);
        data_row.push(data[key]["outward_quantity"]);
        data_row.push(data[key]["outward_quantity"]);
        data_row.push(data[key]["grn_no"]);
        data_row.push(data[key]["barcode"]);
        data_row.push(data[key]["dept_name"]);
        data_row.push(data[key]["line_name"]);
        data_row.push(data[key]['created_on']);
        data_row.push(data[key]['added_by']);
  
        result.push(data_row);
      });
    }else if(report_type == 'fabric_outward_report'){
      title = 'Fabric Outward Report';
      header = ['ERP', 'Fabric Code', 'Received Quantity', 'Outward Quantity', 'GRN Number','Barcode', 'Department', 'Line', 'DateTime', 'Added By'];
      Object.keys(data).forEach(function(key) {
        let data_row = [];
        data_row.push(data[key]["erp_title"]);
        data_row.push(data[key]["outward_quantity"]);
        data_row.push(data[key]["outward_quantity"]);
        data_row.push(data[key]["grn_no"]);
        data_row.push(data[key]["barcode"]);
        data_row.push(data[key]["dept_name"]);
        data_row.push(data[key]["line_name"]);
        data_row.push(data[key]['created_on']);
        data_row.push(data[key]['added_by']);
  
        result.push(data_row);
      });
    }else if(report_type == 'fabric_inspection_report'){
      title = 'Fabric Inspeection Report';
      header = ['ERP', 'Color', 'Roll Number', 'Total Quantity', 'Rejected Quantity', 'Barcode', 'Added By', 'Added On'];
      Object.keys(data).forEach(function(key) {
        let data_row = [];
        data_row.push(data[key]["erp_title"]);
        data_row.push(data[key]["color"]);
        data_row.push(data[key]["roll_no"]);
        data_row.push(data[key]["quantity"]);
        data_row.push(data[key]["rejections"]);
        data_row.push(data[key]["barcode"]);
        data_row.push(data[key]['added_by']);
        data_row.push(data[key]['created_on']);
  
        result.push(data_row);
      });
    }
   
    this.excelService.generateExcel(title, header, result);
  }
}
