import { Injectable } from '@angular/core';
import { HttpClient  } from '@angular/common/http';
import { map } from 'rxjs/operators';
import {environment}  from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  _url: string;
  constructor(private http: HttpClient) { 
    this._url = environment.baseUrl;
  }

  userLogin(data){
    return  this.http.post<any>(this._url+'Authentication/userLogin',data).pipe(map((res:any)=>{
      return res;
    }));
  }

  getLoggedInUserData(){
    return  this.http.get<any>(this._url+'Authentication/getLoggedInUserData').pipe(map((res:any)=>{
      return res;
    }));
  }

  getEmployeeData()
  {
	return  this.http.get<any>(this._url+ "Employee/getEmployeeDetails");
  }
  
  updateProfile(data)
  {
	 return this.http.post<any>(this._url+ "Employee/UpdateEmployeeData",data)
  }
  
  ChangePassword(data)
  {
	  return this.http.post<any>(this._url+ "Employee/ChangePassword",data)
  }
}
