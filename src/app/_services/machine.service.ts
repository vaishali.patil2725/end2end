import { Injectable } from '@angular/core';
import { map } from "rxjs/operators";
import { HttpClient,HttpHeaders } from "@angular/common/http";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class MachineService {
  public base_url: any;
  
  constructor(public http:HttpClient) { 
   this.base_url = environment.baseUrl;
  }

  getAllMachines(){
    return this.http.get<any>( this.base_url+'Machines/getAllMachines')
    .pipe(  
      map(res => {
        return res;
      })
    );
  }

  getServiceHistory(data){
    return this.http.post<any>( this.base_url+'Machines/getServiceHistory', data)
    .pipe(  
      map(res => {
        return res;
      })
    );
  }

  addorEditMachine(data){
    return this.http.post<any>( this.base_url+'Machines/addorEditMachine', data)
    .pipe(  
      map(res => {
        return res;
      })
    );
  }

  addorEditServiceHostory(data){
    return this.http.post<any>( this.base_url+'Machines/addorEditServiceHostory', data)
    .pipe(  
      map(res => {
        return res;
      })
    );
  }

  validateMachinCode(data){
    return this.http.post<any>( this.base_url+'Machines/validateMachinCode', data)
    .pipe(  
      map(res => {
        return res;
      })
    );
  }

}
