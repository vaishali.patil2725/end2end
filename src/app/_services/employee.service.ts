import { Injectable } from '@angular/core';
import { map } from "rxjs/operators";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
    public base_url: any;
    
    constructor(public http:HttpClient) { 
    this.base_url = environment.baseUrl;
    }

    getEmployees(){
      return this.http.get<any>( this.base_url+'Employee/getEmployees')
    .pipe(  
      map(res => {
        return res;
      })
      );
    }

    getFilteredEmployees(data){
      return this.http.post<any>( this.base_url+'Employee/getFilteredEmployees', data)
    .pipe(  
      map(res => {
        return res;
      })
      );
    }

    getAllCompanyManagers(){
      return this.http.get<any>( this.base_url+'Employee/getAllCompanyManagers')
      .pipe(  
        map(res => {
          return res;
        })
        );
    }
    getFactoryManagerEmployees(){
      return this.http.get<any>( this.base_url+'Employee/getFactoryManagerEmployees')
    .pipe(  
      map(res => {
        return res;
      })
      );
    }

    getRequiredFormData(){
      return this.http.get<any>( this.base_url+'Employee/getRequiredFormData')
      .pipe(  
        map(res => {
          return res;
        })
      );
    }
  
    addorEditEmployee(data){
      return this.http.post<any>( this.base_url+'Employee/addorEditEmployee', data)
      .pipe(  
        map(res => {
          return res;
        })
        );
    }

    getEmployeeInformaton(employee_id){
      return this.http.post<any>( this.base_url+'Employee/getEmployeeInformaton', {'employee_id':employee_id})
      .pipe(  
        map(res => {
          return res;
        })
        );
    }

    editEmployee(data){
      return this.http.post<any>( this.base_url+'Employee/editEmployee', data)
      .pipe(  
        map(res => {
          return res;
        })
        );
    }

    deleteEmployee(employee_id){
      return this.http.post<any>( this.base_url+'Employee/deleteEmployee', {'employee_id':employee_id})
      .pipe(  
        map(res => {
          return res;
        })
        );
    }

    getBuyers(){
      return this.http.get<any>( this.base_url+'Employee/getBuyers')
      .pipe(  
        map(res => {
          return res;
        })
        );
    }

    createorEditUser(data){
      return this.http.post<any>( this.base_url+'Employee/createorEditUser', data)
      .pipe(  
        map(res => {
          return res;
        })
      );
    }

    getUnits(data){
      return this.http.post<any>( this.base_url+'Unit/getUnitsByCompanyId', data)
      .pipe(  
        map(res => {
          return res;
        })
      );
    }

}
