import { Injectable } from '@angular/core';
import { map } from "rxjs/operators";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class UnitService {
  public base_url: any;
  
  constructor(public http:HttpClient) { 
   this.base_url = environment.baseUrl;
  }

  getUnits(){
    return this.http.get<any>( this.base_url+'Unit/getUnits')
    .pipe(  
      map(res => {
        return res;
      })
    );
  }

  getFilteredUnits(data){
    return this.http.post<any>( this.base_url+'Unit/getFilteredUnits', data)
    .pipe(  
      map(res => {
        return res;
      })
    );
  }

  addorEditNewUnit(data){
    return this.http.post<any>( this.base_url+'Unit/addorEditNewUnit', data)
    .pipe(  
      map(res => {
        return res;
      })
    );
  }
}
