import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class UnitlinesService {
  base_url:any;
  
  constructor(private http:HttpClient) {
    this.base_url=environment.baseUrl;
  }

  getAllLines(){
    return this.http.get<any>( this.base_url+'UnitLines/getAllLines')
    .pipe(  
      map(res => {
        return res;
      })
      );
  }
  getUnitLines(data){
    return this.http.post<any>( this.base_url+'UnitLines/getUnitLines', data)
    .pipe(  
      map(res => {
        return res;
      })
      );
  }
  addorEditUnitline(data){
    return this.http.post<any>( this.base_url+'UnitLines/addorEditUnitline', data)
    .pipe(  
      map(res => {
        return res;
      })
    );
  }
}
