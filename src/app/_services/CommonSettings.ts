// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const CommonSettings = {
 ROLE_ADMIN : 1,
 ROLE_SUPERUSER : 2,
 ROLE_USER : 3,
 ROLE_BUYER : 4,

 DEPT_TRIM : 3,
 DEPT_FABRIC : 4,
 DEPT_CUTTING : 5,
 DEPT_SEWING : 6,
 DEPT_FINISHING : 7,
 DEPT_KAJA_BUTTON : 8,
 DEPT_FUSING : 9,
 DEPT_DISPATCH : 10,
 DEPT_MECHANICAL : 11

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
