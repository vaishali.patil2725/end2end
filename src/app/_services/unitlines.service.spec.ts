import { TestBed } from '@angular/core/testing';

import { UnitlinesService } from './unitlines.service';

describe('UnitlinesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UnitlinesService = TestBed.get(UnitlinesService);
    expect(service).toBeTruthy();
  });
});
