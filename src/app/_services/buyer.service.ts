import { Injectable } from '@angular/core';
import { map } from "rxjs/operators";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class BuyerService {

  public base_url: any;
  
  constructor(public http:HttpClient) { 
   this.base_url = environment.baseUrl;
  }

  getBuyers(){
    return this.http.get<any>( this.base_url+'Buyers/getBuyers')
  .pipe(  
    map(res => {
      return res;
    })
    );
  }
  
  addorEditNewBuyer(data){
    return this.http.post<any>( this.base_url+'Buyers/addorEditNewBuyer', data)
    .pipe(  
      map(res => {
        return res;
      })
    );
  }

  getBuyerDetails(buyer_id){
    return this.http.post<any>( this.base_url+'Buyers/getBuyerDetails', {'buyer_id':buyer_id})
    .pipe(  
      map(res => {
        return res;
      })
    );
  }
}
