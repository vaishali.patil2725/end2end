import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './pages/login/login.component';

import { AuthGuardService } from './_helpers/auth-guard.service';
import { CurrentUserService } from './_services/current-user.service';

const routes: Routes = [
    { path:'', component:LoginComponent, canActivate:[AuthGuardService] },
    { path:'login', component:LoginComponent, canActivate:[AuthGuardService] },

    { path: 'dashboard', loadChildren: () => import('./modules/dashboard/dashboard.module').then(mod => mod.DashboardModule), canActivate:[CurrentUserService] },
    { path: 'purchase-order', loadChildren: () => import('./modules/purchase-order/purchase-order.module').then(m => m.PurchaseOrderModule), canActivate:[CurrentUserService]  },
    { path: 'erp', loadChildren: () => import('./modules/erp/erp.module').then(m => m.ErpModule), canActivate:[CurrentUserService] },
    { path: 'styles', loadChildren: () => import('./modules/styles/styles.module').then(m => m.StylesModule), canActivate:[CurrentUserService] },
    { path: 'brands', loadChildren: () => import('./modules/brands/brands.module').then(m => m.BrandsModule), canActivate:[CurrentUserService] },
    { path: 'buyers', loadChildren: () => import('./modules/buyers/buyers.module').then(m => m.BuyersModule), canActivate:[CurrentUserService] },
    { path: 'company', loadChildren: () => import('./modules/company/company.module').then(m => m.CompanyModule), canActivate:[CurrentUserService] },
    { path: 'units', loadChildren: () => import('./modules/units/units.module').then(m => m.UnitsModule), canActivate:[CurrentUserService] },
    { path: 'lines', loadChildren: () => import('./modules/unitlines/unitlines.module').then(m => m.UnitlinesModule), canActivate:[CurrentUserService] },
    { path: 'production', loadChildren: () => import('./modules/production/production.module').then(m => m.ProductionModule), canActivate:[CurrentUserService] },
    { path: 'profile', loadChildren: () => import('./modules/profile/profile.module').then(m => m.ProfileModule), canActivate:[CurrentUserService] },
    { path: 'hr', loadChildren: () => import('./modules/hr-management/hr-management.module').then(m => m.HrManagementModule), canActivate:[CurrentUserService] },
    { path: 'shared', loadChildren: () => import('./modules/shared/shared.module').then(m => m.SharedModule), canActivate:[CurrentUserService] },
    
    {
      path: '',
      redirectTo: '',
      pathMatch: 'full'
    },
    { path: 'machines', loadChildren: () => import('./modules/machines/machines.module').then(m => m.MachinesModule) },
    /*
    { path: 'production-data', loadChildren: () => import('./modules/production-data/production-data.module').then(m => m.ProductionDataModule) },
  */

   ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
